#include "AABB.h"

namespace Physics
{
	void AABB::Translate(const Vector2 &position)
	{
		Upper += position;
		Lower += position;
	}

	bool AABB::Intersects(const AABB &b) const
	{
		if(b.Lower.X > Upper.X) return false;
		if(b.Lower.Y > Upper.Y) return false;
		if(Lower.X > b.Upper.X) return false;
		if(Lower.Y > b.Upper.Y) return false;
		return true;
	}
}