#ifndef _AABB_H_
#define _AABB_H_
#include "Vector2.h"

using namespace Math;

namespace Physics
{
	struct AABB
	{
		Vector2 Upper;
		Vector2 Lower;

		bool Intersects(const AABB &b) const;
		void Translate(const Vector2 &position);
	};
}

#endif