#ifndef __BMP_H__
#define __BMP_H__

namespace FileTypes
{
	struct BMPHeader
	{
		char BM[2];
		unsigned int FileSize;
		short Reserved1;
		short Reserved2;
		unsigned int DataOffset;

		unsigned int InfoSize;
		int Width;
		int Height;
		unsigned short Planes;
		unsigned short BitsPerPixel;
		unsigned int Compression;
		unsigned int ImageSize;
		int XResolution;
		int YResolution;
		unsigned int ColorPaletteEntriesUsed;
		unsigned int ColorPaletteEntriesRequired;
	};
}

#endif