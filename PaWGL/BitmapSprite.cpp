#include "BitmapSprite.h"

namespace Graphics
{
	BitmapSprite::BitmapSprite(HDCBitmapBuffer* buffer, LPSTR filename)
	{
		_buffer = buffer;
		_hdc = _buffer->CreateCompatibleHDC();
		Load(filename);
	}

	BitmapSprite::~BitmapSprite(void)
	{
		DeleteObject(_hBitmap);
		DeleteDC(_hdc);
	}

	int BitmapSprite::GetWidth()
	{ return _width; }

	int BitmapSprite::GetHeight()
	{ return _height; }

	Point BitmapSprite::GetPosition()
	{ return _position; }

	void BitmapSprite::SetPosition(const Point p)
	{ _position = p; }

	void BitmapSprite::SetPosition(const int x, const int y)
	{ _position = Point(x, y); }

	void BitmapSprite::Load(LPSTR filename)
	{		
		_hBitmap = (HBITMAP)LoadImage(NULL, filename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		BITMAP b;
		GetObject(_hBitmap, sizeof(BITMAP), (LPVOID)&b);
		_width = (int)b.bmWidth;
		_height = (int)b.bmHeight;

		if(b.bmBitsPixel == 32)
		{
			int size = b.bmHeight * b.bmWidth * b.bmBitsPixel / 8;
			BYTE* lpBits = new BYTE[size];
			GetBitmapBits(_hBitmap, size, lpBits);

			// Alpha Channel
			BYTE* pPixel = lpBits;
			// pre-multiply rgb channels with alpha channel
			for (int y = 0; y < _height; y++)
			{				
				for (int x = 0; x < _width ; x++)
				{
					pPixel[0]= (BYTE)((float)pPixel[0] * (float)pPixel[3] / 255.0f);
					pPixel[1]= (BYTE)((float)pPixel[1] * (float)pPixel[3] / 255.0f);
					pPixel[2]= (BYTE)((float)pPixel[2] * (float)pPixel[3] / 255.0f);
					pPixel += 4;
				}
			}

			SetBitmapBits(_hBitmap, size, lpBits);
			free(lpBits);
		}
	}

	void BitmapSprite::Translate(const int x, const int y)
	{ _position = Point(_position.X + x, _position.Y + y); }

	void BitmapSprite::Translate(const Point p)
	{ _position = Point(_position.X + p.X, _position.Y + p.Y); }

	void BitmapSprite::Render(void)
	{
		HBITMAP originalBitMap;
		originalBitMap = (HBITMAP)SelectObject(_hdc, _hBitmap);
		//BitBlt(_buffer->GetHDC(), _position.X, _position.Y, _position.X + _width, _position.Y + _height, _hdc, 0, 0, SRCCOPY);
		//TransparentBlt(_buffer->GetHDC(), _position.X, _position.Y, _width, _height, _hdc, 0, 0, _width, _height, RGB(255, 0, 255));
		AlphaBlend(_buffer->GetHDC(), _position.X - (_width >> 1), _position.Y - (_height >> 1), _width, _height, _hdc, 0, 0, _width, _height, Blend);
		SelectObject(_hdc, originalBitMap); 
	}

	BLENDFUNCTION BitmapSprite::Blend = {AC_SRC_OVER, 0, 255, AC_SRC_ALPHA};
}