#pragma once
#include <windows.h>
#include "HDCBitmapBuffer.h"
#include "Point.h"

namespace Graphics
{
	class BitmapSprite
	{
	private:
		HDCBitmapBuffer* _buffer;
		HDC _hdc;
		HBITMAP _hBitmap;
		Point _position;
		int _width, _height;

	public:
		BitmapSprite(HDCBitmapBuffer* buffer, LPSTR filename);
		~BitmapSprite(void);

		int GetWidth();
		int GetHeight();

		Point GetPosition();
		void SetPosition(const Point p);
		void SetPosition(const int x, const int y);

		void Load(LPSTR filename);

		void Translate(const int x, const int y);
		void Translate(const Point p);

		void Render(void);

		static BLENDFUNCTION Blend;
	};
}