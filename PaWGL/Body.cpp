#include "Body.h"
#include "SoftBody.h"

namespace Physics
{
	Body::Body(const Vector2 position, const float mass, const float restitution, const float friction, const bool isStatic) 
		: _motionSystem(VerletVector2MotionSystem(position)), _expectedPosition(position),
		_mass(mass), _inverseMass(1 / mass), _restitution(restitution), _friction(friction), _isStatic(isStatic), _isConvex(false), _isSoftBody(false), _isTriggerArea(false), _collideEvent(0), _tag(0)
	{ 		
		for(int i = 0; i < _sides.Count(); i++) _sides[i].Start += position;
		_bounds.Translate(position);
	}

	Body::~Body(void) { }
	
	Vector2& Body::GetPosition() { return _motionSystem.Position; }
	Vector2& Body::GetDisplacement() { return _motionSystem.Displacement; }
	void Body::SetPosition(const Vector2 &position) 
	{ Translate(position - _motionSystem.Position); }
	void Body::Translate(const Vector2 &displacement) 
	{
		for(int i = 0; i < _sides.Count(); i++) _sides[i].Start += displacement;
		_bounds.Translate(displacement);
		_motionSystem.Position += displacement; 
	}

	Vector2& Body::GetAcceleration() { return _motionSystem.Acceleration; }
	void Body::SetAcceleration(const Vector2 &acceleration) { _motionSystem.Acceleration = acceleration; }
	void Body::IncreaseAcceleration(const Vector2 &acceleration) { _motionSystem.Acceleration += acceleration; }

	Vector2 Body::CalculateVelocity(GameTime gameTime)
	{ return _motionSystem.CalculateVelocity(gameTime); }

	void Body::ApplyVelocity(Vector2 velocity, GameTime gameTime)
	{ _motionSystem.ApplyVelocity(velocity, gameTime); }

	bool Body::GetIsStatic() { return _isStatic; }
	void Body::SetIsStatic(bool isStatic) { _isStatic = isStatic; }

	float Body::GetMass() { return _mass; }
	float Body::GetInverseMass() { return _inverseMass; }
	void Body::SetMass(const float mass) { _mass = mass; _inverseMass = 1 / mass; }

	float Body::GetRestitution() { return _restitution; }
	void Body::SetRestitution(const float restitution) { _restitution = restitution; }

	float Body::GetFriction() { return _friction; }
	void Body::SetFriction(const float friction) { _friction = friction; }

	void Body::SetCollideEvent(const Event<CollisionInfo>* collideEvent) { _collideEvent = (Event<CollisionInfo>*)collideEvent; }

	void* Body::GetTag() { return _tag; }
	void Body::SetTag(void* tag) { _tag = tag; }

	List<Line2D>& Body::GetSides() { return _sides; }
	AABB& Body::GetBounds() { return _bounds; }

	bool Body::IsConvex() { return _isConvex; }
	
	bool Body::FindCollisions(Body* b, List<CollisionInfo> &infos)
	{
		if(b->_isTriggerArea) return b->Intersects(this, infos);
		return Intersects(b, infos); 
	}

	void Body::BeginTimeStep(GameTime gameTime, Vector2 gravity)
	{ 
		_motionSystem.TimeStep(gameTime, 0.999f, (_isStatic) ? Vector2(0,0) : gravity);
		_expectedPosition = _motionSystem.Position;

		for(int i = 0; i < _sides.Count(); i++) _sides[i].Start += _motionSystem.Displacement;
		_bounds.Translate(_motionSystem.Displacement);
	}

	void Body::Collide(CollisionInfo &info)
	{ 
		Vector2 reverse(info.Incidence * -info.IncidencePenetration);

		float velocityX = info.Velocity.DotProduct(info.Normal);
		float velocityY = info.Velocity.DotProduct(info.Normal.Perpendicular());
		velocityX *= -info.Restitution;
		velocityY *= (1 - info.Friction);
		Vector2 expulsion(info.Normal * velocityX + info.Normal.Perpendicular() * velocityY);

		if(info.B->_isStatic && !_isStatic)
		{
			_motionSystem.Position += reverse;
			_motionSystem.Displacement = expulsion;
		}
		else if(!_isStatic)
		{
			float momemtumConservation = (info.B->_mass / (_mass + info.B->_mass));
			_motionSystem.Position += reverse * momemtumConservation;
			_motionSystem.Displacement = expulsion * momemtumConservation;
		}

		if(_collideEvent) _collideEvent->Invoke(info);
	}

	void Body::EndTimeStep(GameTime gameTime)
	{ 
		Vector2 positionDifference(_motionSystem.Position - _expectedPosition);
		for(int i = 0; i < _sides.Count(); i++) _sides[i].Start += positionDifference; //Update Sides
		_bounds.Translate(positionDifference); //Update Bounding Box
	}

	void Body::CreateAABB()
	{
		_bounds.Upper.X = _sides[0].Start.X;
		_bounds.Upper.Y = _sides[0].Start.Y;
		_bounds.Lower.X = _sides[0].Start.X;
		_bounds.Lower.Y = _sides[0].Start.Y;

		for(int i = 0; i < _sides.Count(); i++)
		{
			if(_sides[i].Start.X > _bounds.Upper.X) _bounds.Upper.X = _sides[i].Start.X;
			if(_sides[i].Start.Y > _bounds.Upper.Y) _bounds.Upper.Y = _sides[i].Start.Y;
			if(_sides[i].Start.X < _bounds.Lower.X) _bounds.Lower.X = _sides[i].Start.X;
			if(_sides[i].Start.Y < _bounds.Lower.Y) _bounds.Lower.Y = _sides[i].Start.Y;
		}
	}

	bool Body::Intersects(Body* b, List<CollisionInfo> &infos)
	{
		if(!_bounds.Intersects(b->_bounds)) return false;
		bool A = LineTest(b, infos);
		bool B = b->LineTest(this, infos);
		return A || B;
		//Functions Results Must Be Stored To Ensure Both Methods Are Called.
	}

	bool Body::LineTest(Body* b, List<CollisionInfo> &infos)
	{
		bool BodyIntersects = false;
		for(int i = 0; i < _sides.Count(); i++)
		{
			Vector2 VerticeA(_sides[i].Start);
			bool IsVerticeAInside = false;

			int LastVertice = b->GetSides().Count() - 1;

			//http://www.gamasutra.com/view/feature/3429/crashing_into_the_new_year_.php?page=2
			Vector2 Point1(b->GetSides()[0].Start);
			Vector2 Point2(b->GetSides()[1].Start);
			bool Sign = (VerticeA.Y >= Point1.Y);
			for(int vertice = 0; vertice < b->GetSides().Count(); vertice++) 
			{
				Point2 = (vertice == b->GetSides().Count() - 1) ? b->GetSides()[0].Start : b->GetSides()[vertice + 1].Start;
				bool Sign2 = (VerticeA.Y >= Point2.Y);
				if(Sign != Sign2) //If True Then Edge Crosses X Axis
				{
					bool Intersect = (((Point2.Y - VerticeA.Y) * (Point1.X - Point2.X)) >= ((Point2.X - VerticeA.X) * (Point1.Y - Point2.Y)));
					if(Intersect == Sign2) //If True Then Edge Crosses (X > 0)
						IsVerticeAInside = !IsVerticeAInside;
					
				}
				Sign = Sign2;
				Point1 = Point2;
			}

			if(IsVerticeAInside)
			{
				BodyIntersects = true;

				//For Each Line In This Body
				Line2DSegment CheckLine(b->GetSides()[LastVertice].Start, b->GetSides()[0].Start);
				int ClosestSide = LastVertice;
				float ClosestDistanceSquared = CheckLine.ShortestDistanceSquaredToPoint(VerticeA);
				for(int side = 0; side < LastVertice; side++)
				{
					//Find Edge Closest To Intersecting Vertice (VerticeA)
					CheckLine = Line2DSegment(b->GetSides()[side].Start, b->GetSides()[side + 1].Start);
					float DistanceSquared = CheckLine.ShortestDistanceSquaredToPoint(VerticeA);
					if(DistanceSquared < ClosestDistanceSquared)
					{
						ClosestDistanceSquared = DistanceSquared;
						ClosestSide = side;
					}
				}

				CollisionInfo info;
				info.AVertice = i;
				info.BSide = ClosestSide;
				info.A = this;
				info.B = b;
				info.NormalPenetration = sqrtf(ClosestDistanceSquared);

				//Check If Normal Needs To Be Flipped
				//???bool isCCW = b->_sides[ClosestSide].Direction.IsCounterClockwise(_sides[i].Normal);
				if((_sides[i].Normal.DotProduct(_sides[i].Normal) * b->_sides[ClosestSide].Direction.DotProduct(_sides[i].Normal)) > 0.0f)
					info.Normal = -b->_sides[ClosestSide].Normal;
				else 
					info.Normal = b->_sides[ClosestSide].Normal;

				if(FinalizeCollisionInfo(b, info)) infos.Add(info);
				else continue;

				info.A = b;
				info.B = this;
				info.Normal = -info.Normal;
				info.Incidence = -info.Incidence;
				info.Velocity = -info.Velocity;
				if(!b->_isSoftBody) infos.Add(info);
				else
				{
					info.BSide = -1;

					info.AVertice = ClosestSide;
					infos.Add(info);

					info.AVertice = (ClosestSide == b->GetSides().Count() - 1) ? 0 : ClosestSide + 1;
					infos.Add(info);
				}
			}
		}
		return BodyIntersects;
	}
	
	bool Body::FinalizeCollisionInfo(Body* b, CollisionInfo &info)
	{
		info.Restitution = (_restitution + b->_restitution) * 0.5f;
		info.Friction = (_friction + b->_friction) * 0.5f;
		
		Vector2 aVel(_motionSystem.Displacement);
		if(_isSoftBody) aVel = ((SoftBody*)this)->GetPointMasses()[info.AVertice].GetDisplacement();

		Vector2 bVel(b->_motionSystem.Displacement);
		if(b->_isSoftBody) 
		{
			bVel = ((SoftBody*)b)->GetPointMasses()[info.BSide].GetDisplacement();
			int mass2 = (info.BSide == ((SoftBody*)b)->GetPointMasses().Count() - 1) ? 0 : info.BSide + 1;
			bVel += ((SoftBody*)b)->GetPointMasses()[mass2].GetDisplacement();
			//bVel *= 0.5f;
		}

		info.Velocity = aVel - bVel;
		info.Incidence = info.Velocity.Normalize();
		float IncidenceDotNormal = info.Incidence.DotProduct(-info.Normal);
		info.IncidencePenetration = info.NormalPenetration / IncidenceDotNormal;

		if(info.IncidencePenetration < 0)
		{
			info.Incidence = -info.Incidence;
			info.IncidencePenetration = -info.IncidencePenetration;
			info.Velocity = -info.Velocity;
		}

		return !(IncidenceDotNormal == 0.0f);
	}
}