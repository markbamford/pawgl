#ifndef _Body_H_
#define _Body_H_

#include "List.h"
#include "Vector2.h"
#include "Line2D.h"
#include "Line2DSegment.h"
#include "AABB.h"
#include "CollisionInfo.h"
#include "GameTime.h"
#include "Event.h"
#include "MathEx.h"
#include "VerletVector2MotionSystem.h"

namespace Physics
{
	struct CollisionInfo;
	class Body
	{
	protected:
		List<Line2D> _sides;
		AABB _bounds;
		
		VerletVector2MotionSystem _motionSystem;
		Vector2 _expectedPosition;

		float _mass;
		float _inverseMass;
		float _restitution;
		float _friction;
		bool _isStatic;

		bool _isConvex;
		bool _isSoftBody;
		bool _isTriggerArea;

		Event<CollisionInfo>* _collideEvent;
		void* _tag;

	public:
		Body(const Vector2 position, const float mass = 1.0f, const float restitution = 0.9f, const float friction = 0.9f, const bool isStatic = false);
		~Body(void);

		Vector2& GetPosition();
		Vector2& GetDisplacement();
		void SetPosition(const Vector2 &position);
		virtual void Translate(const Vector2 &displacement);

		Vector2& GetAcceleration();
		void SetAcceleration(const Vector2 &acceleration);
		void IncreaseAcceleration(const Vector2 &acceleration);

		Vector2 CalculateVelocity(GameTime gameTime);
		virtual void ApplyVelocity(Vector2 velocity, GameTime gameTime);

		bool GetIsStatic();
		void SetIsStatic(bool isStatic);
		bool IsFinalized();

		float GetMass();
		float GetInverseMass();
		void SetMass(const float mass);

		float GetRestitution();
		void SetRestitution(const float restitution);

		float GetFriction();
		void SetFriction(const float friction);

		void SetCollideEvent(const Event<CollisionInfo>* collideEvent);

		void* GetTag();
		void SetTag(void* tag);

		List<Line2D>& GetSides();
		AABB& GetBounds();

		bool IsConvex();

		bool FindCollisions(Body* b, List<CollisionInfo> &info);

		virtual void BeginTimeStep(GameTime gameTime, Vector2 gravity);
		virtual void Collide(CollisionInfo &info);
		virtual void EndTimeStep(GameTime gameTime);
	
		virtual void CreateAABB();
	protected:
		virtual bool Intersects(Body* b, List<CollisionInfo> &info);
		bool LineTest(Body* b, List<CollisionInfo> &infos);
		bool FinalizeCollisionInfo(Body* b, CollisionInfo &info);
	};
}

#endif