#include "Bounds.h"

namespace Graphics
{
	Bounds::Bounds()
	{ }

	Bounds::Bounds(int width, int height)
		: Left(0), Top(0), Right(width), Bottom(height)
	{ }

	Bounds::Bounds(int x, int y, int width, int height)
		: Left(x), Top(y), Right(x + width), Bottom(y + height)
	{ }
}