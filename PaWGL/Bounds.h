#ifndef __Bounds_H__
#define __Bounds_H__

namespace Graphics
{
	struct Bounds
	{				
		int Top, Left, Bottom, Right;

		Bounds();
		Bounds(int width, int height);
		Bounds(int x, int y, int width, int height);
	};
}

#endif