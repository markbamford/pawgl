#ifndef __Bullet_H_INCLUDED__
#define __Bullet_H_INCLUDED__

#include "PS2Sprite.h"
#include "Box.h"
#include "Vector2.h"
#include "GameTime.h"
#include "Simulator2D.h"
#include "Asteroids.h"
#include "Callback.h"
#include <cmath>

using namespace Graphics;
using namespace Physics;
using namespace Math;

class Asteroids;
class Bullet
{
private:
	Simulator2D* _simulator;
	Asteroids* _asteroids;
	int _playerIndex;
	Box _body;
	PS2Sprite _sprite;
	bool _exists;

	static const float Width = 4.0f;
	static const float Height = 4.0f;
	static const float Velocity = 30.0f;

public:
	Bullet(Vector2 position, Vector2 direction, GameTime gameTime, Asteroids &asteroids, Simulator2D &simulator);
	~Bullet(void);

	Box& GetBody();
	bool Exists();

	void CollideCallback(PolygonConvex& b);

	void Update(GameTime gameTime);
	void Draw();

private:
	MemberCallback<Bullet, PolygonConvex&, &Bullet::CollideCallback>* _bulletCallback;
};

#endif