#include "Circle.h"

namespace Physics
{
	Circle::Circle(const Vector2 &position, const float &radius, const float mass, const float restitution, const float friction, const bool isStatic)
		: ConvexBody(position, radius, mass, restitution, friction, isStatic)
	{
		//TODO: Make Circle Lines...Is this needed? Good for drawing perhaps...
		CreateAABB();
	}

	Circle::~Circle(void)
	{
	}

	bool Circle::Intersects(ConvexBody* b, List<CollisionInfo> &infos)
	{ 
		if(b->GetRadius() == -1) return ConvexBody::Intersects(b, infos);

		Vector2 distance(b->GetPosition() - _motionSystem.Position);
		float distanceLengthSquared = distance.DotProduct(distance);
		float radiusSum = _radius + b->GetRadius();

		if(distanceLengthSquared > (radiusSum * radiusSum)) return false;

		CollisionInfo info;
		info.A = this;
		info.B = b;
		info.Normal = -distance.Normalize();
		info.NormalPenetration = _radius + b->GetRadius() - sqrt(distanceLengthSquared);
		if(FinalizeCollisionInfo(b, info)) infos.Add(info);
		else return false;

		info.Normal = -info.Normal;
		info.Incidence = -info.Incidence;
		info.Velocity = -info.Velocity;
		info.A = b;
		info.B = this;
		infos.Add(info);
		return true;
	}

	void Circle::CreateAABB()
	{
		_bounds.Upper.X = _radius + _motionSystem.Position.X;
		_bounds.Upper.Y = _radius + _motionSystem.Position.Y;
		_bounds.Lower.X = -_radius + _motionSystem.Position.X;
		_bounds.Lower.Y = -_radius + _motionSystem.Position.Y;
	}

	void Circle::AddPossibleSeparatingAxes(ConvexBody* b, List<SeparatingAxisComparer> &Axes)
	{
		for(int i = 0; i < b->GetSides().Count(); i++)
		{
			////Check Side Voronoi Region (Not Required)

			//Vector2 v(sides->[(i == (sides->Count() - 1)) ? 0 : i + 1].Start - sides->[i].Start);
			////v is the vector from the lineseg 's start vertex to its end vertex

			//Vector2 d(_motionSystem.Position - sides->[i].Start);
			////d is the vector from the lineseg's first vertex to the circle center

			//float vLengthSquared = v.DotProduct(v); //len2 is the squared-length of the lineseg
			//float dp = d.DotProduct(v);

			
			//if(dp < 0 || dp > vLengthSquared)
			//{
				//Check Vertice Voronoi Region (Triangle Check)
				// Compute vectors        
				Vector2 v0(b->GetSides()[i].Normal);
				Vector2 v1(b->GetSides()[(i == 0) ? b->GetSides().Count() - 1 : i - 1].Normal);
				Vector2 v2(_motionSystem.Position - b->GetSides()[i].Start);

				// Compute dot products
				float dot00 = v0.DotProduct(v0);
				float dot01 = v0.DotProduct(v1);
				float dot02 = v0.DotProduct(v2);
				float dot11 = v1.DotProduct(v1);
				float dot12 = v1.DotProduct(v2);

				// Compute barycentric coordinates
				float invDenom = 1 / (dot00 * dot11 - dot01 * dot01);
				float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
				float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

				// Check if point is in triangle
				if(!(u > 0 && v > 0)) continue;// && (u + v < 1) last condition removed because the triangle goes on for infinity
			//}
			Axes.Add(SeparatingAxisComparer(v2, SeparatingAxisComparer::Parallel));
			Vector2 radius = Axes[Axes.Count() - 1].GetDirection() * _radius;
			Axes[Axes.Count() - 1].AddPoint(_motionSystem.Position + radius);
			Axes[Axes.Count() - 1].AddPoint(_motionSystem.Position - radius);
			return;
		}
	}

	void Circle::AddComparePoints(SeparatingAxisComparer &Axis)
	{
		Vector2 radius = Axis.GetDirection() * _radius;
		Axis.AddComparePoint(_motionSystem.Position + radius);
		Axis.AddComparePoint(_motionSystem.Position - radius);
	}
}