#ifndef _Circle_H_
#define _Circle_H_
#include "ConvexBody.h"

namespace Physics
{
	class Circle : public ConvexBody
	{
	public:
		Circle(const Vector2 &position, const float &radius, const float mass = 1.0f, const float restitution = 0.7f, const float friction = 0.9f, const bool isStatic = false);
		~Circle(void);

		bool Intersects(ConvexBody* b, List<CollisionInfo> &infos);

	protected:
		void CreateAABB();
		void AddPossibleSeparatingAxes(ConvexBody* b, List<SeparatingAxisComparer> &Axes);
		void AddComparePoints(SeparatingAxisComparer &Axis);
	};
}

#endif