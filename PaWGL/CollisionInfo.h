#ifndef _CollisionInfo_H_
#define _CollisionInfo_H_
#include "Body.h"
#include "Vector2.h"

namespace Physics
{
	class Body;
	struct CollisionInfo
	{
		Body* A;
		Body* B;
		Math::Vector2 Normal;
		float NormalPenetration;
		Math::Vector2 Incidence;
		float IncidencePenetration;
		Math::Vector2 Velocity;
		float Restitution;
		float Friction;

		int AVertice;
		int BSide;

		CollisionInfo(void);

		friend bool operator<(const CollisionInfo &a, const CollisionInfo &b);
	};
}

#endif