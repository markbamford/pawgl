#include "Color4.h"

namespace Graphics
{
	Color4::Color4(void)
		: R(1.0f), G(1.0f), B(1.0f), A(1.0f)
	{ }

	Color4::Color4(float r, float g, float b, float a)
		: R(r), G(g), B(b), A(a)
	{ }
}