#ifndef __Color4_H__
#define __Color4_H__

namespace Graphics
{
	struct Color4
	{				
		float R, G, B, A;
		
		Color4();
		Color4(float r, float g, float b, float a);
	};
}

#endif