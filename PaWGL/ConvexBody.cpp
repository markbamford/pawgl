#include "ConvexBody.h"

namespace Physics
{
	ConvexBody::ConvexBody(const Vector2 position, const float radius, const float mass, const float restitution, const float friction, const bool isStatic) 
		: _radius(radius), Body(position, mass, restitution, friction, isStatic)
	{ _isConvex = true; }

	ConvexBody::ConvexBody(const Vector2 position, const float mass, const float restitution, const float friction, const bool isStatic) 
		: _radius(-1), Body(position, mass, restitution, friction, isStatic)
	{ _isConvex = true; }

	ConvexBody::~ConvexBody(void)
	{
	}

	float ConvexBody::GetRadius() { return _radius; } 

	bool ConvexBody::Intersects(Body* b, List<CollisionInfo> &infos)
	{
		if(!b->IsConvex()) return Body::Intersects(b, infos);
		if(!_bounds.Intersects(b->GetBounds())) return false;

		CollisionInfo info;
		info.A = this;
		info.B = b;
		info.NormalPenetration = -1.0f;

		//Separating Axis Theorem
		static List<SeparatingAxisComparer> Axes;
		Axes.Clear();
		ConvexBody* bConvex = (ConvexBody*)b;
		AddPossibleSeparatingAxes(bConvex, Axes);
		

		for(int i = 0; i < Axes.Count(); i++)
		{
			bConvex->AddComparePoints(Axes[i]);
			if(!Axes[i].Overlaps()) return false;
			Axes[i].SetCollisionInfo(info);
		}
		
		Axes.Clear();
		bConvex->AddPossibleSeparatingAxes(this, Axes);
		bool Switch = false;

		for(int i = 0; i < Axes.Count(); i++)
		{
			AddComparePoints(Axes[i]);
			if(!Axes[i].Overlaps()) return false;
			Switch |= Axes[i].SetCollisionInfo(info);
		}
		if(Switch)
		{
			info.A = b;
			info.B = this;
		}

		if(FinalizeCollisionInfo(b, info)) infos.Add(info);
		else return false;

		//info.Normal = -info.Normal;
		//info.Incidence = -info.Incidence;
		//info.Velocity = -info.Velocity;
		//info.A = b;
		//info.B = this;
		//infos.Add(info);
		return true;
	}

	void ConvexBody::AddPossibleSeparatingAxes(ConvexBody* b, List<SeparatingAxisComparer> &Axes)
	{
		for(int i = 0; i < _sides.Count(); i++)
		{
			int point2 = (i == (_sides.Count() - 1)) ? 0 : i + 1;
			Axes.Add(SeparatingAxisComparer((_sides[i].Start - _sides[point2].Start).Perpendicular(), SeparatingAxisComparer::Parallel));
			for(int x = 0; x < _sides.Count(); x++)
				Axes[i].AddPoint(_sides[x].Start);
		}
	}

	void ConvexBody::AddComparePoints(SeparatingAxisComparer &Axis)
	{
		for(int i = 0; i < _sides.Count(); i++)
			Axis.AddComparePoint(_sides[i].Start);
	}
}