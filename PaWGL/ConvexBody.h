#ifndef _ConvexBody_H_
#define _ConvexBody_H_
#include "Body.h"
#include "SeparatingAxisComparer.h"

using namespace Math;

namespace Physics
{
	class SeparatingAxisComparer;
	class ConvexBody : public Body
	{
	protected:
		float _radius;

	protected:
		ConvexBody(const Vector2 position, const float radius, const float mass = 1.0f, const float restitution = 0.9f, const float friction = 0.9f, const bool isStatic = false);

	public:
		ConvexBody(const Vector2 position, const float mass = 1.0f, const float restitution = 0.9f, const float friction = 0.9f, const bool isStatic = false);
		~ConvexBody(void);

		float GetRadius();

		//bool FindCollision(GameTime gameTime, Vector2 gravity, ConvexBody &b, CollisionInfo &info);

		//void BeginTimeStep(GameTime gameTime, Vector2 gravity);
		//void Collide(CollisionInfo &info);
		//void EndTimeStep(GameTime gameTime);

	protected:
		bool Intersects(Body* b, List<CollisionInfo> &infos);
		virtual void AddPossibleSeparatingAxes(ConvexBody* b, List<SeparatingAxisComparer> &Axes);
		virtual void AddComparePoints(SeparatingAxisComparer &Axis);
	};
}

#endif