#include "DampenedSpringMassSystem.h"

namespace Physics
{
	DampenedSpringMassSystem::DampenedSpringMassSystem(Physics::Spring spring, PointMass* massA, PointMass* massB, float damping)
		: Spring(spring), MassA(massA), MassB(massB), Damping(damping)
	{ }

	DampenedSpringMassSystem::~DampenedSpringMassSystem(void) { }

	void DampenedSpringMassSystem::TimeStep(GameTime gameTime)
	{ 
		Vector2 springLength(MassB->GetPosition() - MassA->GetPosition());
		float springLengthLen = springLength.GetLength();
		Vector2 Direction(springLength / springLengthLen);

		float relativeVelocity = gameTime.ElaspedTime * Direction.DotProduct(MassB->GetDisplacement() - MassA->GetDisplacement());
		Vector2 force(Direction * (Spring.CalculateTension(springLengthLen) - (relativeVelocity * Damping)));

		if(MassA->GetIsStatic())
		{ if(!MassB->GetIsStatic()) MassB->IncreaseForce(force); }
		else if(MassB->GetIsStatic()) MassA->IncreaseForce(-force);
		else
		{
			MassA->IncreaseForce(-force);
			MassB->IncreaseForce(force);
		}
	}
}