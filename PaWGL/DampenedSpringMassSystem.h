#ifndef _DampenedSpringMassSystem_H_
#define _DampenedSpringMassSystem_H_

#include "Spring.h"
#include "PointMass.h"

namespace Physics
{
	class DampenedSpringMassSystem
	{
	public:
		Physics::Spring Spring;
		float Damping;
		PointMass* MassA;
		PointMass* MassB;

		DampenedSpringMassSystem(Physics::Spring spring, PointMass* massA, PointMass* massB, float damping = 0.0f);
		~DampenedSpringMassSystem(void);

		void TimeStep(GameTime gameTime);
	};
}

#endif