#include "DampenedVector2SpringMassSystem.h"

namespace Physics
{
	DampenedVector2SpringMassSystem::DampenedVector2SpringMassSystem(Vector2Spring spring, PointMass* massA, PointMass* massB, float damping)
		: Spring(spring), MassA(massA), MassB(massB), Damping(damping)
	{ }

	DampenedVector2SpringMassSystem::~DampenedVector2SpringMassSystem(void) { }

	void DampenedVector2SpringMassSystem::TimeStep(GameTime gameTime)
	{ 
		Vector2 springLength(MassB->GetPosition() - MassA->GetPosition());
		Vector2 relativeVelocity(MassB->GetDisplacement() - MassA->GetDisplacement());
		Vector2 force(Spring.CalculateTension(springLength) - (relativeVelocity * Damping));
		if(MassA->GetIsStatic())
		{ if(!MassB->GetIsStatic()) MassB->IncreaseForce(force); }
		else if(MassB->GetIsStatic()) MassA->IncreaseForce(-force);
		else
		{
			MassA->IncreaseForce(-force);
			MassB->IncreaseForce(force);
		}
	}
}