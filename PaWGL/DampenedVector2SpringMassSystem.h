#ifndef _DampenedVector2SpringMassSystem_H_
#define _DampenedVector2SpringMassSystem_H_

#include "Vector2Spring.h"
#include "PointMass.h"

namespace Physics
{
	class DampenedVector2SpringMassSystem
	{
	public:
		Vector2Spring Spring;
		float Damping;
		PointMass* MassA;
		PointMass* MassB;

		DampenedVector2SpringMassSystem(Vector2Spring spring, PointMass* massA, PointMass* massB, float damping = 0.0f);
		~DampenedVector2SpringMassSystem(void);

		void TimeStep(GameTime gameTime);
	};
}

#endif