#include "DevPlatform.h"
#ifdef PS2
#include "DualShock.h"

namespace Input
{
	DualShock::Buttons DualShock::ButtonsCur()
	{ return _buttonsCur; }
	DualShock::Buttons DualShock::ButtonsPrev()
	{ return _buttonsPrev; }

	DualShock::ButtonPressures DualShock::Pressures()
	{ return _pressures; }

	Vector2 DualShock::RightStick()
	{ return _rightStick; }
	Vector2 DualShock::LeftStick()
	{ return _leftStick; }

	bool DualShock::SupportsVibration()
	{ return _supportsVibration; }

	DualShock::DualShock(int player, int initFlags)
	{ 
		_ID = open("/dev/ps2pad00", O_RDONLY); 
		if(_ID < 0)
		{
			return;
		}
		WaitForReady();
		if (GetStatus() != PS2PAD_STAT_READY)
		{
			close(_ID);
			_ID = -1;
			return;
		}

		if (!SetMode ((initFlags & INIT_ANALOGUE) ? 1 : 0, (initFlags & INIT_LOCK) ? 1 : 0))
		{
			close(_ID);
			_ID = -1;
			return;
		}
		WaitForReady();

		int PressureSupport;
		ioctl(_ID, PS2PAD_IOCPRESSMODEINFO, &PressureSupport);

		if ((initFlags & INIT_ANALOGUE) && (initFlags & INIT_LOCK) && PressureSupport == 1)
		{
			if (!EnterPressureMode())
			{
				SetMode(0, 0);
				close(_ID);
				_ID = -1;
				return;
			}
			WaitForReady();
		}

		//Check For Vibration Support
		struct ps2pad_actinfo act_info;
		act_info.actno = -1;
		act_info.term = 0;
		act_info.result = 0;
		ioctl(_ID, PS2PAD_IOCACTINFO, &act_info);
		_supportsVibration = (act_info.result == 2);

		_initFlags = initFlags;
	}

	DualShock::~DualShock(void)
	{ 
		if (_initFlags & INIT_PRESSURE) EnterPressureMode();
		SetMode(0, 0);
		WaitForReady();
		close(_ID);
		_ID = -1;
	}

	void DualShock::Update()
	{ 
		//Set Previous Data
		_buttonsPrev = _buttonsCur;

		WaitForReady();

		//Get New Data Buffer
		if(read(_ID, _buffer, 32) < 0) return;

		//Read Button Info From Buffer
		_buttonsCur.Select = ~_buffer[2] & SELECT;
		_buttonsCur.L3 = ~_buffer[2] & _L3;
		_buttonsCur.R3 = ~_buffer[2] & _R3;
		_buttonsCur.Start = ~_buffer[2] & START;
		_buttonsCur.Up = ~_buffer[2] & UP;
		_buttonsCur.Right = ~_buffer[2] & RIGHT;
		_buttonsCur.Down = ~_buffer[2] & DOWN;
		_buttonsCur.Left = ~_buffer[2] & LEFT;

		_buttonsCur.L2 = ~_buffer[3] & _L2;
		_buttonsCur.R2 = ~_buffer[3] & _R2;
		_buttonsCur.L1 = ~_buffer[3] & _L1;
		_buttonsCur.R1 = ~_buffer[3] & _R2;
		_buttonsCur.Triangle = ~_buffer[3] & TRIANGLE;
		_buttonsCur.Circle = ~_buffer[3] & CIRCLE;
		_buttonsCur.Cross = ~_buffer[3] & CROSS;
		_buttonsCur.Square = ~_buffer[3] & SQUARE;

		if(_initFlags & INIT_ANALOGUE)
		{
			_rightStick.X = AxisToFloat(_buffer[4]);
			_rightStick.Y = AxisToFloat(_buffer[5]);
			_leftStick.X = AxisToFloat(_buffer[6]);
			_leftStick.Y = AxisToFloat(_buffer[7]);

			if (_initFlags & INIT_PRESSURE)
			{
				_pressures.Left = PressureToFloat(_buffer[8]);
				_pressures.Right = PressureToFloat(_buffer[9]);
				_pressures.Up = PressureToFloat(_buffer[10]);
				_pressures.Down = PressureToFloat(_buffer[11]);
				_pressures.Triangle = PressureToFloat(_buffer[12]);
				_pressures.Circle = PressureToFloat(_buffer[13]);
				_pressures.Cross = PressureToFloat(_buffer[14]);
				_pressures.Square = PressureToFloat(_buffer[15]);
				_pressures.L1 = PressureToFloat(_buffer[16]);
				_pressures.R1 = PressureToFloat(_buffer[17]);
				_pressures.L2 = PressureToFloat(_buffer[18]);
				_pressures.R2 = PressureToFloat(_buffer[19]);
			}
		}
	}

	int DualShock::GetStatus()
	{	
		int res;
		ioctl(_ID, PS2PAD_IOCGETSTAT, &res);
		return res;
	}

	void DualShock::WaitForReady()
	{ while (GetStatus() == PS2PAD_STAT_BUSY) {} }

	bool DualShock::EnterPressureMode()
	{ return (ioctl(_ID, PS2PAD_IOCENTERPRESSMODE) == 0); }

	bool DualShock::ExitPressureMode()
	{ return (ioctl(_ID, PS2PAD_IOCEXITPRESSMODE) == 0); }

	void DualShock::SetVibratorState(bool smallState, bool bigState)
	{
		struct ps2pad_act act;
		
		act.len = 6;
		memset(act.data, -1, sizeof(act.data));

		if (smallState && bigState)
		{
			act.data[0] = 0;
			act.data[1] = 1;
		}
		else if (smallState)
		{
			act.data[0] = 0;
		}
		else if (bigState)
		{
			act.data[1] = 0;
		}

		ioctl(_ID, PS2PAD_IOCSETACTALIGN, &act);
	}

	void DualShock::SetVibratorIntensitys(unsigned char smallIntensity, unsigned char largeItensity)
	{ 	
		struct ps2pad_act act;
		
		act.len = 6;
		memset(act.data, -1, sizeof(act.data));

		if (smallIntensity > 1)	smallIntensity = 1;

		act.data[0] = smallIntensity;	//Small shaker
		act.data[1] = largeItensity;	//Big shaker
		
		ioctl(_ID, PS2PAD_IOCSETACT, &act);
	}

	bool DualShock::SetMode(int mode, int lock)
	{
		struct ps2pad_mode pmode;
		pmode.offs = mode;
		pmode.lock = lock?3:2;
		return (ioctl(_ID, PS2PAD_IOCSETMODE, &pmode) == 0);
	}

	float DualShock::AxisToFloat(unsigned char data)
	{
		if (abs(data - 127) < DRIFT_TOLERANCE) return 0.0f;
		return (((float)data)- 127.5f) * (1.0f / 127.5f);
	}

	float DualShock::PressureToFloat(unsigned char data)
	{ return ((float)data) / 255.0f; }
}

#endif