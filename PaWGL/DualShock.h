#ifndef __DUALSHOCK_H__
#define __DUALSHOCK_H__

#include "Vector2.h"
#include <sys/ioctl.h>
#include <linux/ps2/pad.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <memory.h>
using namespace Math;

namespace Input
{
	class DualShock
	{
	public:
		struct Buttons
		{
			bool Select;
			bool L3;
			bool R3;
			bool Start;
			bool Up;
			bool Right;
			bool Down;
			bool Left;

			bool L2;
			bool R2;
			bool L1;
			bool R1;
			bool Triangle;
			bool Circle;
			bool Cross;
			bool Square;
		};

		struct ButtonPressures
		{
			float Select;
			float L3;
			float R3;
			float Start;
			float Up;
			float Right;
			float Down;
			float Left;

			float L2;
			float R2;
			float L1;
			float R1;
			float Triangle;
			float Circle;
			float Cross;
			float Square;
		};

		//Init Flags	
		#define INIT_DIGITAL 0x00	//Digital mode (default)
		#define INIT_ANALOGUE 0x01	//Analogue mode
		#define INIT_UNLOCK 0x00	//Unlock The Pad
		#define INIT_LOCK 0x02	//Lock The Pad
		#define INIT_PRESSURE 0x04	//Enable Pressure Sensing

	private:
		//Digital Button Bitmask
		#define SELECT 1
		#define _L3 2
		#define _R3 4
		#define START 8
		#define UP 16
		#define RIGHT 32
		#define DOWN 64
		#define LEFT 128

		#define _L2 1
		#define _R2 8
		#define _L1 4
		#define _R1 2
		#define TRIANGLE 16
		#define CIRCLE 32
		#define CROSS 64
		#define SQUARE 128

		#define DRIFT_TOLERANCE 30

		int _ID;
		char _initFlags;
		unsigned char _buffer[32];

		DualShock::Buttons _buttonsCur;
		DualShock::Buttons _buttonsPrev;

		DualShock::ButtonPressures _pressures;

		Vector2 _rightStick;
		Vector2 _leftStick;

		bool _supportsVibration;
		
	public:
		DualShock::Buttons ButtonsCur();
		DualShock::Buttons ButtonsPrev();

		DualShock::ButtonPressures Pressures();

		Vector2 RightStick();
		Vector2 LeftStick();

		bool SupportsVibration();

	public:
		DualShock(int player, int initFlags);
		~DualShock(void);

		void Update();

		int GetStatus();
		void WaitForReady();

		bool EnterPressureMode();
		bool ExitPressureMode();

		void SetVibratorState(bool smallState, bool bigState);
		void SetVibratorIntensitys(unsigned char smallIntensity, unsigned char largeItensity);

		bool SetMode(int mode, int lock);

		static float AxisToFloat(unsigned char data);
		static float PressureToFloat(unsigned char data);
	};
}

#endif