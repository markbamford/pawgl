#ifndef _Event_H_
#define _Event_H_

template<class Arg> struct Event
{
	virtual void Invoke(Arg a); 
	void operator()(Arg a);
};

template <class Arg> void Event<Arg>::Invoke(Arg a)
{ }

template <class Arg> void Event<Arg>::operator()(Arg a)
{ Invoke(a); }

//Member Event
template<class M, class Arg, void(M::*F)(Arg)> struct MemberEvent : Event<Arg>
{ 
private:
	M* _member;
public:
	MemberEvent(const M* member);
	void Invoke(Arg a); 
};

template <class M, class Arg, void(M::*F)(Arg)> MemberEvent<M, Arg, F>::MemberEvent(const M* member) : _member((M*)member)
{ }

template <class M, class Arg, void(M::*F)(Arg)> void MemberEvent<M, Arg, F>::Invoke(Arg a)
{ ((*_member).*F)(a); }

//StaticEvent
template<class Arg> struct StaticEvent : Event<Arg>
{ 
private:
	void(*_function)(Arg);
public:
	StaticEvent(const void(*function)(Arg));
	void Invoke(Arg a); 
};

template <class Arg> StaticEvent<Arg>::StaticEvent(const void(*function)(Arg)) : _function(function)
{ }

template <class Arg> void StaticEvent<Arg>::Invoke(Arg a)
{ (*_function)(a); }

#endif