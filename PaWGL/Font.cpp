#include "DevPlatform.h"
#ifdef PS2
#include <sps2tags.h>
#include "Font.h"
#include "SPS2.h"
#include "DMA.h"
#include <iostream>

namespace Graphics
{
	Font::Font()
	{
		_doubleHeight = false;
		
		_fontWidths = new unsigned char[256];

		for(int i = 0; i < 256; i++)
			_fontWidths[i] = 16;
	}

	Font::~Font()
	{
		if(_fontWidths)
			delete [] _fontWidths;
	}

	bool Font::Load(const char * strWidths, bool DoubleHeight)
	{
		_doubleHeight = DoubleHeight;
		
		// Load the font widths from the file
		FILE * fp = fopen(strWidths, "rb");

		if(fp == NULL) return false;

		if(_fontWidths) delete [] _fontWidths;

		_fontWidths = new unsigned char[256];
		fread(_fontWidths, sizeof(unsigned char), 256, fp);
		fclose(fp);

		return true;
	}

	Texture* Font::GetTexture() { return _texture; }
	void Font::SetTexture(Texture* texture) { _texture = texture; }

	// Get the width of the first line of the string in pixels
	int Font::GetStringWidth(const char * strText)
	{
		size_t textlen = strlen(strText);
		int size = 0;

		for(uint32 letter = 0; letter < textlen; letter++)
		{
			if(strText[letter] == '\n')
				break;

			size += _fontWidths[strText[letter]];
		}

		return size;
	}

	void Font::PrintLeft(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A)
	{
		// How many sprites we have to render
		size_t textlen = strlen(strText);

		if(textlen == 0)
			return;

		_texture->Select();

		VIFDynamicDMA.StartDirect();
		VIFDynamicDMA.StartAD();
		VIFDynamicDMA.AddAD(RGBAQ_SET(R,G,B,A,0), RGBAQ);
		VIFDynamicDMA.AddAD(GS_PRIM(GS_SPRITE, 0, 1, 0, 1, 0, 1, 0, 0), PRIM);
		VIFDynamicDMA.EndAD();

		VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(textlen, 1, 0, 0, GIF_FLG_REGLIST, GS_BATCH_4(GIF_REG_UV,
							 GIF_REG_XYZ2, GIF_REG_UV, GIF_REG_XYZ2)));

		uint32 lastx = x;
		uint32 line = 0;
		uint32 uvOffset = 0;
		uint32 lineHeight = 16;

		if(_doubleHeight)
		{
			uvOffset = 8;
			lineHeight = 32;
		}

		// Loop through all the letters
		for(uint32 letter = 0; letter < textlen; letter++)
		{
			char cLetter = strText[letter];

			// If the letter is 'Newline' move down a line
			if(cLetter == '\n')
			{
				++line;
				lastx = x;
			}

			// The width of the letter in pixels
			int letterwidth = _fontWidths[cLetter];

			// Make the letterwidth even
			letterwidth = (letterwidth % 2) ? letterwidth + 1 : letterwidth;

			// Get the texture coordinates of the letter
			int tx = cLetter % LETTERSPERROW;
			int ty = cLetter / LETTERSPERROW;

			// The texture coordinates of the center of the letter
			int cx = (tx * 16) + 8;
		
			// Work out the texture coordinates from the center of the letter
			int u0 = ((cx - ((letterwidth) / 2)) << 4);
			int v0 = ((ty * 16) << 4) + uvOffset;

			int u1 = ((letterwidth) << 4) + u0;
			int v1 = ((16) << 4) + v0 - uvOffset;

			// Work out the positions of the corners of the sprites
			int left	= lastx;
			int right	= lastx + letterwidth;
			int top		= y + (line * lineHeight);
			int bottom	= y + (line * lineHeight) + lineHeight;

			VIFDynamicDMA.Add64(UV_SET(u0, v0));
			VIFDynamicDMA.Add64(XYZ2_SET(((left + 2048) << 4) - 8,
										 ((top  + 2048) << 4) - 8,
										 0xFFFFFF));
			VIFDynamicDMA.Add64(UV_SET(u1, v1));
			VIFDynamicDMA.Add64(XYZ2_SET(((right  + 2048) << 4) - 8,
										 ((bottom + 2048) << 4) - 8,
										 0xFFFFFF));

			// Move the sprite position forward the size of the letter we just drew
			lastx += letterwidth;
		}

		VIFDynamicDMA.EndDirect();
	}

	void Font::PrintCenter(const char * strText, const int x, const int y, uint8 R = 0x80, uint8 G = 0x80, uint8 B = 0x80, uint8 A = 0x80)
	{
		PrintLeft(strText, x - (GetStringWidth(strText) >> 1), y, R, G, B, A);
	}

	void Font::PrintRight(const char * strText, const int x, const int y, uint8 R = 0x80, uint8 G = 0x80, uint8 B = 0x80, uint8 A = 0x80)
	{
		PrintLeft(strText, x - GetStringWidth(strText), y, R, G, B, A);
	}

	// A printf wrapper that uses our Render function.
	void Font::PrintfLeft(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A, ...)
	{
		// Unpack the extra arguments (...) using the format specificiation in 
		// strText into strBuffer
		va_list args;
		char strBuffer[4096];

		if (!strText)
			return;

		va_start(args, A);
		vsprintf(strBuffer, strText, args);
		va_end(args);

		PrintLeft(strBuffer, x, y, R, G, B, A);
	}

	// A printf wrapper that uses our Render function.
	void Font::PrintfRight(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A, ...)
	{
		// Unpack the extra arguments (...) using the format specificiation in 
		// strText into strBuffer
		va_list args;
		char strBuffer[4096];

		if (!strText)
			return;

		va_start(args, A);
		vsprintf(strBuffer, strText, args);
		va_end(args);

		PrintLeft(strBuffer, x - GetStringWidth(strBuffer), y, R, G, B, A);
	}

	void Font::PrintfCenter(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A, ...)
	{
		// Unpack the extra arguments (...) using the format specificiation in 
		// strText into strBuffer
		va_list args;
		char strBuffer[4096];

		if (!strText)
			return;

		va_start(args, A);
		vsprintf(strBuffer, strText, args);
		va_end(args);

		PrintLeft(strBuffer, x - (GetStringWidth(strBuffer) >> 1), y, R, G, B, A);
	}
}

#endif