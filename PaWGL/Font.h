#ifndef __FONT_H__
#define __FONT_H__

#include "PS2Defines.h"
#include "Texture.h"
#include <stdio.h>
#include <stdarg.h>

namespace Graphics
{
	class Font
	{
		private:
			Texture* _texture;

		public:
			Font();
			~Font();

			bool Load(const char * strWidths, bool DoubleHeight = false);

			Texture* GetTexture();
			void SetTexture(Texture* texture);

			//Left, Right, and Centered font rendering functions
			void PrintLeft(const char * strText, const int x, const int y, uint8 R = 0x80, uint8 G = 0x80, uint8 B = 0x80, uint8 A = 0x80);
			void PrintfLeft(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A, ...);

			void PrintRight(const char * strText, const int x, const int y, uint8 R = 0x80, uint8 G = 0x80, uint8 B = 0x80, uint8 A = 0x80);
			void PrintfRight(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A, ...);

			void PrintCenter(const char * strText, const int x, const int y, uint8 R = 0x80, uint8 G = 0x80, uint8 B = 0x80, uint8 A = 0x80);
			void PrintfCenter(const char * strText, const int x, const int y, uint8 R, uint8 G, uint8 B, uint8 A, ...);

			int GetStringWidth(const char * strText);

		protected:
			unsigned char * _fontWidths;
			bool _doubleHeight;
	};

	#define LETTERSPERROW 16
}

#endif