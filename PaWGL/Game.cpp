#include "Game.h"

Game::Game()
{ }

Game::~Game()
{ delete _graphics; }

GraphicsManager* Game::GetGraphicsManager()
{ return _graphics; }

int Game::Run()
{ return 0; }

void Game::Exit()
{  }

void Game::Initialise(GraphicsManager* graphics)
{ _graphics = graphics; }

void Game::BeginUpdate()
{ }

void Game::BeginDraw()
{	
	Draw(); 
	_graphics->Present();
}

void Game::Update(GameTime gameTime)
{ }

void Game::Draw()
{ }