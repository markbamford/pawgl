#ifndef __Game_H__
#define __Game_H__

#include "GameTime.h"
#include "GraphicsManager.h"

using namespace Graphics;

class Game
{
private:
	GraphicsManager* _graphics;

public:
	Game();
	virtual ~Game();

	GraphicsManager* GetGraphicsManager();

	virtual int Run();
	virtual void Exit();
	void Initialise(GraphicsManager* graphics);
	void BeginUpdate();
	void BeginDraw();
	virtual void Update(GameTime gameTime);
	virtual void Draw();
};

#endif