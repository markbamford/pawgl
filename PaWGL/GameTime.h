#ifndef __GameTime_H__
#define __GameTime_H__

struct GameTime
{
	float TotalElaspedTime;
	float ElaspedTime;

	GameTime(float totalElaspedTime, float elaspedTime);
};

#endif