#include "GameWindow.h"
#include "Game.h"

namespace Windows
{
	bool GameWindow::_keys[256];
	Point GameWindow::_mousePos;
	bool GameWindow::_mouseL;
	bool GameWindow::_mouseR;

	GameWindow::GameWindow(Game* game, LPCSTR title, HINSTANCE hInstance, bool showWindow)
		: Window(hInstance, title, showWindow, 800, 600, (WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN)&~(WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_THICKFRAME)),
		_lastTime(0), _accumulatedTime(0)
	{ _game = game;	}

	GameWindow::~GameWindow(void)
	{ delete _game;	}

	void GameWindow::OnFrameUpdate()
	{
		static const unsigned long timeStart = timeGetTime();
		unsigned long timeNow = timeGetTime();
		unsigned long timeDelta = timeNow - _lastTime;

		if(timeDelta > MaxTimeStep) timeDelta = MaxTimeStep;
		_accumulatedTime += timeDelta;

		Input::States::_keyboardState = KeyboardState(_keys);

		POINT cursorPos;
		int i = 0;
		while(_accumulatedTime >= TimeStep)
		{
			GetCursorPos(&cursorPos);
			Input::States::_mouseState = MouseState(Point(cursorPos.x - _bounds.left, cursorPos.y - _bounds.top), _mouseL, _mouseR);
			_game->Update(GameTime((float)(timeNow - timeStart - (timeDelta - i * TimeStep)) * 0.001f, ((float)TimeStep) * 0.001f));
			_accumulatedTime -= TimeStep;
			i++;
		}

		_lastTime = timeNow;
	}

	void GameWindow::OnFrameDraw()
	{ _game->BeginDraw(); }

	//*********Events*******
	void GameWindow::OnResize(WORD x, WORD y)
	{ 
		Window::OnResize(x, y);
		_game->GetGraphicsManager()->SetBounds(Bounds(_bounds.left, _bounds.top, _bounds.right - _bounds.left, _bounds.bottom - _bounds.top));
	}

	void GameWindow::OnKeyDown(Input::Keys::Enum k)
	{ GameWindow::_keys[k] = true; }

	void GameWindow::OnKeyUp(Input::Keys::Enum k)
	{ GameWindow::_keys[k] = false; }

	void GameWindow::OnLeftMouseDown()
	{ _mouseL = true; }

	void GameWindow::OnRightMouseDown()
	{ _mouseR = true; }

	void GameWindow::OnLeftMouseUp()
	{ _mouseL = false; }

	void GameWindow::OnRightMouseUp()
	{ _mouseR = false; }
}