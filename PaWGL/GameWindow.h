#pragma once
#include "Game.h"
#include "Window.h"
#include "Point.h"
#include "KeyboardState.h"
#include "MouseState.h"
#include "Input.h"
#include <mmsystem.h>
#include <math.h>

class Game;

namespace Windows
{
	class GameWindow : public Window
	{
	private:
		Game* _game;
		unsigned long _lastTime;
		unsigned long _accumulatedTime;
		static const unsigned long MaxTimeStep = 50ul;
		static const unsigned long TimeStep = 5ul; //Number of Milliseconds

		static bool _keys[256];
		static Point _mousePos;
		static bool _mouseL;
		static bool _mouseR;

	public:
		GameWindow(Game* game, LPCSTR title, HINSTANCE hInstance, bool showWindow);
		~GameWindow(void);

	protected:
		void OnShow();
		void OnResize(WORD x, WORD y);
		void OnKeyDown(Keys::Enum k);
		void OnKeyUp(Keys::Enum k);
		void OnLeftMouseDown();
		void OnRightMouseDown();
		void OnLeftMouseUp();
		void OnRightMouseUp();
		void OnFrameUpdate();
		void OnFrameDraw();

	private:
		void SetBuffers();
		void DisplayFrame();
		void ReleaseResources();
	};
}