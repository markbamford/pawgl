#include "GraphicsManager.h"

namespace Graphics
{
	GraphicsManager::GraphicsManager(void)
		: _selectedTexture(-1)
	{ }

	GraphicsManager::~GraphicsManager(void)
	{ for(int i = 0; i < _textures.Count(); i++) delete _textures[i]; }

	Texture* GraphicsManager::TextureFromBMP(const char* filename)
	{
		FILE* file = 0;
		BMPHeader header;
		unsigned int swizzledPalette[256];
		if(LoadBMPFile(filename, file, header, swizzledPalette))
		{
			printf("BMP File Loaded ");
			Texture* t = CreateBMPTexture(file, header, swizzledPalette);
			printf("BMP Texture Created ");
			fclose(file);
			if(t)
			{
				_textures.Add(t);
				return _textures[_textures.Count() - 1];
			}
			else return 0;
		}
		return 0;
	}

	Texture* GraphicsManager::TextureFromTGA(const char* filename)
	{
		FILE* file = 0;
		TGAHeader header;
		if(LoadTGAFile(filename, file, header))
		{
			Texture* t = CreateTGATexture(file, header);
			fclose(file);
			if(t)
			{
				_textures.Add(t);
				return _textures[_textures.Count() - 1];
			}
			else return 0;
		}
		return 0;
	}

	bool GraphicsManager::SelectTexture(int id)
	{ 
		if(id == _selectedTexture) return false;
		_selectedTexture = id;
		return true;
	}

	void GraphicsManager::EnableAlphaBlend() { throw "Not Implemented"; }
	void GraphicsManager::DisableAlphaBlend() { throw "Not Implemented"; }

	void GraphicsManager::EnableTexturing() { throw "Not Implemented"; }
	void GraphicsManager::DisableTexturing() { throw "Not Implemented"; }

	void GraphicsManager::BeginPoints(int verticeCount) { throw "Not Implemented"; }
	void GraphicsManager::BeginLines(int verticeCount) { throw "Not Implemented"; }
	void GraphicsManager::BeginLineStrip(int verticeCount) { throw "Not Implemented"; }
	void GraphicsManager::BeginTriangles(int verticeCount) { throw "Not Implemented"; }
	void GraphicsManager::BeginTriangleStrip(int verticeCount) { throw "Not Implemented"; }
	void GraphicsManager::BeginTriangleFan(int verticeCount) { throw "Not Implemented"; }

	void GraphicsManager::AddNormal(Vector3 normal) { throw "Not Implemented"; }
	void GraphicsManager::AddColor4(Color4 color) { throw "Not Implemented"; }
	void GraphicsManager::AddTextureCoord(Vector2 uv) { throw "Not Implemented"; }
	void GraphicsManager::AddVertex(Vector3 vertex) { throw "Not Implemented"; }

	void GraphicsManager::End() { throw "Not Implemented"; }

	void GraphicsManager::SetBounds(Bounds bounds)
	{
	}

	void GraphicsManager::Present()
	{
	}

	// Returns the base 2 logarithm of the texture size
	// (also a handy way to check for illegal texture sizes)
	unsigned int GraphicsManager::TextureLog2(unsigned int n)
	{
		switch(n)
		{
		case 256:
			return 8;
		case 128:
			return 7;
		case 64:
			return 6;
		case 32:
			return 5;
		default:
			printf("Error: Unsupported Texture Size, %i", n);
		}
	}

	bool GraphicsManager::LoadBMPFile(const char* filename, FILE* &file, BMPHeader &header, unsigned int swizzledPalette[256])
	{
		// Open the file
		file = fopen(filename, "rb");

		if(file == 0)
		{
			printf("Failed to load %s\n", filename);
			return false;
		}

		//Attempt To Read Header
		unsigned char data[14];
		if(fread(&data, 14, 1, file) == 0)
		{
			printf("Could not read file header");
			if(file != 0) fclose(file);
			return false;
		}

		header.BM[0] = data[0];
		header.BM[1] = data[1];
		header.FileSize = data[2] | data[3] << 8 | data[4] << 16 | data[5] << 24;
		header.Reserved1 = data[6] | data[7] << 8;
		header.Reserved2 = data[8] | data[9] << 8;
		header.DataOffset = data[10] | data[11] << 8 | data[12] << 16 | data[13] << 24;


		// Check that this is a bitmap file and someone isn't trying to read
		// some weird file in.
		if(header.BM[0] != 'B' || header.BM[1] != 'M' )
		{
			printf("%s is not a valid bitmap file\n", filename);
			fclose(file);
			return false;
		}

		//Attempt To Read Rest Of Header
		unsigned char data2[40];
		if(fread(&data2, 40, 1, file) == 0)
		{
			printf("Could not read file header");
			if(file != 0) fclose(file);
			return false;
		}

		header.InfoSize = data2[0] | data2[1] << 8 | data2[2] << 16 | data2[3] << 24;
		header.Width = data2[4] | data2[5] << 8 | data2[6] << 16 | data2[7] << 24;
		header.Height = data2[8] | data2[9] << 8 | data2[10] << 16 | data2[11] << 24;
		header.Planes = data2[12] | data2[13] << 8;
		header.BitsPerPixel = data2[14] | data2[15] << 8;
		header.Compression = data2[16] | data2[17] << 8 | data2[18] << 16 | data2[19] << 24;
		header.ImageSize = data2[20] | data2[21] << 8 | data2[22] << 16 | data2[23] << 24;
		header.XResolution = data2[24] | data2[25] << 8 | data2[26] << 16 | data2[27] << 24;
		header.YResolution = data2[28] | data2[29] << 8 | data2[30] << 16 | data2[31] << 24;
		header.ColorPaletteEntriesUsed = data2[32] | data2[33] << 8 | data2[34] << 16 | data2[35] << 24;
		header.ColorPaletteEntriesRequired = data2[36] | data2[37] << 8 | data2[38] << 16 | data2[39] << 24;

		// Make sure that the user isn't trying to load an unsupported colour
		// depth
		if(!(header.BitsPerPixel == 8 || header.BitsPerPixel == 24 || header.BitsPerPixel == 32))
		{
			printf("Invaild Bits Per Pixel %i ", header.BitsPerPixel);
			if(file != 0) fclose(file);
			return false;
		}

		// We will use PSMT8 for 8 bit images, and PSMCT32 for 24 bit images
		//if(header.BitsPerPixel == 24) header.BitsPerPixel == 32;

		// Some image utilities don't set this field, so check first
		if(header.ImageSize == 0)
			header.ImageSize = header.FileSize - header.DataOffset;

		// Next, if we are loading an 8 bit texture we must load
		// a color palette. 
		int palette[256];
		if(header.BitsPerPixel == 8)
		{
			fread(&palette, sizeof(int), 256, file);

			// Loop through each color palette entry and convert it from
			// BGR to RGB, and "swizzle" it
			for(int p = 0; p < 256; p++)
			{	
				unsigned char* pPtr = (unsigned char*)&palette[p];
				unsigned char red = pPtr[2];
				pPtr[2] = pPtr[0];
				pPtr[0] = red;
				pPtr[3] = 0x80;

				// If you look at page 32 of GSUSER_E.pdf you will see
				// a table called "Arrangement of CLUT in IDTEX8" if you
				// look closely you will see that the CLUT is not laid out
				// contiguously. Therefore we must "swizzle" the position so that
				// the clut is placed into memory correctly.
				// (basically what this does is swap bits 3 & 4, see the tutorial
				// doc for more info)
				((unsigned int*)swizzledPalette)[(p & 231) + ((p & 8) << 1) + ((p & 16) >> 1)] = palette[p];
			}
		}
		return true;
	}

	bool GraphicsManager::LoadTGAFile(const char* filename, FILE* &file, TGAHeader &header)
	{
		// TGA image data
		file = fopen(filename, "rb"); // Open file for reading

		if(file == 0)
		{
			printf("Could not open texture file");
			return false;
		}

		//Attempt To Read Header
		unsigned char data[18];
		if(fread(&data, 18, 1, file) == 0)
		{
			printf("Could not read file header");
			if(file != 0) fclose(file);
			return false;
		}

		header.DataOffset = data[0];
		header.ColorType = data[1];
		header.ImageType = data[2];
		header.ColorPaletteOffset = data[3] | data[4] << 8;
		header.ColorPaletteSize = data[5] | data[6] << 8;
		header.ColorPaletteDepth = data[7];
		header.XOffset = data[8] | data[9] << 8;
		header.YOffset = data[10] | data[11] << 8;
		header.Width = data[12] | data[13] << 8;
		header.Height = data[14] | data[15] << 8;
		header.BitsPerPixel = data[16];
		header.Flags = data[17];
		return true;
	}

	Texture* GraphicsManager::CreateBMPTexture(FILE* file, BMPHeader &header, unsigned int swizzledPalette[256])
	{ throw "Not Implemented, Must Call A Derived Class"; }

	Texture* GraphicsManager::CreateTGATexture(FILE* file, TGAHeader &header)
	{ throw "Not Implemented, Must Call A Derived Class"; }
}