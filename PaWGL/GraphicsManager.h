#ifndef __GraphicsManager_H__
#define __GraphicsManager_H__

#include "List.h"
#include "Texture.h"
#include "Bounds.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Color4.h"
#include "BMP.h"
#include "TGA.h"
#include <stdio.h>

using namespace Math;
using namespace FileTypes;

namespace Graphics
{
	class Texture;
	class GraphicsManager
	{
	protected:
		List<Texture*> _textures;
		int _selectedTexture;
		Bounds _bounds;

	public:
		GraphicsManager(void);
		virtual ~GraphicsManager(void);

		Texture* TextureFromBMP(const char* filename);
		Texture* TextureFromTGA(const char* filename);
		virtual bool SelectTexture(int id);

		virtual void EnableAlphaBlend();
		virtual void DisableAlphaBlend();

		virtual void EnableTexturing();
		virtual void DisableTexturing();

		virtual void BeginPoints(int verticeCount);
		virtual void BeginLines(int verticeCount);
		virtual void BeginLineStrip(int verticeCount);
		virtual void BeginTriangles(int verticeCount);
		virtual void BeginTriangleStrip(int verticeCount);
		virtual void BeginTriangleFan(int verticeCount);

		virtual void AddNormal(Vector3 normal);
		virtual void AddColor4(Color4 color);
		virtual void AddTextureCoord(Vector2 uv);
		virtual void AddVertex(Vector3 vertex);

		virtual void End();
		
		virtual void SetBounds(Bounds bounds);
		virtual void Present();

	protected:
		unsigned int TextureLog2(unsigned int n);
		bool LoadBMPFile(const char* filename, FILE* &file, BMPHeader &header, unsigned int swizzledPalette[256]);
		bool LoadTGAFile(const char* filename, FILE* &file, TGAHeader &header);

		virtual Texture* CreateBMPTexture(FILE* file, BMPHeader &header, unsigned int swizzledPalette[256]);
		virtual Texture* CreateTGATexture(FILE* file, TGAHeader &header);
	};
}

#endif