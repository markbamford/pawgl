#ifndef __IDValuePair_H__
#define __IDValuePair_H__

template <class T> class IDValuePair
{
public:
	IDValuePair(T &value, int hash);
	~IDValuePair();
	int ID;
	T* Value;

	template <class U> friend bool operator<(const IDValuePair<T> &a, const IDValuePair<T> &b);
	template <class U> friend bool operator==(const IDValuePair<T> &a, const IDValuePair<T> &b);
};

template <class T> IDValuePair<T>::IDValuePair(T &value, const int id)
{	
	Value = new T(value);
	ID = id;
}

template <class T> IDValuePair<T>::~IDValuePair()
{
	if(Value)
	{
		delete Value;
		Value = 0;
	}
}

template <class U> bool operator<(const IDValuePair<U> &a, const IDValuePair<U> &b)
{
    return a.ID < b.ID;
}

template <class U> bool operator==(const IDValuePair<U> &a, const IDValuePair<U> &b)
{
    return a.ID = b.ID;
}

#endif