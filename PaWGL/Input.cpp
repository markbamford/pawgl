#include "Input.h"

namespace Input
{		
	KeyboardState States::_keyboardState;
	MouseState States::_mouseState;

	KeyboardState States::GetKeyboardState()
	{ return _keyboardState; }

	MouseState States::GetMouseState()
	{ return _mouseState; }
}