#pragma once
#include "GameWindow.h"
#include "MouseState.h"
#include "KeyboardState.h"

namespace Windows{ class GameWindow; };

namespace Input
{
	class States
	{
		friend class Windows::GameWindow;
		static KeyboardState _keyboardState;
		static MouseState _mouseState;

	public:
		static KeyboardState GetKeyboardState();
		static MouseState GetMouseState();
	};	

	//inline bool Input::_keyboard::WasKeyPressed(Keys::Enum k)
	//{ return _keysCur[k] && !_keysPrev[k]; }
	//
	//inline bool Input::_keyboard::WasKeyReleased(Keys::Enum k)
	//{ return !_keysCur[k] && _keysPrev[k]; }

	//inline void Input::_keyboard::SetKey(Keys::Enum k, bool state)
	//{ _keysCur[k] = state; }
	//
	//inline void Input::_keyboard::NewFrame()
	//{ memcpy(&_keysPrev, &_keysCur, 256 * sizeof(bool)); }
}