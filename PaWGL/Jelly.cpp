#include "Jelly.h"

Jelly::Jelly(Simulator2D* simulator, int verticeCount, float elasticity, float damping)
	: _body(Vector2(0.0f, 0.0f), 10.0f, 1.0f, 1.0f), _triangleStrip((verticeCount << 1) + 2), _simulator(simulator), _elasticity(elasticity), _damping(damping)
{
	List<PointMass>* PointMasses = &_body.GetPointMasses();
	for(int i = 0; i < verticeCount; i++)
		PointMasses->Add(PointMass(Vector2(sinf((float)i * 6.28318531f / (float)verticeCount) * 17.0f, cosf((float)i * 6.28318531f / (float)verticeCount) * 17.0f), false, 0.01f));
	_body.SaveShape();

	int Last = PointMasses->Count() - 1;
	for(int i = 0; i < Last; i++)
	{
		_body.GetDampenedVector2SpringMassSystems().Add(DampenedVector2SpringMassSystem(Vector2Spring(((*PointMasses)[i + 1].GetPosition() - (*PointMasses)[i].GetPosition()), _elasticity), &(*PointMasses)[i], &(*PointMasses)[i + 1], _damping));
		_body.GetDampenedVector2SpringMassSystems().Add(DampenedVector2SpringMassSystem(Vector2Spring(((*PointMasses)[i].GetPosition() - _body.GetCenterPointMass().GetPosition()), _elasticity), &_body.GetCenterPointMass(), &(*PointMasses)[i], _damping));
	}
	_body.GetDampenedVector2SpringMassSystems().Add(DampenedVector2SpringMassSystem(Vector2Spring(((*PointMasses)[0].GetPosition() - (*PointMasses)[Last].GetPosition()), _elasticity), &(*PointMasses)[Last], &(*PointMasses)[0], _damping));
	_body.GetDampenedVector2SpringMassSystems().Add(DampenedVector2SpringMassSystem(Vector2Spring(((*PointMasses)[Last].GetPosition() - _body.GetCenterPointMass().GetPosition()), _elasticity), &_body.GetCenterPointMass(), &(*PointMasses)[Last], _damping));

	_simulator->AddBody(&_body);

	_triangleStrip.SetUseAlpha(true);
}

Jelly::~Jelly(void)
{
}

SoftBody* Jelly::GetSoftBody() { return &_body; }

void Jelly::Update(GameTime gameTime)
{
}

void Jelly::Draw(GraphicsManager* graphicsManager, Matrix4x4 transform)
{
	Vertice* v = _triangleStrip.GetVertices();
	if(_body.GetSides().Count() < 3) return;
	for(int i = 0; i < _body.GetSides().Count(); i++)
	{
		v[i << 1].Position = Vector3(_body.GetSides()[i].Start.X, _body.GetSides()[i].Start.Y, 0.01f);
		v[(i << 1) + 1].Position = Vector3(_body.GetCenterPointMass().GetPosition().X, _body.GetCenterPointMass().GetPosition().Y, 0.01f);
	}
	v[_body.GetSides().Count() << 1].Position = Vector3(_body.GetSides()[0].Start.X, _body.GetSides()[0].Start.Y, 0.01f);
	v[(_body.GetSides().Count() << 1) + 1].Position = Vector3(_body.GetCenterPointMass().GetPosition().X, _body.GetCenterPointMass().GetPosition().Y, 0.01f);
	for(int i = 0; i < _triangleStrip.GetVerticeCount(); i+=2) v[i].Color = Color4(0.0f, 1.0f, 0.0f, 0.6f);
	for(int i = 1; i < _triangleStrip.GetVerticeCount(); i+=2) v[i].Color = Color4(0.25f, 1.0f, 0.25f, 1.0f);

	_triangleStrip.Draw(graphicsManager, transform);




	//Debug
	//for(int i = 0; i < _body._shape.Count(); i++)
	//{
	//	v[i << 1].Position = Vector3(_body._shape[i].X + _body._centerMass.GetPosition().X, _body._shape[i].Y + _body._centerMass.GetPosition().Y, 1.0f);
	//	v[(i << 1) + 1].Position = Vector3(_body.GetPosition().X, _body.GetPosition().Y, 1.0f);
	//}
	//v[_body._shape.Count() << 1].Position = Vector3(_body._shape[0].X + _body._centerMass.GetPosition().X, _body._shape[0].Y + _body._centerMass.GetPosition().Y, 1.0f);
	//v[(_body._shape.Count() << 1) + 1].Position = Vector3(_body.GetPosition().X, _body.GetPosition().Y, 1.0f);
	//for(int i = 0; i < _triangleStrip.GetVerticeCount(); i++) v[i].Color = Color4(0.0f, 0.0f, 1.0f, 0.75f);

	

	//for(int i = 0; i < _triangleStrip.GetVerticeCount(); i++) v[i].Color = Color4(0.0f, 0.0f, 1.0f, 0.5f);
	//for(int i = 0; i < _body.GetSides().Count(); i++) _triangleStrip.Draw(graphicsManager, Matrix4x4::Scale(0.1f, 0.1f, 1.0f) * Matrix4x4::Translation(_body.GetSides()[i].Start.X, _body.GetSides()[i].Start.Y, 20.0f) * transform);
	//_triangleStrip.Draw(graphicsManager, Matrix4x4::Scale(0.1f, 0.1f, 1.0f) * Matrix4x4::Translation(_body.GetCenterPointMass().GetPosition().X, _body.GetCenterPointMass().GetPosition().Y, 20.0f) * transform);
	//_triangleStrip.Draw(graphicsManager, Matrix4x4::Scale(0.1f, 0.1f, 1.0f) * Matrix4x4::Translation(_body.GetPosition().X, _body.GetPosition().Y, 1.0f));

	
	//float averageRotation = 0.0f;
 //   for (int i = 0; i < _body._pointMasses.Count(); i++)
	//{
	//	Vector2 relativePosition(_body._pointMasses[i].GetPosition() - _body._centerMass.GetPosition());
	//	float angle = relativePosition.AngleWith(_body._shape[i]);
	//	if(!_body._shape[i].IsCounterClockwise(relativePosition)) averageRotation -= angle;
	//	else averageRotation += angle;
	//}
	//averageRotation /= (float)_body._pointMasses.Count();

	//graphicsManager->BeginLines(_body.GetPointMasses().Count());
	////Rotation
	//graphicsManager->AddColor4(Color4(0.0f, 1.0f, 1.0f, 1.0f));
	//graphicsManager->AddVertex(transform * Vector3(
	//		_body.GetPointMasses()[0].GetPosition().X,
	//		_body.GetPointMasses()[0].GetPosition().Y,
	//		20.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		_body.GetPointMasses()[0].GetPosition().X + averageRotation * 50.0f,
	//		_body.GetPointMasses()[0].GetPosition().Y,
	//		20.0f));

	//graphicsManager->AddColor4(Color4(0.0f, 0.0f, 1.0f, 1.0f));
	//for(int i = 0; i < 1; i++)//_body.GetPointMasses().Count(); i++)
	//{
	//	//for(int i = 0; i < _triangleStrip.GetVerticeCount(); i++) v[i].Color = Color4(1.0f / (float)i, 0.0f, 0.0f, 1.0f);
	//	//_triangleStrip.Draw(graphicsManager, Matrix4x4::Translation(_body.GetCenterPointMass().GetPosition().X * 10.0f - _body.GetDampenedVector2SpringMassSystems()[i].Spring.NaturalLength.X * 10.0f, _body.GetDampenedVector2SpringMassSystems()[i].Spring.NaturalLength.Y * 10.0f, 20.0f) * Matrix4x4::Scale(0.1f, 0.1f, 1.0f) * transform);

	//	//Edge Springs
	//	int pointB = ((i == _body.GetPointMasses().Count() - 1) ? 0 : i + 1);
	//	Vector2 pos((_body.GetPointMasses()[i].GetPosition() + _body.GetPointMasses()[pointB].GetPosition()) * 0.5f);
	//	
	//	graphicsManager->AddColor4(Color4(0.0f, 0.0f, 0.0f, 0.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		pos.X - _body.GetDampenedVector2SpringMassSystems()[(i << 1)].Spring.NaturalLength.X * 0.5f,
	//		pos.Y - _body.GetDampenedVector2SpringMassSystems()[(i << 1)].Spring.NaturalLength.Y * 0.5f,
	//		20.0f));
	//	graphicsManager->AddColor4(Color4(1.0f, 0.0f, 0.0f, 1.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		pos.X + _body.GetDampenedVector2SpringMassSystems()[(i << 1)].Spring.NaturalLength.X * 0.5f,
	//		pos.Y + _body.GetDampenedVector2SpringMassSystems()[(i << 1)].Spring.NaturalLength.Y * 0.5f,
	//		20.0f));

	//	//Shape Springs
	//	graphicsManager->AddColor4(Color4(0.0f, 0.0f, 0.0f, 0.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		_body.GetCenterPointMass().GetPosition().X,
	//		_body.GetCenterPointMass().GetPosition().Y,
	//		20.0f));
	//	graphicsManager->AddColor4(Color4(1.0f, 0.0f, 0.0f, 1.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		_body.GetCenterPointMass().GetPosition().X + _body.GetDampenedVector2SpringMassSystems()[(i << 1) + 1].Spring.NaturalLength.X,
	//		_body.GetCenterPointMass().GetPosition().Y + _body.GetDampenedVector2SpringMassSystems()[(i << 1) + 1].Spring.NaturalLength.Y,
	//		20.0f));

	//	//Displacement
	//	graphicsManager->AddColor4(Color4(1.0f, 0.0f, 1.0f, 1.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		_body.GetPointMasses()[i].GetPosition().X,
	//		_body.GetPointMasses()[i].GetPosition().Y,
	//		20.0f));
	//	graphicsManager->AddVertex(transform * Vector3(
	//		_body.GetPointMasses()[i].GetPosition().X - _body.GetPointMasses()[i].GetDisplacement().X * 10.0f,
	//		_body.GetPointMasses()[i].GetPosition().Y - _body.GetPointMasses()[i].GetDisplacement().Y * 10.0f,
	//		20.0f));

	//	graphicsManager->AddColor4(Color4(1.0f, 0.0f, 0.0f, 1.0f));
	//}
	//graphicsManager->End();

	////Point Masses
	//graphicsManager->BeginPoints(0);
	//graphicsManager->AddColor4(Color4(1.0f, 1.0f, 1.0f, 1.0f));
	//for(int i = 0; i < _body.GetPointMasses().Count(); i++) 
	//	graphicsManager->AddVertex(transform * Vector3(
	//	_body.GetPointMasses()[i].GetPosition().X,
	//	_body.GetPointMasses()[i].GetPosition().Y,
	//	20.0f));
	//graphicsManager->End();


	//_triangleStrip.Draw(graphicsManager, Matrix4x4::Translation(_body.GetCenterPointMass().GetPosition().X * 10.0f, _body.GetCenterPointMass().GetPosition().Y * 10.0f, 1.0f) * Matrix4x4::Scale(0.1f, 0.1f, 1.0f));

	//for(int i = 0; i < _triangleStrip.GetVerticeCount(); i++) v[i].Color = Color4(0.0f, 1.0f, 0.0f, 0.5f);
	//_triangleStrip.Draw(graphicsManager, transform);
}