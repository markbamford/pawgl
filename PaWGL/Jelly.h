#ifndef __Jelly_H__
#define __Jelly_H__

#include "SoftBody.h"
#include "TriangleStrip.h"
#include "List.h"
#include "Simulator2D.h"
#include "Matrix4x4.h"

using namespace Physics;
using namespace Graphics;

class Jelly
{
private:
	Simulator2D* _simulator;
	SoftBody _body;
	float _elasticity;
	float _damping;
	TriangleStrip _triangleStrip;

public:
	Jelly(Simulator2D* simulator, int verticeCount, float elasticity = 50.0f, float damping = 15.0f);
	~Jelly(void);

	SoftBody* GetSoftBody();

	void Update(GameTime gameTime);
	void Draw(GraphicsManager* graphicsManager, Matrix4x4 transform = Matrix4x4::IDENTITY);
};

#endif