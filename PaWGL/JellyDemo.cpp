#include "JellyDemo.h"

JellyDemo::JellyDemo(
#ifdef OPENGL 
HINSTANCE hInstance)
	: MultiplatformGame(hInstance, "Gloo")
#endif
#ifdef PS2 
)
	: MultiplatformGame()
#endif
	,_gameState(GameState::Menu)
{ 
#ifdef PS2
	Music = new SoundSample("Music", &PS2Game::AudioDevice0());
	Music->PlayLoop();
#endif

	_menu = new Menu(GetGraphicsManager(), &_gameState);
	_level = new Level(GetGraphicsManager(), &_gameState); 
}

JellyDemo::~JellyDemo(void)
{ 
	delete _menu;
	delete _level; 
}

void JellyDemo::Update(GameTime gameTime)
{	
	switch(_gameState)
	{
	case GameState::NewGame:
		delete _level;
		_level = new Level(GetGraphicsManager(), &_gameState); 
		_gameState = GameState::Menu;
		break;
	case GameState::Game:
		_level->Update(gameTime);
		break;
	default:
		_menu->Update(gameTime);
		break;
	}
}

float Light_Ambient[] = {1.0f, 1.0f, 1.0f, 1.0f};
float Light_Diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
float Light_Position[]= {0.0f, 0.0f, 6.0f, 1.0f};

void JellyDemo::Draw()
{ 
	switch(_gameState)
	{
	case GameState::Game:
		_level->Draw(GetGraphicsManager());
		break;
	default:
		_menu->Draw(GetGraphicsManager());
		break;
	}
}