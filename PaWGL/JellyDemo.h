#ifndef __JellyDemo_H__
#define __JellyDemo_H__

#include "MultiplatformGame.h"
#include "Menu.h"
#include "Level.h"
#ifdef PS2
#include "PS2Audio.h"
#endif

class JellyDemo : public MultiplatformGame
{
private:
	GameState::GameStateEnum _gameState;
	Menu* _menu;
	Level* _level;
#ifdef PS2
	SoundSample* Music;
#endif

public:
#ifdef OPENGL
	JellyDemo(HINSTANCE hInstance);
#endif
#ifdef PS2
	JellyDemo(void);
#endif
	~JellyDemo(void);

	void Update(GameTime gameTime);
	void Draw();
};

#endif