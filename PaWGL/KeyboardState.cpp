#include "KeyboardState.h"

namespace Input
{
	KeyboardState::KeyboardState()
	{ }

	KeyboardState::KeyboardState(const bool keys[256])
	{ memcpy(_keys, keys, 256 * sizeof(bool)); }	

	bool KeyboardState::IsKeyDown(Keys::Enum k)
	{ return _keys[k]; }

	bool KeyboardState::IsKeyUp(Keys::Enum k)
	{ return !_keys[k]; }
}