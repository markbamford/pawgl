#pragma once
#include "Keys.h"
#include <string>

namespace Input
{
	struct KeyboardState
	{
	private:
		bool _keys[256];

	public:
		KeyboardState();
		KeyboardState(const bool keys[256]);

		bool IsKeyDown(Keys::Enum k);
		bool IsKeyUp(Keys::Enum k);
	};
}