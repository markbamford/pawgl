#include "Level.h"

Level::Level(GraphicsManager* graphics, GameState::GameStateEnum* gameState)
	: _simulator(Vector2(0.0f, 90.0f)),
	_background(800, 600),
	_playerJelly(&_simulator, 36, 50.0f, 20.0f),
	_playerCollideEvent(this),
	_winTriggerAreaEvent(this),
	_gameState(gameState)
{
	_background.SetTexture(graphics->TextureFromBMP("SkyStars.bmp"));
	_background.SetUpperUV(Vector2(8.0f, 3.0f));

	//Scenery
	//_scenery.Add(new Scenery(Quad(graphics->TextureFromBMP("MetalMesh.bmp"), 600, 50, true), Vector3(200.0f, 10.0f, 0.008f)));
	//_scenery[0]->GetQuad().SetUpperUV(Vector2(12.0f, 1.0f));
	_scenery.Add(new Scenery(Quad(graphics->TextureFromBMP("MetalBarHorz.bmp"), 100.0f, 15.0f), Vector3(-50.0f, 30.0f, 0.009f)));
	_scenery.Add(new Scenery(Quad(graphics->TextureFromBMP("MetalBarHorz.bmp"), 350.0f, 15.0f), Vector3(175.0f, 30.0f, 0.009f)));
	_scenery.Add(new Scenery(Quad(graphics->TextureFromBMP("MetalBarHorz.bmp"), 350.0f, 15.0f), Vector3(525.0f, 30.0f, 0.009f)));
	_scenery.Add(new Scenery(Quad(graphics->TextureFromBMP("MetalBarHorz.bmp"), 350.0f, 15.0f), Vector3(875.0f, 30.0f, 0.009f)));
	_scenery.Add(new Scenery(Quad(graphics->TextureFromBMP("MetalBarHorz.bmp"), 350.0f, 15.0f), Vector3(1225.0f, 30.0f, 0.009f)));

	//Floor
	_platforms.Add(new Platform(&_simulator, Vector3(650.0f, 30.0f, 0.009f), Vector2(1500.0f, 15.0f)));
	_platforms[0]->GetQuad()->SetTexture(graphics->TextureFromBMP("MetalBarHorz.bmp"));
	_platforms[0]->GetQuad()->SetUpperUV(Vector2(10.0f, 1.0f));

	//Walls
	Texture* wallTex = graphics->TextureFromBMP("MetalWall.bmp");

	_platforms.Add(new Platform(&_simulator, Vector3(400.0f, 40.0f, 0.01f), Vector2(100.0f, 200.0f)));
	_platforms[1]->GetQuad()->SetTexture(wallTex);
	_platforms[1]->GetQuad()->SetUpperUV(Vector2(2.0f, 4.0f));

	_platforms.Add(new Platform(&_simulator, Vector3(500.0f, -40.0f, 0.01f), Vector2(100.0f, 200.0f)));
	_platforms[2]->GetQuad()->SetTexture(wallTex);
	_platforms[2]->GetQuad()->SetUpperUV(Vector2(2.0f, 4.0f));

	_platforms.Add(new Platform(&_simulator, Vector3(700.0f, -40.0f, 0.01f), Vector2(100.0f, 200.0f)));
	_platforms[3]->GetQuad()->SetTexture(wallTex);
	_platforms[3]->GetQuad()->SetUpperUV(Vector2(2.0f, 4.0f));

	_platforms.Add(new Platform(&_simulator, Vector3(1000.0f, -175.0f, 0.01f), Vector2(100.0f, 200.0f)));
	_platforms[4]->GetQuad()->SetTexture(wallTex);
	_platforms[4]->GetQuad()->SetUpperUV(Vector2(2.0f, 4.0f));

	_platforms.Add(new Platform(&_simulator, Vector3(1200.0f, -40.0f, 0.01f), Vector2(100.0f, 200.0f)));
	_platforms[5]->GetQuad()->SetTexture(wallTex);
	_platforms[5]->GetQuad()->SetUpperUV(Vector2(2.0f, 4.0f));


	//Wall Tops
	_surfaces.Add(new Surface(&_simulator, 8, Vector3(150.0f, 30.0f, 1.0f), Vector2(50.0f, 50.0f)));
	_surfaces[0]->GetTriangleStrip()->SetTexture(wallTex);

	_surfaces.Add(new Surface(&_simulator, 8, Vector3(700.0f, -140.0f, 1.0f), Vector2(50.0f, 50.0f)));
	_surfaces[1]->GetTriangleStrip()->SetTexture(wallTex);

	//Trigger Areas
	_triggerAreas.Add(new TriggerArea(Vector2(1000.0f, -301.0f), Vector2(30.0f, 30.0f)));
	_triggerAreas[0]->SetCollideEvent(&_winTriggerAreaEvent);
	_simulator.AddBody(_triggerAreas[0]);

	//Player
	_playerJelly.GetSoftBody()->SetCollideEvent(&_playerCollideEvent);
}

float Level::Light_Ambient[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float Level::Light_Diffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float Level::Light_Position[4]= {0.0f, 0.0f, 6.0f, 1.0f};

Level::~Level(void)
{
}

void Level::WinTrigger(CollisionInfo info)
{ (*_gameState) = GameState::Win; }

bool playerUpdated;
#ifdef OPENGL
KeyboardState prev;
#endif
void Level::PlayerCollide(CollisionInfo info)
{
	Vector2 velocity;
#ifdef OPENGL
	KeyboardState k = Input::States::GetKeyboardState();
	if(k.IsKeyDown(Input::Keys::Control)) velocity -= info.Normal * 0.012f;
#endif
#ifdef PS2
	if(PS2Game::Player(0).ButtonsCur().R1) velocity -= info.Normal * 0.012f;
#endif

	_playerJelly.GetSoftBody()->GetDisplacement() += velocity;

	if(!playerUpdated)
	{
#ifdef OPENGL
 		if(k.IsKeyDown(Input::Keys::Space) && prev.IsKeyUp(Input::Keys::Space)) 
			_playerJelly.GetSoftBody()->GetDisplacement() += info.Normal * 0.5f;
#endif
#ifdef PS2
		if(PS2Game::Player(0).ButtonsCur().L1 && !PS2Game::Player(0).ButtonsPrev().L1) 
			_playerJelly.GetSoftBody()->GetDisplacement() += info.Normal * 0.5f;
#endif
		playerUpdated = true;
	}
}

void Level::Update(GameTime gameTime)
{
	playerUpdated = false;
#ifdef OPENGL
	KeyboardState k = Input::States::GetKeyboardState();
	if(k.IsKeyDown(Input::Keys::Escape))
	{
		(*_gameState) = GameState::Menu;
		return;
	}
	if(k.IsKeyDown(Input::Keys::Right)) _playerJelly.GetSoftBody()->GetRotationalDisplacement() += 0.0025f;
	if(k.IsKeyDown(Input::Keys::Left)) _playerJelly.GetSoftBody()->GetRotationalDisplacement() -= 0.0025f;
	
#endif

#ifdef PS2
	if(PS2Game::Player(0).ButtonsCur().Start)
	{
		(*_gameState) = GameState::Menu;
		return;
	}
	//if(PS2Game::Player(0).ButtonsCur().Right) _playerJelly.GetSoftBody()->GetRotationalDisplacement() += 0.0025f;
	//if(PS2Game::Player(0).ButtonsCur().Left) _playerJelly.GetSoftBody()->GetRotationalDisplacement() -= 0.0025f;
	_playerJelly.GetSoftBody()->GetRotationalDisplacement() += 0.0025f * PS2Game::Player(0).LeftStick().X;
#endif

	_simulator.Update(gameTime); 

	if(_playerJelly.GetSoftBody()->GetCenterPointMass().GetPosition().Y > 50.0f)
		(*_gameState) = GameState::Fail;
#ifdef OPENGL
	prev = k;
#endif
}

void Level::Draw(GraphicsManager* graphics)
{
#ifdef OPENGL
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear The Screen And The Depth Buffer
	glLoadIdentity();// load Identity Matrix

	//gluPerspective(45.0f, 4.0f / 3.0f, 1, 500.0f);

	//set camera looking down the -z axis,  6 units away from the center
	gluLookAt(0, 0, 108,     0, 0, -1,     0, -1, 0); //Where we are, What we look at, and which way is up
	
	glLightfv(GL_LIGHT1, GL_AMBIENT,  Light_Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE,  Light_Diffuse);
	//glLightfv(GL_LIGHT1, GL_POSITION, Light_Position);
	glEnable(GL_LIGHT1);
#endif

	Matrix4x4 Camera = Matrix4x4::Translation(-_playerJelly.GetSoftBody()->GetPosition().X, -_playerJelly.GetSoftBody()->GetPosition().Y + 30.0f, 1.0f) * Matrix4x4::Scale(3.5f, 3.5f, 1.0f);

	_background.Draw(graphics, Matrix4x4::Translation(0,0, 0.9f));
	for(int i = 0; i < _scenery.Count(); i++) _scenery[i]->Draw(graphics, Camera);
	for(int i = 0; i < _platforms.Count(); i++) _platforms[i]->Draw(graphics, Camera);
	for(int i = 0; i < _surfaces.Count(); i++) _surfaces[i]->Draw(graphics, Camera);
	for(int i = 0; i < _jellys.Count(); i++) _jellys[i]->Draw(graphics, Camera);
	_playerJelly.Draw(graphics, Camera);
}