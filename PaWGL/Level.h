#ifndef __Level_H__
#define __Level_H__

#include "MultiplatformGame.h"
#include "Simulator2D.h"
#include "Jelly.h"
#include "Platform.h"
#include "Surface.h"
#include "Scenery.h"
#include "TriggerArea.h"
#include "Event.h"
#include "CollisionInfo.h"
#include "GameState.h"

using namespace Physics;

class Level;
class Level
{
private:
	Simulator2D _simulator;
	Quad _background;
	List<Platform*> _platforms;
	List<Surface*> _surfaces;
	List<Jelly*> _jellys;
	List<Scenery*> _scenery;
	List<TriggerArea*> _triggerAreas;

	Jelly _playerJelly;

	GameState::GameStateEnum* _gameState;

	static float Light_Ambient[4];
	static float Light_Diffuse[4];
	static float Light_Position[4];

public:
	Level(GraphicsManager* graphics, GameState::GameStateEnum* gameState);
	~Level(void);

	void PlayerCollide(CollisionInfo info);
	void WinTrigger(CollisionInfo info);

	void Update(GameTime gameTime);
	void Draw(GraphicsManager* graphics);

private:
	MemberEvent<Level, CollisionInfo, &Level::PlayerCollide> _playerCollideEvent;
	MemberEvent<Level, CollisionInfo, &Level::WinTrigger> _winTriggerAreaEvent;
};

#endif
