#include "Line2D.h"

namespace Math
{
	Line2D::Line2D(const Vector2& pointA, const Vector2& pointB)
		: Start(pointA), Direction((pointB - pointA).Normalize()), Normal(Direction.Perpendicular())
	{ }

	Vector2 Line2D::ClosestPoint(const Vector2& point)
	{
		Vector2 diff = point - Start;
		float t = diff.DotProduct(Direction) / Direction.DotProduct(Direction);
		return Start + (Direction * t);
	}

	float Line2D::ShortestDistanceToPoint(const Vector2& point)
	{ return (point - ClosestPoint(point)).GetLength();}

	float Line2D::ShortestDistanceSquaredToPoint(const Vector2& point)
	{ return (point - ClosestPoint(point)).GetLengthSquared();}
}