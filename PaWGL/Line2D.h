#ifndef _Line2D_H_
#define _Line2D_H_

#include "MathEx.h"
#include "Vector2.h"

namespace Math
{
	struct Line2D
	{
		Vector2 Start;
		Vector2 Direction;
		Vector2 Normal;

		Line2D(const Vector2& pointA, const Vector2& pointB);

		Vector2 ClosestPoint(const Vector2& point);
		float ShortestDistanceToPoint(const Vector2& point);
		float ShortestDistanceSquaredToPoint(const Vector2& point);
	};
}

#endif