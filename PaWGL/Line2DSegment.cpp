#include "Line2DSegment.h"

namespace Math
{
	Line2DSegment::Line2DSegment(Vector2 pointA, Vector2 pointB)
		: Start(pointA), Direction(pointB - pointA), Normal(Direction.Perpendicular())
	{ }

	Vector2 Line2DSegment::ClosestPoint(Vector2 point)
	{
		Vector2 diff = point - Start;
		float t = diff.DotProduct(Direction) / Direction.DotProduct(Direction);
		if (t <= 0.0f) return Start;
		else if (t >= 1.0f) return Start + Direction;
		else return Start + (Direction * t);
	}

	float Line2DSegment::ShortestDistanceToPoint(Vector2 point)
	{ return (point - ClosestPoint(point)).GetLength(); }

	float Line2DSegment::ShortestDistanceSquaredToPoint(Vector2 point)
	{ return (point - ClosestPoint(point)).GetLengthSquared(); }
}