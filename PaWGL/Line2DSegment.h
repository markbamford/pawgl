#ifndef _Line2DSegment_H_
#define _Line2DSegment_H_

#include "MathEx.h"
#include "Vector2.h"

namespace Math
{
	struct Line2DSegment
	{
		Vector2 Start;
		Vector2 Direction;
		Vector2 Normal;

		Line2DSegment(Vector2 pointA, Vector2 pointB);

		Vector2 ClosestPoint(Vector2 point);
		float ShortestDistanceToPoint(Vector2 point);
		float ShortestDistanceSquaredToPoint(Vector2 point);
	};
}

#endif