#ifndef __LIST_H__
#define __LIST_H__

#include <algorithm>

/*To Use Sort() overload < operator in your class...

friend bool operator<(const YOURCLASS<T> &a, const YOURCLASS<T> &b);
bool operator<(const YOURCLASS& a, const YOURCLASS& b)
{
    return a.YOURVALUE < b.YOURVALUE;
}
*/

template <class T> class List
{
private:
	int _count;
	int _capacity;
	T** _list;

public:
	List(void);
	List(const List &o);
	List(int capacity);
	~List(void);

	List<T>& operator=(const List<T>& o);
	T& operator[] (const int index);

	void Add(const T &item);
	void AddAfter(int index, const T &item);
	void AddBefore(int index, const T &item);
	void Remove(const T &item);
	void RemoveAt(int index);
	void Clear();
	int IndexOf(const T &item);
	int Count();
	void Sort();
	void Shuffle();
	int GetCapacity();
	void SetCapacity(int capacity);
	T* GetArray();

private:
	void FixCapacity();
	static const int DEFAULTCAPACITY = 8;
};

template <class T> List<T>::List(void) : _count(0),	_capacity(DEFAULTCAPACITY), _list((T**)calloc(DEFAULTCAPACITY, sizeof(T*)))
{ if (_list == 0) throw "Unable To Allocate Memory"; }

template <class T> List<T>::List(int capacity) : _count(0),	_capacity(capacity), _list((T**)calloc(_capacity, sizeof(T*)))
{ if (_list == 0) throw "Unable To Allocate Memory"; }

template <class T> List<T>::List(const List &o) : _count(o._count),	_capacity(o._capacity), _list((T**)calloc(_capacity, sizeof(T*)))
{ for(int i = 0; i < _count; i++) _list[i] = new T(*o._list[i]); }

template <class T> List<T>& List<T>::operator=(const List<T>& o)
{ 
	for(int i = 0; i < _capacity; i++)
		delete _list[i];
	free(_list);

	_count = o._count;
	_capacity = o._capacity;
	_list = (T**)calloc(_capacity, sizeof(T*));
	for(int i = 0; i < _count; i++) _list[i] = new T(*o._list[i]);
	return *this; 
}

template <class T> List<T>::~List(void)
{ 
	for(int i = 0; i < _capacity; i++)
		delete _list[i];
	free(_list);
}

template <class T> T& List<T>::operator[] (const int index)
{
	if(index < 0 || index >= _count) throw "Index Out Of Range!";
    return *_list[index];
}

template <class T> void List<T>::Add(const T &item)
{
	FixCapacity();
	_list[_count] = new T(item);
	_count++;
}

template <class T> void List<T>::AddAfter(int index, const T &item)
{
	if(index < 0 || index >= _count) throw "Index Out Of Range!";
	FixCapacity();
	memmove(&_list[index + 2], &_list[index + 1], (_count - (index + 1)) * sizeof(T*));
	_list[index] = new T(item);
	_count++;
}

template <class T> void List<T>::AddBefore(int index, const T &item)
{
	if(index < 0 || index >= _count) throw "Index Out Of Range!";
	FixCapacity();
	memmove(&_list[index + 1], &_list[index], (_count - index) * sizeof(T*));
	_list[index] = new T(item);
	_count++;
}

template <class T> void List<T>::Remove(const T &item)
{ RemoveAt(IndexOf(item)); }

template <class T> void List<T>::RemoveAt(int index)
{
	if(index < 0 || index >= _count) throw "Index Out Of Range!";
	delete _list[index];
	memmove(&_list[index], &_list[index + 1], (_count - (index + 1)) * sizeof(T*));
	_count--;
	_list[_count] = 0;
}

template <class T> void List<T>::Clear()
{	
	if(_count == 0) return;
	for(int i = 0; i < _count; i++)
		delete _list[i];
	memset(_list, 0, _count * sizeof(T*));
	_count = 0;
}

template <class T> int List<T>::IndexOf(const T &item)
{ 
	for(int i = 0; i < _count; i++) if(*_list[i] == item) return i;
	throw "Item Not Found!";
}

template <class T> int List<T>::Count()
{ return _count; }

template <class T> void List<T>::Sort()
{ if(_count > 1) std::sort<T*>(_list[0], _list[_count - 1]); }

template <class T> void List<T>::Shuffle()
{ if(_count > 1) std::random_shuffle<T*>(_list[0], _list[_count - 1]); }

template <class T> int List<T>::GetCapacity()
{ return _capacity; }

template <class T> void List<T>::SetCapacity(int capacity)
{
	if(capacity < 0) throw "Capacity Cannot Be Less Than Zero!";
	T** temp = (T**)realloc(_list, capacity * sizeof(T*));
	if (temp == 0) throw "Unable To Allocate Memory";
	_list = temp;
	if(_capacity < capacity) memset(&_list[_capacity], 0, (capacity - _capacity) * sizeof(T*));
	_capacity = capacity;
}

template <class T> void List<T>::FixCapacity()
{ if(_count == _capacity) SetCapacity(_capacity << 1); }

template <class T> T* List<T>::GetArray()
{ 
	T* _array = (T*)calloc(_count, sizeof(T));
	for(int i = 0; i < _count; i++) memcpy(&_array[i], _list[i], sizeof(T));
	return _array;
}
#endif