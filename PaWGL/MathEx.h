#ifndef __MathEx_H__
#define __MathEx_H__

#include "DevPlatform.h"

#define TWOPI 6.283185307179586476925286766559f
#define PI 3.1415926535897932384626433832795f
#define PIHALF 1.5707963267948966192313216916398f
#define PIDIV4 0.78539816339744830961566084581988f
#ifdef PS2
float asinf(float x);
float cosf(float v);
float abs(const float x);
float sqrt(const float x);
float max(const float a, const float b);
float min(const float a, const float b);
float acosf(float x);
float sinf(float v);

float DegToRad(float Deg);
float mod(const float a, const float b);
#else
#include <cmath>
#endif

#endif