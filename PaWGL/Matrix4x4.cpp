#include "Matrix4x4.h"

namespace Math
{
	const Matrix4x4 Matrix4x4::IDENTITY(1.0f,0.0f,0.0f,0.0f,
										0.0f,1.0f,0.0f,0.0f,
										0.0f,0.0f,1.0f,0.0f,
										0.0f,0.0f,0.0f,1.0f);

	const Matrix4x4 Matrix4x4::NULLMATRIX(0.0f,0.0f,0.0f,0.0f,
										  0.0f,0.0f,0.0f,0.0f,
										  0.0f,0.0f,0.0f,0.0f,
										  0.0f,0.0f,0.0f,0.0f);

	Matrix4x4::Matrix4x4(void)
	{
		memcpy(elem, NULLMATRIX.elem, sizeof(float) * 16);
	}

	Matrix4x4::~Matrix4x4(void)
	{
	}

	Matrix4x4::Matrix4x4(const Matrix4x4 & rhs)
	{
		memcpy(elem, rhs.elem, sizeof(float) * 16);
	}

	Matrix4x4::Matrix4x4(float _11, float _12, float _13, float _14,
						 float _21, float _22, float _23, float _24,
						 float _31, float _32, float _33, float _34,
						 float _41, float _42, float _43, float _44)
	{
		elem[0][0] = _11;	elem[0][1] = _12;	elem[0][2] = _13;	elem[0][3] = _14;
		elem[1][0] = _21;	elem[1][1] = _22;	elem[1][2] = _23;	elem[1][3] = _24;
		elem[2][0] = _31;	elem[2][1] = _32;	elem[2][2] = _33;	elem[2][3] = _34;
		elem[3][0] = _41;	elem[3][1] = _42;	elem[3][2] = _43;	elem[3][3] = _44;
	}

	Matrix4x4::Matrix4x4(float _elem[][4])
	{
		memcpy(elem, _elem, sizeof(float) * 16);
	}

	Matrix4x4 operator*( const Matrix4x4 &M1,
						 const Matrix4x4 &M2)
	{
		Matrix4x4 ret;

		for(int i = 0; i < 4; i++)
		{
			for(int j = 0; j < 4; j++)
			{
				float Value = 0;
	          
				for(int k = 0; k < 4; k++)
				{
					Value += M1(i,k) * M2(k,j);
				}

				ret(i,j) = Value;
			}
		}

		return ret;
	}

	Vector4 operator * ( const Matrix4x4 &M,
						 const Vector4 &V )
	{
		Vector4 ret;

		ret.X = M(0,0) * V.X + M(1,0) * V.Y + M(2,0) * V.Z + M(3,0) * V.W;
		ret.Y = M(0,1) * V.X + M(1,1) * V.Y + M(2,1) * V.Z + M(3,1) * V.W;
		ret.Z = M(0,2) * V.X + M(1,2) * V.Y + M(2,2) * V.Z + M(3,2) * V.W;
		ret.W = M(0,3) * V.X + M(1,3) * V.Y + M(2,3) * V.Z + M(3,3) * V.W;

		return ret;
	}

	Vector3 operator*( const Matrix4x4 & M,
						 const Vector3 & V )
	{ 
		Vector3 ret;
		ret.X = M(0,0) * V.X + M(1,0) * V.Y + M(2,0) * V.Z + M(3,0);
		ret.Y = M(0,1) * V.X + M(1,1) * V.Y + M(2,1) * V.Z + M(3,1);
		ret.Z = M(0,2) * V.X + M(1,2) * V.Y + M(2,2) * V.Z + M(3,2);
		return ret;
	}

	Vector2 operator*( const Matrix4x4 & M,
						 const Vector2 & V )
	{ 
		Vector2 ret;
		ret.X = M(0,0) * V.X + M(1,0) * V.Y + M(2,0) + M(3,0);
		ret.Y = M(0,1) * V.X + M(1,1) * V.Y + M(2,1) + M(3,1);
		return ret;
	}

	Matrix4x4 Matrix4x4::Translation(Vector3 v)
	{ return Matrix4x4::Translation(v.X, v.Y, v.Z); }

	Matrix4x4 Matrix4x4::Translation(float X, float Y, float Z)
	{
		Matrix4x4 m;
		memcpy(m.elem, IDENTITY.elem, sizeof(float) * 16);

		m.elem[3][0] = X;
		m.elem[3][1] = Y;
		m.elem[3][2] = Z;
		return m;
	}

	Matrix4x4 Matrix4x4::Scale(float S)
	{
		Matrix4x4 m;
		m.elem[0][0] = S;
		m.elem[1][1] = S;
		m.elem[2][2] = S;
		m.elem[3][3] = 1;
		return m;
	}

	Matrix4x4 Matrix4x4::Scale(float X, float Y, float Z)
	{
		Matrix4x4 m;
		m.elem[0][0] = X;
		m.elem[1][1] = Y;
		m.elem[2][2] = Z;
		m.elem[3][3] = 1;
		return m;
	}

	Matrix4x4 Matrix4x4::RotationX(float fAngle)
	{
		Matrix4x4 m;
		memcpy(m.elem, IDENTITY.elem, sizeof(float) * 16);
		float c = cosf(fAngle);
		float s = sinf(fAngle);
		m.elem[1][1] = c;
		m.elem[1][2] = s;
		m.elem[2][1] = -s;
		m.elem[2][2] = c;
		return m;
	}

	Matrix4x4 Matrix4x4::RotationY(float fAngle)
	{
		Matrix4x4 m;
		memcpy(m.elem, IDENTITY.elem, sizeof(float) * 16);
		float c = cosf(fAngle);
		float s = sinf(fAngle);
		m.elem[0][0] = c;
		m.elem[2][0] = s;
		m.elem[0][2] = -s;
		m.elem[2][2] = c;
		return m;
	}

	Matrix4x4 Matrix4x4::RotationZ(float fAngle)
	{
		Matrix4x4 m;
		memcpy(m.elem, IDENTITY.elem, sizeof(float) * 16);
		float c = cosf(fAngle);
		float s = sinf(fAngle);
		m.elem[0][0] = c;
		m.elem[0][1] = s;
		m.elem[1][0] = -s;
		m.elem[1][1] = c;
		return m;
	}

	Matrix4x4 Matrix4x4::LookAt(const Vector4 & vFrom, const Vector4 & vTo, const Vector4 & vUp)
	{
		Matrix4x4 m;
		Vector4 vZ = Normalise(vFrom - vTo);
		//vZ.DumpVector4("vZ");
		Vector4 vX = Normalise(vUp.Cross(vZ));
		//vX.DumpVector4("vX");
		Vector4 vY = vZ.Cross(vX);
		//vY.DumpVector4("vY");

		m.elem[0][0] = vX.X;	m.elem[0][1] = vY.X;	m.elem[0][2] = vZ.X;	m.elem[0][3] = 0;
		m.elem[1][0] = vX.Y;	m.elem[1][1] = vY.Y;	m.elem[1][2] = vZ.Y;	m.elem[1][3] = 0;
		m.elem[2][0] = vX.Z;	m.elem[2][1] = vY.Z;	m.elem[2][2] = vZ.Z;	m.elem[2][3] = 0;

		m.elem[3][0] = -vX.Dot3(vFrom);
		m.elem[3][1] = -vY.Dot3(vFrom);
		m.elem[3][2] = -vZ.Dot3(vFrom);
		m.elem[3][3] = 1;
		return m;
	}


	void Matrix4x4::DumpMatrix4x4(char * s)
	{
		if(s != NULL)printf("\n%s\n");
		else printf("\n");
		printf("%f %f %f %f\n",   elem[0][0], elem[0][1], elem[0][2], elem[0][3]);
		printf("%f %f %f %f\n",   elem[1][0], elem[1][1], elem[1][2], elem[1][3]);
		printf("%f %f %f %f\n",   elem[2][0], elem[2][1], elem[2][2], elem[2][3]);
		printf("%f %f %f %f\n\n", elem[3][0], elem[3][1], elem[3][2], elem[3][3]);
	}


	inline Matrix4x4 operator - ( const Matrix4x4 & M )
	{
		return Matrix4x4(-M(0,0),-M(0,1),-M(0,2),-M(0,3),
						 -M(1,0),-M(1,1),-M(1,2),-M(1,3),
						 -M(2,0),-M(2,1),-M(2,2),-M(2,3),
						 -M(3,0),-M(3,1),-M(3,2),-M(3,3));
	}

	inline Matrix4x4 operator - ( const Matrix4x4 & M1,
								  const Matrix4x4 & M2 )
	{
		return Matrix4x4(M1(0,0)-M2(0,0),M1(0,1)-M2(0,1),M1(0,2)-M2(0,2),M1(0,3)-M2(0,3),
						 M1(1,0)-M2(1,0),M1(1,1)-M2(1,1),M1(1,2)-M2(1,2),M1(1,3)-M2(1,3),
						 M1(2,0)-M2(2,0),M1(2,1)-M2(2,1),M1(2,2)-M2(2,2),M1(2,3)-M2(2,3),
						 M1(3,0)-M2(3,0),M1(3,1)-M2(3,1),M1(3,2)-M2(3,2),M1(3,3)-M2(3,3));
	}

	inline Matrix4x4 operator + ( const Matrix4x4 & M1,
								  const Matrix4x4 & M2 )
	{
		return Matrix4x4(M1(0,0)+M2(0,0),M1(0,1)+M2(0,1),M1(0,2)+M2(0,2),M1(0,3)+M2(0,3),
						 M1(1,0)+M2(1,0),M1(1,1)+M2(1,1),M1(1,2)+M2(1,2),M1(1,3)+M2(1,3),
						 M1(2,0)+M2(2,0),M1(2,1)+M2(2,1),M1(2,2)+M2(2,2),M1(2,3)+M2(2,3),
						 M1(3,0)+M2(3,0),M1(3,1)+M2(3,1),M1(3,2)+M2(3,2),M1(3,3)+M2(3,3));
	}

	inline Matrix4x4 operator * ( const Matrix4x4 & M,
								  const float & s )
	{
		return Matrix4x4(M(0,0) * s,M(0,1) * s,M(0,2) * s,M(0,3) * s,
						 M(1,0) * s,M(1,1) * s,M(1,2) * s,M(1,3) * s,
						 M(2,0) * s,M(2,1) * s,M(2,2) * s,M(2,3) * s,
						 M(3,0) * s,M(3,1) * s,M(3,2) * s,M(3,3) * s);
	}

	inline Matrix4x4 operator * ( const float & s,
								  const Matrix4x4 & M )
	{
		return Matrix4x4(M(0,0) * s,M(0,1) * s,M(0,2) * s,M(0,3) * s,
						 M(1,0) * s,M(1,1) * s,M(1,2) * s,M(1,3) * s,
						 M(2,0) * s,M(2,1) * s,M(2,2) * s,M(2,3) * s,
						 M(3,0) * s,M(3,1) * s,M(3,2) * s,M(3,3) * s);
	}

	inline Matrix4x4 Transpose( Matrix4x4 const & M )
	{
		Matrix4x4 ret;

		for(int j = 0; j < 4; j++)
		{
			for(int i = 0; i < 4; i++)
			{
				ret(i,j) = M(j,i);
			}
		}

		return ret;
	}
}