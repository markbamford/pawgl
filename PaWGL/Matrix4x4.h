#ifndef __MATRIX4x4_H__
#define __MATRIX4x4_H__

#include "Vector4.h"
#include "Vector3.h"
#include "Vector2.h"
#include "MathEx.h"
#include <string>

namespace Math
{
	class Matrix4x4
	{
	public:
		Matrix4x4(void);
		Matrix4x4(const Matrix4x4 & rhs);
		Matrix4x4(float _elem[][4]);
		Matrix4x4(float _11, float _12, float _13, float _14,
					float _21, float _22, float _23, float _24,
					float _31, float _32, float _33, float _34,
					float _41, float _42, float _43, float _44);

		~Matrix4x4(void);

		static Matrix4x4 Translation(float X, float Y, float Z);
		static Matrix4x4 Translation(Vector3 v);
		static Matrix4x4 Scale(float S);
		static Matrix4x4 Scale(float X, float Y, float Z);
		static Matrix4x4 RotationX(float fAngle);
		static Matrix4x4 RotationY(float fAngle);
		static Matrix4x4 RotationZ(float fAngle);
		
		void DumpMatrix4x4(char * s = NULL);

		Matrix4x4 LookAt(const Vector4 & vFrom,
					const Vector4 & vTo,
					const Vector4 & vUp);

	 //   Matrix4x4 & operator = (Matrix4x4 rhs)
		//{ 
		//	memcpy(elem, rhs.elem, sizeof(float) * 16);
		//	return *this;
		//}

		inline float &operator()( unsigned int Row, unsigned int Column )
		{ return elem[Row][Column]; }
		inline float const &operator()( unsigned int Row, unsigned int Column ) const
		{ return elem[Row][Column]; }

		float elem[4][4];

		static const Matrix4x4 IDENTITY;
		static const Matrix4x4 NULLMATRIX;
	};

	Matrix4x4 operator*( const Matrix4x4 & M1,
						   const Matrix4x4 & M2 );

	Vector4 operator*( const Matrix4x4 & M,
						 const Vector4 & V );

	Vector3 operator*( const Matrix4x4 & M,
						 const Vector3 & V );

	Vector2 operator*( const Matrix4x4 & M,
						 const Vector2 & V );
}
#endif
