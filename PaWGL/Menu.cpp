#include "Menu.h"

Menu::Menu(GraphicsManager* graphics, GameState::GameStateEnum* gameState)
	: _background(800, 600),
	_menu(Quad(200, 200, true), Vector3(0.0f, 25.0f, 0.0f)),
	_title(Quad(100, 50, true), Vector3(0.0f, -100.0f, 0.0f)),
	_controls(Quad(200, 200, true), Vector3(0.0f, 25.0f, 0.0f)),
	_credits(Quad(200, 200, true), Vector3(0.0f, 25.0f, 0.0f)),
	_win(Quad(200, 200, true), Vector3(0.0f, 25.0f, 0.0f)),
	_fail(Quad(200, 200, true), Vector3(0.0f, 25.0f, 0.0f)),
	_gameState(gameState)
{
	_background.SetTexture(graphics->TextureFromBMP("SkyStars.bmp"));
	_background.SetUpperUV(Vector2(4.0f, 3.0f));

	_title.GetQuad().SetTexture(graphics->TextureFromBMP("Title.bmp"));
	_win.GetQuad().SetTexture(graphics->TextureFromBMP("Win.bmp"));
	_fail.GetQuad().SetTexture(graphics->TextureFromBMP("Fail.bmp"));
	_credits.GetQuad().SetTexture(graphics->TextureFromBMP("Credits.bmp"));
#ifdef PS2
	_menu.GetQuad().SetTexture(graphics->TextureFromBMP("PS2Menu.bmp"));
	_controls.GetQuad().SetTexture(graphics->TextureFromBMP("PS2Controls.bmp"));
#else
	_menu.GetQuad().SetTexture(graphics->TextureFromBMP("PCMenu.bmp"));
	_controls.GetQuad().SetTexture(graphics->TextureFromBMP("PCControls.bmp"));
#endif
}

Menu::~Menu(void)
{
}

float Menu::Light_Ambient[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float Menu::Light_Diffuse[4] = {1.0f, 1.0f, 1.0f, 1.0f};
float Menu::Light_Position[4]= {0.0f, 0.0f, 6.0f, 1.0f};

void Menu::Update(GameTime gameTime)
{
#ifdef OPENGL
	KeyboardState k = Input::States::GetKeyboardState();
	switch(*_gameState)
	{
	case GameState::Menu:
		if(k.IsKeyDown(Input::Keys::One) || k.IsKeyDown(Input::Keys::Numpad1)) (*_gameState) = GameState::Game;
		else if((k.IsKeyDown(Input::Keys::Two) && _prevKeys.IsKeyUp(Input::Keys::Two))
			 ||(k.IsKeyDown(Input::Keys::Numpad2) && _prevKeys.IsKeyUp(Input::Keys::Numpad2)))
				(*_gameState) = GameState::Controls;

		else if((k.IsKeyDown(Input::Keys::Three) && _prevKeys.IsKeyUp(Input::Keys::Three))
			 ||(k.IsKeyDown(Input::Keys::Numpad3) && _prevKeys.IsKeyUp(Input::Keys::Numpad3)))
				(*_gameState) = GameState::Credits;
		break;
	case GameState::Controls:
		if(k.IsKeyDown(Input::Keys::One) || k.IsKeyDown(Input::Keys::Numpad1)) (*_gameState) = GameState::Game;
		else if(((k.IsKeyDown(Input::Keys::Two) && _prevKeys.IsKeyUp(Input::Keys::Two))
			 ||(k.IsKeyDown(Input::Keys::Numpad2) && _prevKeys.IsKeyUp(Input::Keys::Numpad2)))
			 || k.IsKeyDown(Input::Keys::Escape))
				(*_gameState) = GameState::Menu;

		else if(k.IsKeyDown(Input::Keys::Three) || k.IsKeyDown(Input::Keys::Numpad3)) (*_gameState) = GameState::Credits;
		break;
	case GameState::Credits:
		if(k.IsKeyDown(Input::Keys::One) || k.IsKeyDown(Input::Keys::Numpad1)) (*_gameState) = GameState::Game;
		else if(k.IsKeyDown(Input::Keys::Two) || k.IsKeyDown(Input::Keys::Numpad2)) (*_gameState) = GameState::Controls;
		else if(((k.IsKeyDown(Input::Keys::Three) && _prevKeys.IsKeyUp(Input::Keys::Three))
			 ||(k.IsKeyDown(Input::Keys::Numpad3) && _prevKeys.IsKeyUp(Input::Keys::Numpad3)))
			 || k.IsKeyDown(Input::Keys::Escape))
				(*_gameState) = GameState::Menu;
		break;
	case GameState::Win:
	case GameState::Fail:
		if(k.IsKeyDown(Input::Keys::Escape)) (*_gameState) = GameState::NewGame;
		break;
	}
	_prevKeys = k;
#endif

#ifdef PS2
	switch(*_gameState)
	{
	case GameState::Menu:
		if(PS2Game::Player(0).ButtonsCur().Triangle) (*_gameState) = GameState::Game;
		else if(PS2Game::Player(0).ButtonsCur().Circle && !PS2Game::Player(0).ButtonsPrev().Circle) (*_gameState) = GameState::Controls;
		else if(PS2Game::Player(0).ButtonsCur().Square && !PS2Game::Player(0).ButtonsPrev().Square) (*_gameState) = GameState::Credits;
		break;
	case GameState::Controls:
		if(PS2Game::Player(0).ButtonsCur().Triangle) (*_gameState) = GameState::Game;
		else if(PS2Game::Player(0).ButtonsCur().Circle && !PS2Game::Player(0).ButtonsPrev().Circle) (*_gameState) = GameState::Menu;
		else if(PS2Game::Player(0).ButtonsCur().Square && !PS2Game::Player(0).ButtonsPrev().Square) (*_gameState) = GameState::Credits;
		else if(PS2Game::Player(0).ButtonsCur().Cross) (*_gameState) = GameState::Menu;
		break;
	case GameState::Credits:
		if(PS2Game::Player(0).ButtonsCur().Triangle) (*_gameState) = GameState::Game;
		else if(PS2Game::Player(0).ButtonsCur().Circle && !PS2Game::Player(0).ButtonsPrev().Circle) (*_gameState) = GameState::Controls;
		else if(PS2Game::Player(0).ButtonsCur().Square && !PS2Game::Player(0).ButtonsPrev().Square) (*_gameState) = GameState::Menu;
		else if(PS2Game::Player(0).ButtonsCur().Cross) (*_gameState) = GameState::Menu;
		break;
	case GameState::Win:
	case GameState::Fail:
		if(PS2Game::Player(0).ButtonsCur().Cross || PS2Game::Player(0).ButtonsCur().Start || PS2Game::Player(0).ButtonsCur().Circle) (*_gameState) = GameState::NewGame;
		break;
	}
#endif
}

void Menu::Draw(GraphicsManager* graphics)
{
#ifdef OPENGL
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear The Screen And The Depth Buffer
	glLoadIdentity();// load Identity Matrix

	//gluPerspective(45.0f, 4.0f / 3.0f, 1, 500.0f);

	//set camera looking down the -z axis,  6 units away from the center
	gluLookAt(0, 0, 89,     0, 0, -100,     0, -1, 0); //Where we are, What we look at, and which way is up
	//gluLookAt(0, 0, 0,     0, 0, -1,     0, -1, 0); //Where we are, What we look at, and which way is up
	
	glLightfv(GL_LIGHT1, GL_AMBIENT,  Light_Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE,  Light_Diffuse);
	//glLightfv(GL_LIGHT1, GL_POSITION, Light_Position);
	glEnable(GL_LIGHT1);
#endif

	Matrix4x4 Camera = Matrix4x4::Translation(0.0f, 0.0f, 1.0f) * Matrix4x4::Scale(1.75f, 1.75f, 1.0f);//-50.0f);
	_background.Draw(graphics, Matrix4x4::Translation(0,0, 0.9f));

	switch(*_gameState)
	{
	case GameState::Menu:
		_title.Draw(graphics, Camera);
		_menu.Draw(graphics, Camera);
		break;
	case GameState::Controls:
		_controls.Draw(graphics, Camera);
			break;
	case GameState::Credits:
		_credits.Draw(graphics, Camera);
			break;
	case GameState::Win:
		_win.Draw(graphics, Camera);
			break;
	case GameState::Fail:
		_fail.Draw(graphics, Camera);
			break;
	}
}