#ifndef __Menu_H__
#define __Menu_H__

#include "DevPlatform.h"
#include "MultiplatformGame.h"
#include "GameTime.h"
#include "Scenery.h"
#include "GameState.h"

class Menu
{
private:
	Quad _background;
	Scenery _controls;
	Scenery _credits;
	Scenery _win;
	Scenery _fail;
	Scenery _menu;
	Scenery _title;
	GameState::GameStateEnum* _gameState;
#ifdef OPENGL
	KeyboardState _prevKeys;
#endif

	static float Light_Ambient[4];
	static float Light_Diffuse[4];
	static float Light_Position[4];

public:
	Menu(GraphicsManager* graphics, GameState::GameStateEnum* gameState);
	~Menu(void);

	void Update(GameTime gameTime);
	void Draw(GraphicsManager* graphics);
};

#endif