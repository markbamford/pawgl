#include "MouseState.h"

namespace Input
{
	MouseState::MouseState()
	{ }

	MouseState::MouseState(Point position, bool left, bool right)
	{
		_position = position; 
		_leftButton = left;
		_rightButton = right;
	}	

	Point MouseState::GetPosition()
	{ return _position; }

	bool MouseState::IsLeftButtonDown()
	{ return _leftButton; }
	bool MouseState::IsRightButtonDown()
	{ return _rightButton; }
}