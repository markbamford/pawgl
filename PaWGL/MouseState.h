#pragma once
#include "Point.h"
using namespace Graphics;

namespace Input
{
	struct MouseState
	{
	private:
		Point _position;
		bool _leftButton;
		bool _rightButton;

	public:
		Point GetPosition();

		MouseState();
		MouseState(Point position, bool left, bool right);

		bool IsLeftButtonDown();
		bool IsRightButtonDown();
	};
}