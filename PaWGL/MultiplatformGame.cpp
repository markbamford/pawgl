#include "MultiplatformGame.h"

MultiplatformGame::MultiplatformGame(
#ifdef WINDOWS
HINSTANCE hInstance, LPCSTR title)
	: WindowsGame(hInstance, title)
#endif

#ifdef OPENGL 
HINSTANCE hInstance, LPCSTR title)
	: OpenGLGame(hInstance, title)
#endif

#ifdef PS2 
)
	: PS2Game()
#endif
{ }

MultiplatformGame::~MultiplatformGame(void)
{
}
