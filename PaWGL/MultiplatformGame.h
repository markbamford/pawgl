#ifndef __MultiplatformGame_H__
#define __MultiplatformGame_H__
#include "DevPlatform.h"
#ifdef WINDOWS 
#include "WindowsGame.h"
#endif
#ifdef OPENGL
#include "OpenGLGame.h"
#endif
#ifdef PS2
#include "PS2Game.h"
#endif

class MultiplatformGame : public
#ifdef WINDOWS 
	WindowsGame
#endif
#ifdef OPENGL
	OpenGLGame
#endif
#ifdef PS2
	PS2Game
#endif
{
public:
#ifdef WINDOWS 
	MultiplatformGame(HINSTANCE hInstance, LPCSTR title);
#endif
#ifdef OPENGL
	MultiplatformGame(HINSTANCE hInstance, LPCSTR title);
#endif
#ifdef PS2
	MultiplatformGame(void);
#endif
	virtual ~MultiplatformGame(void);
};

#endif