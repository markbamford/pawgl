#include "OpenGLGame.h"

OpenGLGame::OpenGLGame(HINSTANCE hInstance, LPCSTR title)
	: _window(new Windows::GameWindow(this, title, hInstance, true))
{ Initialise(new OpenGLGraphicsManager(_window->GetHandle())); }

OpenGLGame::~OpenGLGame(void)
{ }

int OpenGLGame::Run()
{ return Windows::Window::MessageLoop(); }

void OpenGLGame::Exit()
{ _window->Close(); }

void OpenGLGame::Update(GameTime gameTime)
{ }

void OpenGLGame::Draw()
{ }