#pragma once
#include "GameWindow.h"
#include "GameTime.h"
#include "OpenGLGraphicsManager.h"
#include <windows.h>

namespace Windows { class GameWindow; };

class OpenGLGame : public Game
{
private:
	Windows::GameWindow* _window;

public:
	OpenGLGame(HINSTANCE hInstance, LPCSTR title);
	virtual ~OpenGLGame(void);
	
	int Run();
	void Exit();
	virtual void Update(GameTime gameTime);
	virtual void Draw();
};
