#include "OpenGLGraphicsManager.h"

namespace Graphics
{
	OpenGLGraphicsManager::OpenGLGraphicsManager(HWND hwnd)
	{
		_hwnd = hwnd;
		_front = GetDC(_hwnd); // Initialises front buffer device context (window)
		RECT rect;
		GetClientRect(_hwnd, &rect);
		_bounds = Bounds(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);

		if (!SetPixelFormat(_front))//  sets  pixel format
		PostQuitMessage (0);

		_renderContext = wglCreateContext(_front);	//  creates  rendering context from  hdc
		wglMakeCurrent(_front, _renderContext);		//	Use this HRC.

		FixGLViewport();	// Setup the Screen

		
		//OpenGL settings
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		glClearColor(0.0f, 0.0f, 0.0f, 0.5f);				// Background Color
		glClearDepth(1.0f);									// Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
		glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
		glEnable(GL_LIGHTING);
		glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		//Also, do any other setting of variables here for your app if you wish. 
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);// Linear Filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);// Linear Filtering
	}

	OpenGLGraphicsManager::~OpenGLGraphicsManager(void)
	{
		if (_renderContext)
		{
			wglMakeCurrent(0, 0); // free rendering memory
			wglDeleteContext(_renderContext); // Delete our OpenGL Rendering Context
		}
	}

	Texture* OpenGLGraphicsManager::CreateBMPTexture(FILE* file, BMPHeader &header, unsigned int swizzledPalette[256])
	{ 
		unsigned int TexID;
		unsigned char* imageData;

		// Get the total size of all of the pixel data
		int DataSize = header.Width * header.Height * 32;

		if((DataSize & 0xF) != 0)	// Make sure the texture data size is a quadword multiple.
		{
			printf("Error: Bad Data Size");
			return 0;
		}

		imageData = new unsigned char[DataSize];

		// Start on the last line and work our way up the image as
		// bitmaps are stored upside down
		int pixelCount = (header.Width * header.Height);
		for(int i = 0; i < pixelCount; i++)
		{
			// We have finished a whole row move up one line
			// (we know we have finished a row if i is a multiple
			// of the image width
			if(i % header.Width == 0)
				fseek(file,(int)(header.DataOffset) + (((header.Height * header.Width) - (i + header.Width)) * (header.BitsPerPixel >> 3)), SEEK_SET);

			switch(header.BitsPerPixel)
			{
				case 8:
				{
					// Read the pixel in
					char index = 0;
					fread(&index, 1, 1, file);
					imageData[i << 2] = swizzledPalette[index] | (swizzledPalette[index + 1] << 8) | (swizzledPalette[index + 2] << 16) | (swizzledPalette[index + 3] << 24);
					break;
				}
				case 24:
				{
					// Read the pixel in
					unsigned int pixel = 0;
					fread(&pixel, 1, 3, file);

					// Swap the red and blue channels
					unsigned char* pPtr = (unsigned char*)&pixel;
					imageData[(i << 2) + 0] = pPtr[2];
					imageData[(i << 2) + 1] = pPtr[1];
					imageData[(i << 2) + 2] = pPtr[0];
					imageData[(i << 2) + 3] = 0x7F;
					break;
				}
				case 32:
				{
					// Read the pixel in
					fread(&imageData[i << 2], 1, 4, file);

					// Swap the red and blue channels
					unsigned char* pPtr = (unsigned char*)&imageData[i << 2];
					unsigned char red = pPtr[2];
					pPtr[2] = pPtr[0];
					pPtr[0] = red;
					break;
				}
			}
		}

		unsigned int textureType = GL_RGBA;

		glGenTextures(1, &TexID);
		glBindTexture(GL_TEXTURE_2D, TexID);
		glTexImage2D(GL_TEXTURE_2D, 0, header.BitsPerPixel >> 3, header.Width, header.Height, 0, textureType, GL_UNSIGNED_BYTE, imageData);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);// Linear Filtering
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);// Linear Filtering
		delete imageData;
		return new OpenGLTexture(this, _textures.Count(), header.Width, header.Height, header.BitsPerPixel, TexID);
	}

	Texture* OpenGLGraphicsManager::CreateTGATexture(FILE* file, TGAHeader &header)
	{ 		
		unsigned int TexID;
		unsigned char* imageData;
		if(header.ImageType == 2 && !LoadUncompressedTGA(file, header, imageData)) return 0;
		else if(header.ImageType == 10 && !LoadCompressedTGA(file, header, imageData)) return 0;
		else
		{
			MessageBox(0, "TGA file be type 2 or type 10 ", "Invalid Image", MB_OK);
			if(file != 0) fclose(file);
		}

		unsigned int textureType;
		if(header.BitsPerPixel == 24) textureType = GL_RGB;
		else textureType = GL_RGBA;

		glGenTextures(1, &TexID);
		glBindTexture(GL_TEXTURE_2D, TexID);
		glTexImage2D(GL_TEXTURE_2D, 0, header.BitsPerPixel >> 3, header.Width, header.Height, 0, textureType, GL_UNSIGNED_BYTE, imageData);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,  GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);

		if (imageData) free(imageData);
		return new OpenGLTexture(this, _textures.Count(), header.Width, header.Height, header.BitsPerPixel, TexID);
	}

	bool OpenGLGraphicsManager::SelectTexture(int id)
	{ 
		if(!GraphicsManager::SelectTexture(id)) return false;
		glBindTexture(GL_TEXTURE_2D, ((OpenGLTexture*)_textures[id])->GetGLTextureID());
		return true;
	}

	void OpenGLGraphicsManager::SetBounds(Bounds bounds)
	{
		_bounds.Right = bounds.Right - bounds.Left; 
		_bounds.Bottom = bounds.Bottom - bounds.Top;

		FixGLViewport();
	}

	void OpenGLGraphicsManager::Present()
	{ SwapBuffers(_front); }

	void OpenGLGraphicsManager::FixGLViewport()
	{
		_vpWidth = _bounds.Right - _bounds.Left;
		_vpHeight = _bounds.Top - _bounds.Bottom;

		if (_vpHeight == 0)// Prevent A Divide By Zero error
			_vpHeight = 1;// Make the Height Equal One

		glViewport(0, 0, _vpWidth, _vpHeight);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		//calculate aspect ratio
		gluPerspective(140.0f, (float)_vpWidth / (float)_vpHeight, 1 , 150.0f);

		glMatrixMode(GL_MODELVIEW);// Select The Modelview Matrix
		glLoadIdentity();// Reset The Modelview Matrix
	}

	bool OpenGLGraphicsManager::SetPixelFormat(HDC hdc) 
	{ 
		PIXELFORMATDESCRIPTOR pfd = {0}; 
		int pixelformat; 
	 
		pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);	// Set the size of the structure
		pfd.nVersion = 1;							// Always set this to 1
		// Pass in the appropriate OpenGL flags
		pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER; 
		pfd.dwLayerMask = PFD_MAIN_PLANE;			// standard mask (this is ignored anyway)
		pfd.iPixelType = PFD_TYPE_RGBA;				// RGB and Alpha pixel type
		pfd.cColorBits = COLOUR_DEPTH;				// Here we use our #define for the color bits
		pfd.cDepthBits = COLOUR_DEPTH;				// Ignored for RBA
		pfd.cAccumBits = 0;							// nothing for accumulation
		pfd.cStencilBits = 0;						// nothing for stencil
	 
		//Gets a best match on the pixel format as passed in from device
		if ( (pixelformat = ChoosePixelFormat(hdc, &pfd)) == false ) 
		{ 
			MessageBox(0, "ChoosePixelFormat failed", "Error", MB_OK); 
			return false; 
		} 
	 
		//sets the pixel format if its ok. 
		if (::SetPixelFormat(hdc, pixelformat, &pfd) == false) 
		{ 
			MessageBox(0, "SetPixelFormat failed", "Error", MB_OK); 
			return false; 
		} 
	 
		return true;
	}

	bool OpenGLGraphicsManager::LoadUncompressedTGA(FILE* tgaFile, TGAHeader tgaHeader, unsigned char* &imageData)
	{
		if((tgaHeader.Width <= 0) || (tgaHeader.Height <= 0) || ((tgaHeader.BitsPerPixel != 24) && (tgaHeader.BitsPerPixel !=32)))
		{
			MessageBox(0, "Invalid texture information", "Error", MB_OK);
			if(tgaFile != 0) fclose(tgaFile);
			return false;
		}

		unsigned int bytesPerPixel = tgaHeader.BitsPerPixel >> 3;
		unsigned int imageSize = (bytesPerPixel * tgaHeader.Width * tgaHeader.Height);
		imageData = (unsigned char*)malloc(imageSize);

		if(imageData == 0)
		{
			MessageBox(0, "Could not allocate memory for image", "ERROR", MB_OK);
			fclose(tgaFile);
			return false;
		}

		//Attempt To Read Image Data
		if(fread(imageData, 1, imageSize, tgaFile) != imageSize)
		{
			MessageBox(0, "Could not read image data", "Error", MB_OK);
			if(imageData != 0) free(imageData);
			fclose(tgaFile);
			return false;
		}

		// Byte Swapping Optimized By Steve Thomas
		for(unsigned int i = 0; i < imageSize; i += bytesPerPixel)
			imageData[i] ^= imageData[i + 2] ^=	imageData[i] ^= imageData[i + 2];

		fclose(tgaFile);
		return true;			
	}

	bool OpenGLGraphicsManager::LoadCompressedTGA(FILE* tgaFile, TGAHeader tgaHeader, unsigned char* &imageData)
	{
		if((tgaHeader.Width <= 0) || (tgaHeader.Height <= 0) || ((tgaHeader.BitsPerPixel != 24) && (tgaHeader.BitsPerPixel !=32)))
		{
			MessageBox(0, "Invalid texture information", "Error", MB_OK);
			if(tgaFile != 0) fclose(tgaFile);
			return false;
		}

		unsigned int bytesPerPixel = tgaHeader.BitsPerPixel >> 3;
		unsigned int imageSize = (bytesPerPixel * tgaHeader.Width * tgaHeader.Height);
		imageData = (unsigned char*)malloc(imageSize);

		if(imageData == 0)
		{
			MessageBox(0, "Could not allocate memory for image", "Error", MB_OK);
			fclose(tgaFile);
			return false;
		}

		unsigned int pixelCount	= tgaHeader.Height * tgaHeader.Width;
		unsigned int currentPixel = 0;
		unsigned int currentByte = 0;
		unsigned char* colorBuffer = (unsigned char*)malloc(bytesPerPixel);

		do
		{
			unsigned char chunkHeader = 0;

			// Attempt To Read The 1 byte header
			if(fread(&chunkHeader, sizeof(unsigned char), 1, tgaFile) == 0)
			{
				MessageBox(0, "Could not read RLE header", "Error", MB_OK);
				if(tgaFile != 0) fclose(tgaFile);
				if(imageData != 0) free(imageData);
				return false;
			}

			if(chunkHeader < 128) // If the header is < 128, it means the that is the number of RAW color packets minus 1
			{
				chunkHeader++;	  // add 1 to get number of color values

				// Read RAW color values
				for(int i = 0; i < chunkHeader; i++)
				{
					if(fread(colorBuffer, 1, bytesPerPixel, tgaFile) != bytesPerPixel) // Try to read 1 pixel
					{
						MessageBox(0, "Could not read image data", "Error", MB_OK);
						if(tgaFile != 0) fclose(tgaFile);
						if(colorBuffer != 0) free(colorBuffer);
						if(imageData!= 0) free(imageData);
						return false;
					}

					//Copy and Flip R and B
					imageData[currentByte] = colorBuffer[2];
					imageData[currentByte + 1] = colorBuffer[1];
					imageData[currentByte + 2] = colorBuffer[0];
					if(bytesPerPixel == 4) imageData[currentByte + 3] = colorBuffer[3];

					currentByte += bytesPerPixel;
					currentPixel++;

					if(currentPixel > pixelCount)
					{
						MessageBox(0, "Too many pixels read", "Error", 0);
						if(tgaFile != 0) fclose(tgaFile);
						if(colorBuffer != 0) free(colorBuffer);
						if(imageData!= 0) free(imageData);
						return false;
					}
				}
			}
			else //chunkHeader > 128 RLE data, next color reapeated chunkHeader - 127 times
			{
				chunkHeader -= 127; // Subtract 127 to get rid of the ID bit
				if(fread(colorBuffer, 1, bytesPerPixel, tgaFile) != bytesPerPixel) // Attempt to read color values
				{	
					MessageBox(0, "Could not read from file", "Error", MB_OK);
					if(tgaFile != 0) fclose(tgaFile);
					if(colorBuffer != 0) free(colorBuffer);
					if(imageData!= 0) free(imageData);
					return false;
				}

				//Copy the color into the image data as many times as dictated by the header
				for(int i = 0; i < chunkHeader; i++)
				{
					//Copy and Flip R and B
					imageData[currentByte] = colorBuffer[2];
					imageData[currentByte + 1] = colorBuffer[1];
					imageData[currentByte + 2] = colorBuffer[0];
					if(bytesPerPixel == 4) imageData[currentByte + 3] = colorBuffer[3];

					currentByte += bytesPerPixel;
					currentPixel++;

					if(currentPixel > pixelCount)
					{
						MessageBox(0, "Too many pixels read", "Error", 0);
						if(tgaFile != 0) fclose(tgaFile);
						if(colorBuffer != 0) free(colorBuffer);
						if(imageData!= 0) free(imageData);
						return false;
					}
				}
			}
		}while(currentPixel < pixelCount);
		fclose(tgaFile);
		return true;	
	}

	void OpenGLGraphicsManager::EnableAlphaBlend() { glEnable(GL_BLEND); }
	void OpenGLGraphicsManager::DisableAlphaBlend() { glDisable(GL_BLEND); }

	void OpenGLGraphicsManager::EnableTexturing() { glEnable(GL_TEXTURE_2D); }
	void OpenGLGraphicsManager::DisableTexturing() { glDisable(GL_TEXTURE_2D); }

	void OpenGLGraphicsManager::BeginPoints(int verticeCount) { glBegin(GL_POINTS); }
	void OpenGLGraphicsManager::BeginLines(int verticeCount) { glBegin(GL_LINES); }
	void OpenGLGraphicsManager::BeginLineStrip(int verticeCount) { glBegin(GL_LINE_STRIP); }
	void OpenGLGraphicsManager::BeginTriangles(int verticeCount) { glBegin(GL_TRIANGLES); }
	void OpenGLGraphicsManager::BeginTriangleStrip(int verticeCount) { glBegin(GL_TRIANGLE_STRIP); }
	void OpenGLGraphicsManager::BeginTriangleFan(int verticeCount) { glBegin(GL_TRIANGLE_FAN); }

	void OpenGLGraphicsManager::AddNormal(Vector3 normal) { glNormal3f(normal.X, normal.Y, normal.Z); }
	void OpenGLGraphicsManager::AddColor4(Color4 color) 
	{ 
		float light[4] ={color.R, color.G, color.B, color.A};
		glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, light);
	}
	void OpenGLGraphicsManager::AddTextureCoord(Vector2 uv) { glTexCoord2f(uv.X, uv.Y); }
	void OpenGLGraphicsManager::AddVertex(Vector3 vertex) { glVertex3f(vertex.X, vertex.Y, vertex.Z); }

	void OpenGLGraphicsManager::End() { glEnd(); }
}