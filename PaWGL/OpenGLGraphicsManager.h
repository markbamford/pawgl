#ifndef __OpenGLGraphicsManager_H__
#define __OpenGLGraphicsManager_H__
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "OpenGLTexture.h"
#include "TGA.h"
#include "GraphicsManager.h"

using namespace FileTypes;

namespace Graphics
{
	class OpenGLGraphicsManager : public GraphicsManager
	{
	private:
		#define COLOUR_DEPTH 16	//Colour depth
		HGLRC _renderContext;
		int _vpWidth;
		int _vpHeight;
		HWND _hwnd;
		HDC _front;

	public:
		OpenGLGraphicsManager(HWND hwnd);
		~OpenGLGraphicsManager(void);

		bool SelectTexture(int id);
		void SetBounds(Bounds bounds);
		void Present();

		void EnableAlphaBlend();
		void DisableAlphaBlend();

		void EnableTexturing();
		void DisableTexturing();

		void BeginPoints(int verticeCount);
		void BeginLines(int verticeCount);
		void BeginLineStrip(int verticeCount);
		void BeginTriangles(int verticeCount);
		void BeginTriangleStrip(int verticeCount);
		void BeginTriangleFan(int verticeCount);

		void AddNormal(Vector3 normal);
		void AddColor4(Color4 color);
		void AddTextureCoord(Vector2 uv);
		void AddVertex(Vector3 vertex);

		void End();

	private:
		Texture* CreateBMPTexture(FILE* file, BMPHeader &header, unsigned int swizzledPalette[256]);
		Texture* CreateTGATexture(FILE* file, TGAHeader &header);

		bool LoadUncompressedTGA(FILE* tgaFile, TGAHeader tgaHeader, unsigned char* &imageData);
		bool LoadCompressedTGA(FILE* tgaFile, TGAHeader tgaHeader, unsigned char* &imageData);
		bool SetPixelFormat(HDC hdc);
		void FixGLViewport();
	};
}

#endif