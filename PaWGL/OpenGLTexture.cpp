#include "OpenGLTexture.h"

namespace Graphics
{
	OpenGLTexture::OpenGLTexture(GraphicsManager* graphics, int id, int width, int height, short bitsPerPixel, unsigned int glTextureID)
		: Texture(graphics, id, width, height, bitsPerPixel), _glTextureID(glTextureID)
	{
	}

	OpenGLTexture::~OpenGLTexture(void)
	{
	}

	unsigned int OpenGLTexture::GetGLTextureID() { return _glTextureID; }
}