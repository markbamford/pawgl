#pragma once
#include "Texture.h"

namespace Graphics
{
	class OpenGLTexture : public Texture
	{
	private:
		unsigned int _glTextureID;

	public:
		OpenGLTexture(GraphicsManager* graphics, int id, int width, int height, short bitsPerPixel, unsigned int glTextureID);
		~OpenGLTexture(void);

		unsigned int GetGLTextureID();
	};
}