#include "DevPlatform.h"
#ifdef PS2
#include "SPS2.h"
#include "DMA.h"
#include <signal.h>
#include "PS2Game.h"

#include <iostream>
using namespace std;

PS2Game::PS2Game(void) : _stepCount(0)
{
	Initialise(new PS2GraphicsManager());

	SPS2Manager.Initialise(4096);	// 4096 * 4K Pages = 4MB Total
	VIFStaticDMA.Initialise(1536);	// 1536 * 4K Pages = 2MB Static DMA
	VIFDynamicDMA.Initialise(256);	// 256 * 4K Pages * 2 Buffers =
								// 2MB Dynamic DMA

	// Set up the DMA packet to clear the screen. We want to clear to blue.
	SPS2Manager.InitScreenClear(0x0, 0x0, 0x0);

	Playing = false;

	// Load the font bitmap and data
	if(!_console.Load("font.dat", true))
	{
		Exit();
		printf("Can't load font.dat\n");
	}

	_console.SetTexture(GetGraphicsManager()->TextureFromBMP("font2.bmp"));
	if(_console.GetTexture() == 0)
	{
		Exit();
		printf("Can't load font bitmap\n");
	}
}

PS2Game::~PS2Game(void)
{
}

const GameTime PS2Game::InitGameTime(((float)TimeStep) * 0.001f, ((float)TimeStep) * 0.001f);

Font PS2Game::_console;
DualShock PS2Game::_players[2] =
{
	DualShock(0, INIT_LOCK | INIT_ANALOGUE | INIT_PRESSURE),
	DualShock(1, INIT_LOCK | INIT_ANALOGUE | INIT_PRESSURE)
};

AudioDevice PS2Game::DSP0(0);
AudioDevice PS2Game::DSP1(1);

Font& PS2Game::Console() { return _console; }
DualShock& PS2Game::Player(int index) { return PS2Game::_players[index]; }
AudioDevice& PS2Game::AudioDevice0() { return DSP0; }
AudioDevice& PS2Game::AudioDevice1() { return DSP1; }

int PS2Game::Run()
{
	Playing = true;
	while(Playing)
	{
		VIFDynamicDMA.Fire();

		for(int i = 0; i < 5; i++)
		{
			_stepCount++;
			PS2Game::DSP0.HandleAudio();
			PS2Game::DSP1.HandleAudio();
			PS2Game::_players[0].Update();
			PS2Game::_players[1].Update();
			Update(GameTime(((float)_stepCount) * InitGameTime.TotalElaspedTime, InitGameTime.ElaspedTime));
		}

		SPS2Manager.BeginScene();
		Draw();
		SPS2Manager.EndScene();	
	}
}

void PS2Game::Exit()
{ Playing = false; }

void PS2Game::Update(GameTime gameTime)
{ }

void PS2Game::Draw()
{ }

#endif