#ifndef __PS2Game_H__
#define __PS2Game_H__

#include "Texture.h"
#include "Font.h"
#include "DualShock.h"
#include "GameTime.h"
#include "Game.h"
#include "PS2GraphicsManager.h"
#include "PS2Audio.h"

using namespace Input;
using namespace Graphics;

class PS2Game : public Game
{
private:
	bool Playing;
	static Font _console;
	static DualShock _players[2];
	static AudioDevice DSP0;
	static AudioDevice DSP1;

	unsigned long _stepCount;
	static const unsigned long TimeStep = 5ul;

public:
	PS2Game(void);
	~PS2Game(void);

	static const GameTime InitGameTime;

	static Font& Console();
	static DualShock& Player(int index);
	static AudioDevice& AudioDevice0();
	static AudioDevice& AudioDevice1();

	int Run();
	void Exit();
	virtual void Update(GameTime gameTime);
	virtual void Draw();
};

#endif