#include "DevPlatform.h"
#ifdef PS2
#include <sps2lib.h>
#include <sps2tags.h>
#include <sps2tags.h>
#include <sps2regstructs.h>
#include "SPS2.h"
#include "DMA.h"
#include "PS2Defines.h"
#include "PS2GraphicsManager.h"

#include <iostream>
using namespace std;

namespace Graphics
{
	PS2GraphicsManager::PS2GraphicsManager(void)
	{ }

	PS2GraphicsManager::~PS2GraphicsManager(void)
	{
	}

	void PS2GraphicsManager::ScreenShot()
	{ SPS2Manager.ScreenShot(); }

	bool PS2GraphicsManager::SelectTexture(int id)
	{ 
		if(!GraphicsManager::SelectTexture(id)) return false;

		PS2Texture* texture = (PS2Texture*)_textures[id];

		VIFDynamicDMA.Add32(FLUSH);

		unsigned int iDstPtr = (TEXBUF480 << 5);
		unsigned int iClutPtr = ((TEXBUF480 + 15) << 5);
		unsigned int uQSizeClut = (256 >> 2);

		if( texture->GetBitsPerPixel() == 8 )
		{
			// Add the clut
			VIFDynamicDMA.StartDirect();

			VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(4, 0, 0, 0, 
								 GIF_FLG_PACKED, GS_BATCH_1(GIF_REG_A_D)));

			VIFDynamicDMA.Add64(BITBLTBUF_SET(0, 0, 0, iClutPtr, 1, 0));
			VIFDynamicDMA.Add64(BITBLTBUF);

			VIFDynamicDMA.Add64(TRXPOS_SET(0, 0, 0, 0, 0));
			VIFDynamicDMA.Add64(TRXPOS);

			VIFDynamicDMA.Add64(TRXREG_SET(16, 16));
			VIFDynamicDMA.Add64(TRXREG);

			VIFDynamicDMA.Add64(TRXDIR_SET(0));
			VIFDynamicDMA.Add64(TRXDIR);

			VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(uQSizeClut, 0, 0, 0, 
								 GIF_FLG_IMAGE, 0));

			VIFDynamicDMA.EndDirect();

			VIFDynamicDMA.DMACall(texture->GetPaletteAddress());
		}

		// Add the texture data itself.
		VIFDynamicDMA.StartDirect();
		VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(4, 0, 0, 0,
							 GIF_FLG_PACKED, GS_BATCH_1(GIF_REG_A_D)));

		// TODO - This bit doesn't seem to work so just set TBW to 4 (256 wide) for now.
		// Problems arise with the texture displayed when in 8 bit mode with a texture width less than 256?
		// Can anybody tell me why? (i'd love to know!)
		//int TBW = 4;
		int TBW = texture->GetWidth() / 64;

		VIFDynamicDMA.Add64(BITBLTBUF_SET(0, 0, 0, iDstPtr, (TBW == 0) ? 1 : TBW, texture->GetBitsPerPixel() == 8 ? PSMT8 : PSMCT32));
		VIFDynamicDMA.Add64(BITBLTBUF);

		VIFDynamicDMA.Add64(TRXPOS_SET(0, 0, 0, 0, 0));
		VIFDynamicDMA.Add64(TRXPOS);

		VIFDynamicDMA.Add64(TRXREG_SET(texture->GetWidth(), texture->GetHeight()));
		VIFDynamicDMA.Add64(TRXREG);

		VIFDynamicDMA.Add64(TRXDIR_SET(0));
		VIFDynamicDMA.Add64(TRXDIR);

		unsigned int iQWC = (texture->GetDataSize()) >> 4;

		VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(iQWC, 1, 0, 0, 
							 GIF_FLG_IMAGE, 0));

		VIFDynamicDMA.EndDirect();

		VIFDynamicDMA.DMACall(texture->GetTextureAddress());

		// Generate a default TEX0.
		unsigned long long m_iTex0 = TEX0_SET(iDstPtr, (TBW == 0) ? 1 : TBW, texture->GetBitsPerPixel() == 8 ? PSMT8 : PSMCT32, texture->GetLog2Width(), texture->GetLog2Height(), 1, 0, iClutPtr, 0, 0, 0, 1);

		// Wait for idle state
		VIFDynamicDMA.Add32(FLUSH);
			
		VIFDynamicDMA.StartDirect();
		VIFDynamicDMA.StartAD();
		VIFDynamicDMA.AddAD(0, TEXFLUSH);
		VIFDynamicDMA.AddAD(ALPHA_SET(0, 1, 0, 1, 128), ALPHA_1);
		VIFDynamicDMA.AddAD(TEX1_SET(1, 0, 1, 1, 0, 0, 0), TEX1_1);

		VIFDynamicDMA.AddAD(m_iTex0, TEX0_1);

		VIFDynamicDMA.EndAD();
		VIFDynamicDMA.EndDirect();

		return true;
	}

	void PS2GraphicsManager::SetBounds(Bounds bounds)
	{
	}

	void PS2GraphicsManager::Present()
	{
	}
	
	Texture* PS2GraphicsManager::CreateBMPTexture(FILE* file, BMPHeader &header, unsigned int swizzledPalette[256])
	{
		// The texture data buffer
		int TextureAddress = VIFStaticDMA.GetPointer();
		VIFStaticDMA.StartDirect();

		// Get the total size of all of the pixel data
		int DataSize = header.Width * header.Height * (((header.BitsPerPixel == 24) ? 32 : header.BitsPerPixel) >> 3);

		if((DataSize & 0xF) != 0)	// Make sure the texture data size is a quadword multiple.
		{
			printf("Error: Bad Data Size");
			return 0;
		}

		// Start on the last line and work our way up the image as
		// bitmaps are stored upside down
		for(int i = 0; i < (header.Width * header.Height); i++)
		{
			// We have finished a whole row move up one line
			// (we know we have finished a row if i is a multiple
			// of the image width
			if(i % header.Width == 0)
				fseek(file,(int)(header.DataOffset) + (((header.Height * header.Width) - (i + header.Width)) * (header.BitsPerPixel >> 3)), SEEK_SET);

			switch(header.BitsPerPixel)
			{
				case 8:
				{
					// We have to wait until we have 4 pixels to add them to the DMA chain, because 32 bits are the smallest size
					// the DMA class accepts. (we only accept images that have widths that are a multiple of 4 so we don't need to
					// check that)
					if(i % 4 == 0)
					{
						// Read the pixel in
						int pixel = 0;
						fread(&pixel, 4, 1, file);

						VIFStaticDMA.Add32(pixel);
					}
					break;
				}
				case 24:
				{
					// Read the pixel in
					unsigned int pixel = 0;
					fread(&pixel,1,3,file);

					// Swap the red and blue channels
					unsigned char* pPtr = (unsigned char*)&pixel;
					unsigned char red = pPtr[2];
					pPtr[2] = pPtr[0];
					pPtr[0] = red;
					pPtr[3] = 0x80;

					VIFStaticDMA.Add32(pixel);
					break;
				}
				case 32:
				{
					// Read the pixel in
					unsigned int pixel = 0;
					fread(&pixel, 1, 4, file);

					// Swap the red and blue channels
					unsigned char* pPtr = (unsigned char*)&pixel;
					unsigned char red = pPtr[2];
					pPtr[2] = pPtr[0];
					pPtr[0] = red;

					VIFStaticDMA.Add32(pixel);
					break;
				}
			}
		}

		// End the texture data buffer
		VIFStaticDMA.EndDirect();
		VIFStaticDMA.DMARet();

		// And if we have a clut...
		int PaletteAddress = 0;
		if(header.BitsPerPixel == 8)
		{
			// Start a clut buffer
			PaletteAddress = VIFStaticDMA.GetPointer();

			VIFStaticDMA.StartDirect();

			for(int i = 0; i < 256; i++)
				VIFStaticDMA.Add32(swizzledPalette[i]);

			VIFStaticDMA.EndDirect();

			VIFStaticDMA.DMARet();
		}
		return new PS2Texture(this, _textures.Count(), header.Width, header.Height, header.BitsPerPixel, TextureAddress, PaletteAddress, DataSize, TextureLog2(header.Width), TextureLog2(header.Height));
	}

	Texture* PS2GraphicsManager::CreateTGATexture(FILE* file, TGAHeader &header)
	{ throw; }

	void PS2GraphicsManager::EnableAlphaBlend() { _alphaBlend = true; }
	void PS2GraphicsManager::DisableAlphaBlend() { _alphaBlend = false; }

	void PS2GraphicsManager::EnableTexturing() { _texturing = true; }
	void PS2GraphicsManager::DisableTexturing() { _texturing = false; }

	//void PS2GraphicsManager::BeginPoints(int verticeCount) { glBegin(GL_POINTS); }
	//void PS2GraphicsManager::BeginLines(int verticeCount) { glBegin(GL_LINES); }
	//void PS2GraphicsManager::BeginLineStrip(int verticeCount) { glBegin(GL_LINE_STRIP); }
	//void PS2GraphicsManager::BeginTriangles(int verticeCount) { glBegin(GL_TRIANGLES); }
	void PS2GraphicsManager::BeginTriangleStrip(int verticeCount) 
	{ 
		VIFDynamicDMA.StartDirect();
		
		int useAlpha = (_alphaBlend) ? PRIM_ABE_ON : PRIM_ABE_OFF;

		if(_texturing)
		{
			// Add the GIFTag
			VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(	verticeCount,							// NLOOP
													1,							// EOP
													1,							// PRE
													GS_PRIM(PRIM_TRI_STRIP, 	// PRIM
														PRIM_IIP_FLAT, 
														PRIM_TME_ON,
														PRIM_FGE_OFF, 
														useAlpha, 
														PRIM_AA1_OFF, 
														PRIM_FST_UV, 
														PRIM_CTXT_CONTEXT1, 
														PRIM_FIX_NOFIXDDA),
													GIF_FLG_PACKED,					//FLG
													GS_BATCH_3(	GIF_REG_RGBAQ,
																GIF_REG_UV, 
																GIF_REG_XYZ2)));
		}
		else
		{
			VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(	verticeCount,							// NLOOP
											1,							// EOP
											1,							// PRE
											GS_PRIM(PRIM_TRI_STRIP, 	// PRIM
												PRIM_IIP_FLAT, 
												PRIM_TME_OFF,
												PRIM_FGE_OFF, 
												useAlpha, 
												PRIM_AA1_OFF, 
												PRIM_FST_UV, 
												PRIM_CTXT_CONTEXT1, 
												PRIM_FIX_NOFIXDDA),
											GIF_FLG_PACKED,					//FLG
											GS_BATCH_2(	GIF_REG_RGBAQ, 
														GIF_REG_XYZ2)));	//BATCH	
		}
	}
	//void PS2GraphicsManager::BeginTriangleFan(int verticeCount) { glBegin(GL_TRIANGLE_FAN); }

	void PS2GraphicsManager::AddNormal(Vector3 normal) {  }
	void PS2GraphicsManager::AddColor4(Color4 color) { VIFDynamicDMA.Add128(PACKED_RGBA((unsigned char)(color.R * 255.0f), (unsigned char)(color.G * 255.0f), (unsigned char)(color.B * 255.0f), (unsigned char)(color.A * 255.0f))); }
	void PS2GraphicsManager::AddTextureCoord(Vector2 uv) { VIFDynamicDMA.Add128(PACKED_UV((((int)uv.X) << 4) + 8, (((int)uv.Y) << 4) + 8)); }
	void PS2GraphicsManager::AddVertex(Vector3 vertex) { VIFDynamicDMA.Add128(PACKED_XYZ2((int)((2048.0f + vertex.X/* / vertex.Z*/) * 16.0f), (int)((2048.0f + vertex.Y/* / vertex.Z*/) * 16.0f), (int)(vertex.Z * 16.0f), 0)); }

	void PS2GraphicsManager::End() { VIFDynamicDMA.EndDirect(); }
}

#endif