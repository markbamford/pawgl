#ifndef __PS2GraphicsManager_H__
#define __PS2GraphicsManager_H__

#include "GraphicsManager.h"
#include "PS2Texture.h"
#include "BMP.h"

namespace Graphics
{
	class PS2GraphicsManager : public GraphicsManager
	{
	private:
		bool _alphaBlend;
		bool _texturing;

	public:
		PS2GraphicsManager(void);
		~PS2GraphicsManager(void);

		void ScreenShot();

		bool SelectTexture(int id);
		void SetBounds(Bounds bounds);
		void Present();

		void EnableAlphaBlend();
		void DisableAlphaBlend();

		void EnableTexturing();
		void DisableTexturing();

		//void BeginPoints(int verticeCount);
		//void BeginLines(int verticeCount);
		//void BeginLineStrip(int verticeCount);
		//void BeginTriangles(int verticeCount);
		void BeginTriangleStrip(int verticeCount);
		//void BeginTriangleFan(int verticeCount);
				
		void AddNormal(Vector3 normal);
		void AddColor4(Color4 color);
		void AddTextureCoord(Vector2 uv);
		void AddVertex(Vector3 vertex);

		void End();

	private:
		Texture* CreateBMPTexture(FILE* file, BMPHeader &header, unsigned int swizzledPalette[256]);
		Texture* CreateTGATexture(FILE* file, TGAHeader &header);
	};
}

#endif