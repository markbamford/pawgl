//#ifndef __PS2MATHS_H__
//#define __PS2MATHS_H__
//#include "DevPlatform.h"
//#ifdef PS2
//// Morten "Sparky" Mikkelsen's fast maths routines (From a post on the forums
//// at www.playstation2-linux.com)
//
//namespace Math
//{
//
//	// Morten "Sparky" Mikkelsen's fast maths routines (From a post on the forums
//	// at www.playstation2-linux.com)
//
//	float asinf(float x);
//	float cosf(float v);
//	float abs(const float x);
//	float sqrt(const float x);
//	float max(const float a, const float b);
//	float min(const float a, const float b);
//	float acosf(float x);
//	float sinf(float v);
//
//	float DegToRad(float Deg);
//	float mod(const float a, const float b);
//}
//#endif
//#endif