#include "DevPlatform.h"
#ifdef PS2
#include <sps2tags.h>
#include <sps2regstructs.h>
#include "SPS2.h"
#include "DMA.h"
#include "PS2Sprite.h"
#include <iostream>
using namespace std;
namespace Graphics
{
	PS2Sprite::PS2Sprite() : Sprite()
	{ }

	PS2Sprite::PS2Sprite(float x, float y) : Sprite(x, y)
	{ }

	PS2Sprite::~PS2Sprite()
	{ }

	void PS2Sprite::Render(void)
	{
		static const float Scale = 14.0f;
		Rectangle result(0, 0, _width * Scale, _height * Scale);//((_position.X * Scale) + (2048.0f * 16.0f), (_position.Y * Scale) + (2048.0f * 16.0f), _width * Scale, _height * Scale);
		_matrix = Matrix4x4::RotationZ(_rotation);
			//Matrix4x4::Translation((_position.X * Scale) + (2048.0f * 16.0f), (_position.Y * Scale) + (2048.0f * 16.0f), 0);// * Matrix4x4::Translation(2048.0f * 16.0f, 2048.0f * 16.0f, 0);
		result = Rectangle::Transform(result, _matrix);
		Vector2 fixedPosition((_position.X * Scale) + (2048.0f * 16.0f), (_position.Y * Scale) + (2048.0f * 16.0f));
		result.Center += fixedPosition;
		result.BottomLeft += fixedPosition;
		result.BottomRight += fixedPosition;
		result.TopLeft += fixedPosition;
		result.TopRight += fixedPosition;
		VIFDynamicDMA.StartDirect();
		
		// Add the GIFTag
		VIFDynamicDMA.Add128(GS_GIFTAG_BATCH(4,							// NLOOP
											1,							// EOP
											1,							// PRE
											GS_PRIM(PRIM_TRI_STRIP, 	// PRIM
												PRIM_IIP_FLAT, 
												PRIM_TME_OFF,
												PRIM_FGE_OFF, 
												PRIM_ABE_OFF, 
												PRIM_AA1_OFF, 
												PRIM_FST_UV, 
												PRIM_CTXT_CONTEXT1, 
												PRIM_FIX_NOFIXDDA
												),
											GIF_FLG_PACKED,					//FLG
											GS_BATCH_2(GIF_REG_RGBAQ, GIF_REG_XYZ2))
											);	//BATCH	
			
		// Add the colour (Top Left)
		VIFDynamicDMA.Add128(PACKED_RGBA(_color.R ,_color.G, _color.B, _color.A));
			    	
		// Add the position (Top Left)  	   	
		VIFDynamicDMA.Add128(PACKED_XYZ2((int)(result.TopLeft.X), 
										 (int)(result.TopLeft.Y),  
										 (int)_depth, 
										  0));
		
		// Add the colour (Bottom Left)
		VIFDynamicDMA.Add128(PACKED_RGBA(_color.R ,_color.G, _color.B, _color.A));
			    	
		// Add the position (Bottom Left)  	
		VIFDynamicDMA.Add128(PACKED_XYZ2((int)(result.BottomLeft.X), 
										 (int)(result.BottomLeft.Y),  
										 (int)_depth, 
										  0));
										  
		
		// Add the colour (Top Right)
		VIFDynamicDMA.Add128(PACKED_RGBA(_color.R ,_color.G, _color.B, _color.A));
			    	
		// Add the position (Top Right)  	
		VIFDynamicDMA.Add128(PACKED_XYZ2((int)(result.TopRight.X), 
										 (int)(result.TopRight.Y),  
										 (int)_depth, 
										  0));

		// Add the colour (Bottom Right)
		VIFDynamicDMA.Add128(PACKED_RGBA(_color.R ,_color.G, _color.B, _color.A));
			    	
		// Add the position  (Bottom Right)  	
		VIFDynamicDMA.Add128(PACKED_XYZ2((int)(result.BottomRight.X), 
										 (int)(result.BottomRight.Y),  
										 (int)_depth, 
										  0));
		
		
		VIFDynamicDMA.EndDirect();
	}
}
#endif