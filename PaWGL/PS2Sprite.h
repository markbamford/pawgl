#include "DevPlatform.h"
#ifdef PS2
#ifndef _PS2Sprite_H_
#define _PS2Sprite_H_

#include <sps2lib.h>
#include <sps2tags.h>
#include <sps2util.h>
#include "PS2Defines.h"
#include "Sprite.h"
#include "Matrix4x4.h"
#include "SPS2.h"

using namespace Math;

namespace Graphics
{
	class PS2Sprite : public Sprite
	{
	public:
		PS2Sprite();
		PS2Sprite(const float x, const float y);
		~PS2Sprite();
		void Render(void);
	};	
}

#endif
#endif