#include "PS2Texture.h"

namespace Graphics
{
	PS2Texture::PS2Texture(GraphicsManager* graphics, int id, int width, int height, short bitsPerPixel, int textureAddress, int paletteAddress, int dataSize, int log2width, int log2height)
		: Texture(graphics, id, width, height, bitsPerPixel), 
		_textureAddress(textureAddress), _paletteAddress(paletteAddress), 
		_dataSize(dataSize), 
		_log2width(log2width), _log2height(log2height)
	{ }

	PS2Texture::~PS2Texture(void)
	{ }

	int PS2Texture::GetPaletteAddress() { return _paletteAddress; }
	int PS2Texture::GetTextureAddress() { return _textureAddress; }
	int PS2Texture::GetDataSize() { return _dataSize; }
	int PS2Texture::GetLog2Width() { return _log2width; }
	int PS2Texture::GetLog2Height() { return _log2height; }
}