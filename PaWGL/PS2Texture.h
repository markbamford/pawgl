#ifndef __PS2Texture_H__
#define __PS2Texture_H__

#include "Texture.h"

namespace Graphics
{
	class PS2Texture : public Texture
	{
	private:
		int _textureAddress;
		int _paletteAddress;
		int _dataSize;
		int _log2width;
		int _log2height;

	public:
		PS2Texture(GraphicsManager* graphics, int id, int width, int height, short bitsPerPixel, int textureAddress, int paletteAddress, int dataSize, int log2width, int log2height);
		~PS2Texture(void);

		int GetPaletteAddress();
		int GetTextureAddress();
		int GetDataSize();
		int GetLog2Width();
		int GetLog2Height();
	};
}

#endif