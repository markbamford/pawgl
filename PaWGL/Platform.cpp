#include "Platform.h"

Platform::Platform(Simulator2D* simulator, const Vector3 &position, const Vector2 &size)
	: _simulator(simulator), 
	_quad(size.X, size.Y), 
	_body(Vector2(position.X, position.Y), size, 1.0f, 0.7f, 0.7f, true),
	_transform(Matrix4x4::Translation(position))
{ _simulator->AddBody(&_body); }

Platform::~Platform(void)
{ }

Quad* Platform::GetQuad() { return &_quad; }

void Platform::Update(GameTime gameTime)
{
}

void Platform::Draw(GraphicsManager* graphicsManager, Matrix4x4 transform)
{ 
	AABB bounds;
	bounds.Upper = transform * _body.GetBounds().Upper;
	bounds.Lower = transform * _body.GetBounds().Lower;
	AABB screen;
	screen.Upper = Vector2(400.0f, 300.0f);
	screen.Lower = Vector2(-400.0f, -300.0f);

	if(bounds.Intersects(screen)) _quad.Draw(graphicsManager, _transform * transform); 
}