#ifndef __Platform_H__
#define __Platform_H__

#include "QuadBody.h"
#include "Quad.h"
#include "List.h"
#include "Simulator2D.h"
#include "AABB.h"

using namespace Physics;
using namespace Graphics;

class Platform
{
private:
	Simulator2D* _simulator;
	QuadBody _body;
	Quad _quad;
	Matrix4x4 _transform;

public:
	Platform(Simulator2D* simulator, const Vector3 &position, const Vector2 &size);
	~Platform(void);

	Quad* GetQuad();

	void Update(GameTime gameTime);
	void Draw(GraphicsManager* graphicsManager, Matrix4x4 transform = Matrix4x4::IDENTITY);
};

#endif