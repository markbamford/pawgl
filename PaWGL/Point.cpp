#include "Point.h"

namespace Graphics
{
	Point::Point()
	{
		X = 0;
		Y = 0;
	}

	Point::Point(int x, int y)
	{
		X = x;
		Y = y;
	}

	Point& Point::operator+=(const Point& b)
	{ 
		X += b.X;
		Y += b.Y;
		return (*this);
	}

	Point& Point::operator-=(const Point& b)
	{ 
		X -= b.X;
		Y -= b.Y;
		return (*this);
	}

	Point& Point::operator*=(const Point& b)
	{ 
		X -= b.X;
		Y -= b.Y;
		return (*this);
	}

	Point& Point::operator/=(const Point& b)
	{ 
		X -= b.X;
		Y -= b.Y;
		return (*this);
	}

	Point& Point::operator*=(const float& b)
	{ 
		X *= b;
		Y *= b;
		return (*this);
	}

	Point& Point::operator/=(const float& b)
	{ 
		X /= b;
		Y /= b;
		return (*this);
	}
}