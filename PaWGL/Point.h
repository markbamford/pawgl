#ifndef __Point_H__
#define __Point_H__

namespace Graphics
{
	struct Point
	{
		int X;
		int Y;

		Point();
		Point(int x, int y);

		Point& Point::operator+=(const Point& b);
		Point& Point::operator-=(const Point& b);
		Point& Point::operator*=(const Point& b);
		Point& Point::operator/=(const Point& b);
		Point& Point::operator*=(const float& b);
		Point& Point::operator/=(const float& b);
	};

	inline Point operator+(const Point& a, const Point& b)
	{ return Point(a.X + b.X, a.Y + b.Y); }

	inline Point operator-(const Point& a, const Point& b)
	{ return Point(a.X - b.X, a.Y - b.Y); }

	inline Point operator*(const Point& a, const Point& b)
	{ return Point(a.X * b.X, a.Y * b.Y); }

	inline Point operator/(const Point& a, const Point& b)
	{ return Point(a.X / b.X, a.Y / b.Y); }

	inline Point operator*(const Point& a, const float& b)
	{ return Point((int)((float)a.X * b), (int)((float)a.Y * b)); }

	inline Point operator/(const Point& a, const float& b)
	{ return Point((int)((float)a.X / b), (int)((float)a.Y / b)); }
}

#endif