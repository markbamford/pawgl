#include "PointMass.h"

namespace Physics
{
	PointMass::PointMass(const Vector2 position, const bool isStatic, const float mass)
		: _motionSystem(position), _force(0.0f, 0.0f), _mass(mass), _inverseMass(1 / mass), _isStatic(isStatic)
	{ }

	PointMass::~PointMass(void)	{ }

	Vector2& PointMass::GetPosition() { return _motionSystem.Position; }
	Vector2& PointMass::GetDisplacement() { return _motionSystem.Displacement; }
	void PointMass::SetPosition(const Vector2 &position) { Translate(position - _motionSystem.Position); }
	void PointMass::SetDisplacement(const Vector2 &difference) { _motionSystem.Displacement = difference; }
	void PointMass::Translate(const Vector2 &displacement) { _motionSystem.Position += displacement; }

	Vector2& PointMass::GetAcceleration() { return _motionSystem.Acceleration; }
	void PointMass::SetAcceleration(const Vector2 &acceleration) { _motionSystem.Acceleration = acceleration; }
	void PointMass::IncreaseAcceleration(const Vector2 &acceleration) { _motionSystem.Acceleration += acceleration; }

	Vector2& PointMass::GetForce() { return _force; }
	void PointMass::SetForce(const Vector2 &force) { _force = force; }
	void PointMass::IncreaseForce(const Vector2 &force) { _force += force; }

	Vector2 PointMass::CalculateVelocity(GameTime gameTime) { return _motionSystem.CalculateVelocity(gameTime); }
	void PointMass::ApplyVelocity(Vector2 velocity, GameTime gameTime) { _motionSystem.ApplyVelocity(velocity, gameTime); }

	bool PointMass::GetIsStatic() { return _isStatic; }
	void PointMass::SetIsStatic(bool isStatic) { _isStatic = isStatic; }

	float PointMass::GetMass() { return _mass; }
	float PointMass::GetInverseMass() { return _inverseMass; }
	void PointMass::SetMass(const float mass) { _mass = mass; _inverseMass = 1 / mass; }

	void PointMass::TimeStep(GameTime gameTime, Vector2 gravity)
	{
		_motionSystem.TimeStep(gameTime, 1.0f, ((_isStatic) ? Vector2(0,0) : (gravity + (_force * _inverseMass))));
		_force = Vector2(0.0f, 0.0f);
	}

	void PointMass::Collide(CollisionInfo &info)
	{ 
		Vector2 reverse(info.Incidence * -info.IncidencePenetration);

		float velocityX = info.Velocity.DotProduct(info.Normal);
		float velocityY = info.Velocity.DotProduct(info.Normal.Perpendicular());
		velocityX *= -info.Restitution;
		velocityY *= (1 - info.Friction);
		Vector2 expulsion(info.Normal * velocityX + info.Normal.Perpendicular() * velocityY);

		if(info.B->GetIsStatic() && !_isStatic)
		{
			_motionSystem.Position += reverse;
			_motionSystem.Displacement = expulsion;
		}
		else if(!_isStatic)
		{
			float momemtumConservation = (info.B->GetMass() / (_mass + info.B->GetMass()));
			_motionSystem.Position += reverse * momemtumConservation;
			_motionSystem.Displacement = expulsion * momemtumConservation;
		}
	}
}