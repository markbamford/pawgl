#ifndef _PointMass_H_
#define _PointMass_H_

#include "Vector2.h"
#include "GameTime.h"
#include "CollisionInfo.h"

using namespace Math;

namespace Physics
{
	class PointMass
	{
	private:
		VerletVector2MotionSystem _motionSystem;
		Vector2 _force;

		float _mass;
		float _inverseMass;

		bool _isStatic;

	public:
		PointMass(const Vector2 position, const bool isStatic = false, const float mass = 1.0f);
		~PointMass(void);

		Vector2& GetPosition();
		Vector2& GetDisplacement();
		void SetPosition(const Vector2 &position);
		void SetDisplacement(const Vector2 &difference);
		void Translate(const Vector2 &displacement);

		Vector2& GetAcceleration();
		void SetAcceleration(const Vector2 &acceleration);
		void IncreaseAcceleration(const Vector2 &acceleration);

		Vector2& GetForce();
		void SetForce(const Vector2 &force);
		void IncreaseForce(const Vector2 &force);

		Vector2 CalculateVelocity(GameTime gameTime);
		void ApplyVelocity(Vector2 velocity, GameTime gameTime);

		bool GetIsStatic();
		void SetIsStatic(bool isStatic);

		float GetMass();
		float GetInverseMass();
		void SetMass(const float mass);

		void TimeStep(GameTime gameTime, Vector2 gravity = Vector2(0.0f, 0.0f));
		void Collide(CollisionInfo &info);
	};
}
#endif