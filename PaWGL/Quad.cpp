#include "Quad.h"
#include "DevPlatform.h"
#include "PS2Texture.h"

namespace Graphics
{
	Quad::Quad(float width, float height, bool useAlpha, Vector3 normal, Vector3 up)
		: _color(1.0f, 1.0f, 1.0f, 1.0f), _texture(0), _useAlpha(useAlpha), _width(width), _height(height), _normal(normal), _up(up), _lowerUV(0.0f, 0.0f), _upperUV(1.0f, 1.0f)
	{ CreateVertices(); }

	Quad::Quad(Color4 color, float width, float height, bool useAlpha, Vector3 normal, Vector3 up)
		: _color(color), _texture(0), _useAlpha(useAlpha), _width(width), _height(height), _normal(normal), _up(up), _lowerUV(0.0f, 0.0f), _upperUV(1.0f, 1.0f)
	{ CreateVertices(); }

	Quad::Quad(Texture* texture, float width, float height, bool useAlpha, Vector3 normal, Vector3 up)
		: _color(1.0f, 1.0f, 1.0f, 1.0f), _texture(texture), _useAlpha(useAlpha), _width(width), _height(height), _normal(normal), _up(up), _lowerUV(0.0f, 0.0f), _upperUV(1.0f, 1.0f)
	{ CreateVertices(); }

	Quad::~Quad(void)
	{ }

	Vector3 Quad::GetNormal() { return _normal; }
	Vector3 Quad::GetUp() { return _up; }

	float Quad::GetWidth() { return _width; }
	float Quad::GetHeight() { return _height; }

	Vector2 Quad::GetLowerUV() { return _lowerUV; }
	void Quad::SetLowerUV(Vector2 lowerUV) { _lowerUV = lowerUV; }

	Vector2 Quad::GetUpperUV() { return _upperUV; }
	void Quad::SetUpperUV(Vector2 upperUV) { _upperUV = upperUV; }

	Texture* Quad::GetTexture() { return _texture; }
	void Quad::SetTexture(Texture* texture)	{ _texture = texture; }

	bool Quad::GetUseAlpha() { return _useAlpha; }
	void Quad::SetUseAlpha(bool useAlpha) { _useAlpha = useAlpha; }

	void Quad::CreateVertices()
	{
		Vector3 left = _up.CrossProduct(_normal);
		float halfWidth = _width * 0.5f;
		float halfHeight = _height * 0.5f;

		_vertices[0] = -(left * halfWidth) - (_up * halfHeight);
		_vertices[1] = -(left * halfWidth) + (_up * halfHeight);
		_vertices[2] = (left * halfWidth) - (_up * halfHeight);
		_vertices[3] = (left * halfWidth) + (_up * halfHeight);
	}

	void Quad::Draw(GraphicsManager* graphics, Matrix4x4 m)
	{
		if(_texture) 
		{
			_texture->Select();
			graphics->EnableTexturing();
		}
		else graphics->DisableTexturing();

		if(_useAlpha) graphics->EnableAlphaBlend();
		else graphics->DisableAlphaBlend();

		graphics->BeginTriangleStrip(4);
		
			graphics->AddColor4(_color);
#ifdef PS2
			graphics->AddTextureCoord(Vector2(_lowerUV.X * (_texture->GetWidth() - 1), _lowerUV.Y * (_texture->GetHeight() - 1)));
#else
			graphics->AddTextureCoord(_lowerUV);
#endif
			graphics->AddNormal(_normal);
			graphics->AddVertex(m * _vertices[0]);
			
			graphics->AddColor4(_color);
#ifdef PS2
			graphics->AddTextureCoord(Vector2(_lowerUV.X * (_texture->GetWidth() - 1), _upperUV.Y * (_texture->GetHeight() - 1)));
#else
			graphics->AddTextureCoord(Vector2(_lowerUV.X, _upperUV.Y));
#endif
			graphics->AddNormal(_normal);
			graphics->AddVertex(m * _vertices[1]);
			
			graphics->AddColor4(_color);
#ifdef PS2
			graphics->AddTextureCoord(Vector2(_upperUV.X * (_texture->GetWidth() - 1), _lowerUV.Y * (_texture->GetHeight() - 1)));
#else
			graphics->AddTextureCoord(Vector2(_upperUV.X, _lowerUV.Y));
#endif
			graphics->AddNormal(_normal);
			graphics->AddVertex(m * _vertices[2]);	

			graphics->AddColor4(_color);
#ifdef PS2
			graphics->AddTextureCoord(Vector2(_upperUV.X * (_texture->GetWidth() - 1), _upperUV.Y * (_texture->GetHeight() - 1)));
#else
			graphics->AddTextureCoord(_upperUV);
#endif
			graphics->AddNormal(_normal);
			graphics->AddVertex(m * _vertices[3]);

		graphics->End();
	}
}