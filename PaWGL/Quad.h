#ifndef __Quad_H__
#define __Quad_H__
#include "Matrix4x4.h"
#include "Vector3.h"
#include "GraphicsManager.h"

using namespace Math;

namespace Graphics
{
	class Quad
	{
	private:
		Texture* _texture;

		bool _useAlpha;
	
		float _width, _height;
		
		Vector3 _vertices[4];
		Vector3 _normal;
		Vector3 _up;
		Vector2 _lowerUV;
		Vector2 _upperUV;
		Color4 _color;
		
	public:
		Quad(float width, float height, bool useAlpha = false, Vector3 normal = Vector3(0.0f, 0.0f, 1.0f), Vector3 up = Vector3(0.0f, 1.0f, 0.0f));
		Quad(Color4 color, float width, float height, bool useAlpha = false, Vector3 normal = Vector3(0.0f, 0.0f, 1.0f), Vector3 up = Vector3(0.0f, 1.0f, 0.0f));
		Quad(Texture* texture, float width, float height, bool useAlpha = false, Vector3 normal = Vector3(0.0f, 0.0f, 1.0f), Vector3 up = Vector3(0.0f, 1.0f, 0.0f));
		~Quad(void);

		Vector3 GetNormal();
		Vector3 GetUp();

		Color4 GetColor();
		void SetColor(const Color4 color);

		float GetWidth();
		float GetHeight();

		Vector2 GetLowerUV();
		void SetLowerUV(Vector2 lowerUV);
		Vector2 GetUpperUV();
		void SetUpperUV(Vector2 upperUV);
		
		Texture* GetTexture();
		void SetTexture(Texture* texture);

		bool GetUseAlpha();
		void SetUseAlpha(bool useAlpha);

		void Draw(GraphicsManager* graphics, Matrix4x4 m = Matrix4x4::IDENTITY);

	private:
		void CreateVertices();
	};
}

#endif