#include "QuadBody.h"

namespace Physics
{
	QuadBody::QuadBody(const Vector2 &position, const Vector2 &size, const float mass, const float restitution, const float friction, const bool isStatic)
		: ConvexBody(position, mass, restitution, friction, isStatic)
	{ 
		float halfWidth = size.X / 2;
		float halfHeight = size.Y / 2;
		Vector2 A(-halfWidth, halfHeight);
		Vector2 B(halfWidth, halfHeight);
		Vector2 C(halfWidth, -halfHeight);
		Vector2 D(-halfWidth, -halfHeight);
		A += position;
		B += position;
		C += position;
		D += position;
		_sides.Add(Line2D(A, B));
		_sides.Add(Line2D(B, C));
		_sides.Add(Line2D(C, D));
		_sides.Add(Line2D(D, A));

		CreateAABB();
	}

	QuadBody::~QuadBody(void)
	{
	}

	void QuadBody::CreateAABB()
	{
		_bounds.Upper.X = _sides[1].Start.X;
		_bounds.Upper.Y = _sides[1].Start.Y;
		_bounds.Lower.X = _sides[3].Start.X;
		_bounds.Lower.Y = _sides[3].Start.Y;
	}

	void QuadBody::AddPossibleSeparatingAxes(ConvexBody* b, List<SeparatingAxisComparer> &Axes)
	{
		Axes.Add(SeparatingAxisComparer(_sides[0].Direction, SeparatingAxisComparer::Parallel));
		Axes[Axes.Count() - 1].AddPoint(_sides[0].Start);
		Axes[Axes.Count() - 1].AddPoint(_sides[1].Start);
		Axes.Add(SeparatingAxisComparer(_sides[1].Direction, SeparatingAxisComparer::Parallel));
		Axes[Axes.Count() - 1].AddPoint(_sides[1].Start);
		Axes[Axes.Count() - 1].AddPoint(_sides[2].Start);
	}
}