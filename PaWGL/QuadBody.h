#ifndef _QuadBody_H_
#define _QuadBody_H_
#include "ConvexBody.h"

namespace Physics
{
	class QuadBody : public ConvexBody
	{
	public:
		QuadBody(const Vector2 &position, const Vector2 &size, const float mass = 1.0f, const float restitution = 0.7f, const float friction = 0.9f, const bool isStatic = false);
		~QuadBody(void);

	protected:
		virtual void CreateAABB();
		void AddPossibleSeparatingAxes(ConvexBody* b, List<SeparatingAxisComparer> &Axes);
	};
}

#endif