#ifndef __QUEUE_H__
#define __QUEUE_H__

template <class T> class Queue
{
private:
	int _count;
	int _capacity;
	T** _list;
	int _first;
	int _next;

public:
	Queue(void);
	Queue(int capacity);
	~Queue(void);

	void Enqueue(T &item);
	T& Peek();
	T Dequeue();
	int Count();
	int GetCapacity();
	void SetCapacity(int capacity);

private:
	void FixCapacity();
	static const int DEFAULTCAPACITY = 8;
};

template <class T> Queue<T>::Queue(void)
{
	_count = 0;
	_list = (T**)calloc(DEFAULTCAPACITY, sizeof(T*));
	if (_list == 0) throw "Unable To Allocate Memory";
	_capacity = DEFAULTCAPACITY;
	_first = 0;
	_next = 0;
}

template <class T> Queue<T>::Queue(int capacity)
{
	_count = 0;
	_list = (T**)calloc(capacity, sizeof(T*));
	if (_list == 0) throw "Unable To Allocate Memory";
	_capacity = capacity;
	_first = 0;
	_next = 0;
}

template <class T> Queue<T>::~Queue(void)
{ 
	for(int i = 0; i < _capacity; i++)
		delete _list[i];
	if(!(_first == _next)) free(_list);
}

template <class T> void Queue<T>::Enqueue(T &item)
{
	_count++;
	FixCapacity();
	_list[_next] = new T(item);
	_next++;
	if(_next == _capacity) _next = 0;
}

template <class T> T& Queue<T>::Peek()
{ 
	if(_first == _next) throw;
	return *_list[_first];
}

template <class T> T Queue<T>::Dequeue()
{ 
	if(_count == 0) throw "Index Out Of Range!";
	_count--;
	T val = T(*_list[_first]);
	_list[_first] = 0;
	_first++;
	if(_first == _capacity) _first = 0;
	return val;
}

template <class T> int Queue<T>::Count()
{ return _count; }

template <class T> int Queue<T>::GetCapacity()
{ return _capacity; }

template <class T> void Queue<T>::SetCapacity(int capacity)
{
	if(capacity < 0) throw "Capacity Cannot Be Less Than Zero!";
	T** temp = (T**)realloc(_list, capacity * sizeof(T*));
	if (temp == 0) throw "Unable To Allocate Memory";
	_list = temp;
	if(_next < _first)
	{
		int newFirst = capacity - (_capacity - _first);
		memmove(&_list[newFirst], &_list[_first], (_capacity - _first) * sizeof(T*));
		memset(&_list[_next], 0, (newFirst - _next) * sizeof(T*));
		_first = newFirst;
	}
	_capacity = capacity;
}

template <class T> void Queue<T>::FixCapacity()
{ if(_count == _capacity) SetCapacity(_capacity << 1); }

#endif