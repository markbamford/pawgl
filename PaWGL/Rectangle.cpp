#include "Rectangle.h"

namespace Graphics
{
	Rectangle::Rectangle()
	{ }

	Rectangle::Rectangle(float width, float height)
	{
		TopLeft.X = (-width / 2.0f);
		TopLeft.Y = (height / 2.0f);
		BottomLeft.X = (-width / 2.0f);
		BottomLeft.Y = (-height / 2.0f);
		TopRight.X = (width / 2.0f);
		TopRight.Y = (height / 2.0f);
		BottomRight.X = (width / 2.0f);
		BottomRight.Y = (-height / 2.0f);
		Center.X = 0;
		Center.Y = 0;
	}

	Rectangle::Rectangle(float x, float y, float width, float height)
	{
		TopLeft.X = (-width / 2.0f) + x;
		TopLeft.Y = (height / 2.0f) + y;
		BottomLeft.X = (-width / 2.0f) + x;
		BottomLeft.Y = (-height / 2.0f) + y;
		TopRight.X = (width / 2.0f) + x;
		TopRight.Y = (height / 2.0f) + y;
		BottomRight.X = (width / 2.0f) + x;
		BottomRight.Y = (-height / 2.0f) + y;
		Center.X = x;
		Center.Y = y;
	}

	Rectangle Rectangle::Transform(Rectangle r, Matrix4x4 m)
	{
		Rectangle R;
		R.TopLeft = m * r.TopLeft;
		R.BottomLeft = m * r.BottomLeft;
		R.TopRight = m * r.TopRight;
		R.BottomRight = m * r.BottomRight;
		R.Center = m * r.Center;
		return R;
	}
}