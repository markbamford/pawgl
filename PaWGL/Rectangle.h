#ifndef __Rectangle_H__
#define __Rectangle_H__

#include "Vector2.h"
#include "Vector4.h"
#include "Matrix4x4.h"
using namespace Math;

namespace Graphics
{
	struct Rectangle
	{				
		Vector2 TopLeft, BottomLeft, TopRight, BottomRight, Center;

		Rectangle();
		Rectangle(float width, float height);
		Rectangle(float x, float y, float width, float height);

		static Rectangle Transform(Rectangle r, Matrix4x4 m);
	};
}
#endif