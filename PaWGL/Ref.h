#pragma once

template <class T> class Ref
{
private:
	T* _ref;

public:
	Ref();
	Ref(T* ref);
	~Ref();

	T& Get();
	void Set(T* ref);

	bool Exists();
};

template <class T> Ref<T>::Ref()
{ }

template <class T> Ref<T>::Ref(T* ref)
{ Set(ref); }

template <class T> Ref<T>::~Ref()
{ if(_ref) delete _ref; }

template <class T> T& Ref<T>::Get()
{ return *_ref; }

template <class T> void Ref<T>::Set(T* ref)
{ 
	if(_ref) delete _ref;
	_ref = ref; 
}

template <class T> bool Ref<T>::Exists()
{ return _ref; }