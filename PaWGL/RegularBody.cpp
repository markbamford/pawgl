#include "RegularBody.h"

namespace Physics
{
	RegularBody::RegularBody(int sideCount, const Vector2 &position, const Vector2 &size, const float mass, const float restitution, const float friction, const bool isStatic)
		: ConvexBody(position, mass, restitution, friction, isStatic)
	{
		Vector2 point(position + Vector2(sinf(0.0f) * size.X, cosf(0.0f) * size.Y));
		Vector2 nextPoint;
		
		for(int i = 0; i < sideCount; i++)
		{
			nextPoint = position + Vector2(
				sinf((float)(i + 1) * TWOPI / (float)sideCount) * size.X,
				cosf((float)(i + 1) * TWOPI / (float)sideCount) * size.Y
				);
			_sides.Add(Line2D(point, nextPoint));
			point = nextPoint;
		}

		CreateAABB();
	}

	RegularBody::~RegularBody(void)
	{
	}
}