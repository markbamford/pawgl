#ifndef _RegularPolygon_H_
#define _RegularPolygon_H_

#include "ConvexBody.h"

namespace Physics
{
	class RegularBody : public ConvexBody
	{
	public:
		RegularBody(int sideCount,const Vector2 &position, const Vector2 &size, const float mass = 1.0f, const float restitution = 0.7f, const float friction = 0.9f, const bool isStatic = false);
		~RegularBody(void);
	};
}

#endif