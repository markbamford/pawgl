#include "RotatableQuadBody.h"

namespace Physics
{
	RotatableQuadBody::RotatableQuadBody(const Vector2 &position, const Vector2 &size, const float mass, const float restitution, const float friction, const bool isStatic)
		: QuadBody(position, size, mass, restitution, friction, isStatic)
	{ }

	RotatableQuadBody::~RotatableQuadBody(void)
	{
	}

	void RotatableQuadBody::CreateAABB()
	{ ConvexBody::CreateAABB(); }

	//Vector2& GetOrigin() { return _origin; }
	//void SetOrigin(const Vector2 &origin) { _origin = origin; }
	//void TranslateOrigin(const Vector2 &displacement) { _origin += displacement; }

	float RotatableQuadBody::GetRotation() { return _rotation; }
	void RotatableQuadBody::SetRotation(const float radians) { Rotate(radians - _rotation); }
	void RotatableQuadBody::Rotate(const float radians)//, const Vector2 &origin)
	{
		float CosAngle = cosf(radians);
		float SinAngle = sinf(radians);
		Vector2 displacement(GetPosition());// + origin);

		Vector2 vertices[4] = { GetSides()[0].Start,
								GetSides()[1].Start, 
								GetSides()[2].Start, 
								GetSides()[3].Start 
							  };

		for(int i = 0; i < 4; i++)
		{
			vertices[i] -= displacement;
			vertices[i] = Vector2(
				CosAngle * vertices[i].X - SinAngle * vertices[i].Y,
				SinAngle * vertices[i].X + CosAngle * vertices[i].Y
				);
		}

		for(int i = 0; i < 4; i++) vertices[i] += displacement;

		for(int i = 0; i < 3; i++) GetSides()[i] = Line2D(vertices[i], vertices[i + 1]);
		GetSides()[3] = Line2D(vertices[3], vertices[0]);

		_rotation += radians;
	}
}