#ifndef _RotatableQuadBody_H_
#define _RotatableQuadBody_H_
#include "QuadBody.h"

namespace Physics
{
	class RotatableQuadBody : public QuadBody
	{
	private:
	//	Vector2 _origin;
		float _rotation;

	public:
		RotatableQuadBody(const Vector2 &position, const Vector2 &size, const float mass = 1.0f, const float restitution = 0.7f, const float friction = 0.9f, const bool isStatic = false);
		~RotatableQuadBody(void);

		//Vector2& GetOrigin();
		//void SetOrigin(const Vector2 &origin);
		//void TranslateOrigin(const Vector2 &displacement);

		float GetRotation();
		void SetRotation(const float radians);
		void Rotate(const float radians);//, const Vector2 &origin = Vector2(0, 0));

	protected:
		void CreateAABB();
	};
}

#endif