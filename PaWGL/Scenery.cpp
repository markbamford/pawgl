#include "Scenery.h"

Scenery::Scenery(Quad quad, Vector3 position, float rotation)
	: _quad(quad), _position(position), _rotation(rotation)
{ CreateMatrix(); }

Scenery::~Scenery(void)
{ }

Vector3& Scenery::GetPosition() { return _position; }
void Scenery::SetPosition(const Vector3 &position) { _position = position; CreateMatrix(); }

float& Scenery::GetRotation() { return _rotation; }
void Scenery::SetRotation(const float &rotation) { _rotation = rotation; CreateMatrix(); }

Quad& Scenery::GetQuad() { return _quad; }

void Scenery::Draw(GraphicsManager* graphicsManager, Matrix4x4 transform)
{ 
	AABB bounds;
	bounds.Upper = transform * Vector2(_position.X + _quad.GetWidth() * 0.5f, _position.Y +  _quad.GetHeight() * 0.5f);
	bounds.Lower = transform * Vector2(_position.X - _quad.GetWidth() * 0.5f, _position.Y  - _quad.GetHeight() * 0.5f);
	AABB screen;
	screen.Upper = Vector2(400.0f, 300.0f);
	screen.Lower = Vector2(-400.0f, -300.0f);

	if(bounds.Intersects(screen)) _quad.Draw(graphicsManager, _matrix * transform); 
}

void Scenery::CreateMatrix()
{ _matrix = Matrix4x4::RotationZ(_rotation) * Matrix4x4::Translation(_position.X, _position.Y, _position.Z); }