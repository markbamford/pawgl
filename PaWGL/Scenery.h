#ifndef __Scenery_H__
#define __Scenery_H__

#include "Quad.h"
#include "Matrix4x4.h"
#include "AABB.h"

using namespace Graphics;
using namespace Physics;

class Scenery
{
private:
	Matrix4x4 _matrix;
	Vector3 _position;
	float _rotation;
	Quad _quad;

public:
	Scenery(Quad quad, Vector3 position = Vector3(0.0f, 0.0f, 0.0f), float rotation = 0.0f);
	~Scenery(void);

	Vector3& GetPosition();
	void SetPosition(const Vector3 &position);

	float& GetRotation();
	void SetRotation(const float &rotation);

	Quad& GetQuad();

	void Draw(GraphicsManager* graphicsManager, Matrix4x4 transform = Matrix4x4::IDENTITY);

private:
	void CreateMatrix();
};

#endif