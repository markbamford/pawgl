#include "SeparatingAxisComparer.h"

namespace Physics
{
	SeparatingAxisComparer::SeparatingAxisComparer(Vector2 vector2, ResponceTypeEnum responceType)
		: _direction(vector2.Normalize()), _initCheckA(false), _initCheckB(false)
	{
		ResponceType = responceType;
	}

	Vector2 SeparatingAxisComparer::GetDirection() { return _direction; }

	void SeparatingAxisComparer::AddPoint(Vector2 vertice)
	{
		float point = Project(vertice);
		if(point > _maxA) _maxA = point;
		else if(point < _minA) _minA = point;
		if(!_initCheckA)
		{
			_minA = _maxA = point;
			_initCheckA = true;
		}
	}

	void SeparatingAxisComparer::AddComparePoint(Vector2 vertice)
	{
		float point = Project(vertice);
		if(point > _maxB) _maxB = point;
		else if(point < _minB) _minB = point;
		if(!_initCheckB)
		{
			_minB = _maxB = point;
			_initCheckB = true;
		}
	}

	float SeparatingAxisComparer::Project(Vector2 vertice)
	{ return vertice.DotProduct(_direction); }

	bool SeparatingAxisComparer::Overlaps()
	{ return _initCheckA && _initCheckB && _minA < _maxB && _minB < _maxA; }

	float SeparatingAxisComparer::GetPenetration()
	{ 
		float p1 = _minA - _maxB;
		if(p1 < 0) p1 = -p1;

		float p2 = _minB - _maxA;
		if(p2 < 0) p2 = -p2;

		if(p1 < p2) return p1;
		return p2;
	}

	bool SeparatingAxisComparer::SetCollisionInfo(CollisionInfo &info)
	{
		float penetration = GetPenetration();
		if(info.NormalPenetration > penetration || info.NormalPenetration < 0.0f)
		{
			info.NormalPenetration = penetration;

			switch(ResponceType)
			{
			case SeparatingAxisComparer::Parallel:
				info.Normal = -_direction;
				break;
			case SeparatingAxisComparer::Perpendicular:
				info.Normal = -_direction.Perpendicular();
				break;
			}
			if(_minA > _minB) info.Normal = -info.Normal;
			return true;
		}
		return false;
	}
}