#ifndef _SeparatingAxisComparer_H_
#define _SeparatingAxisComparer_H_
#include "Vector2.h"
#include "CollisionInfo.h"

using namespace Math;

namespace Physics
{
	struct CollisionInfo;
	class SeparatingAxisComparer
	{
	private:
		bool _initCheckA;
		bool _initCheckB;
		float _minA;
		float _maxA;
		float _minB;
		float _maxB;
		Vector2 _direction;

	public:
		enum ResponceTypeEnum { Parallel, Perpendicular };
		ResponceTypeEnum ResponceType;

		Vector2 GetDirection();

		SeparatingAxisComparer(Vector2 vector2, ResponceTypeEnum responceType);
		
		void AddPoint(Vector2 vertice);
		void AddComparePoint(Vector2 vertice);
		bool Overlaps();
		float GetPenetration();
		bool SetCollisionInfo(CollisionInfo &info);
	private:
		float Project(Vector2 vector2);
	};
}

#endif