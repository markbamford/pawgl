#include "Simulator2D.h"

namespace Physics
{
	Simulator2D::Simulator2D(Vector2 gravity) : _gravity(gravity) { }

	Simulator2D::~Simulator2D(void)	{ }

	void Simulator2D::AddBody(Body* body)
	{ _shapes.Add(body); }

	void Simulator2D::RemoveBody(Body* body)
	{ 
		int i = _shapes.IndexOf(body);
		_shapesRemoveList.Add(i, i); 
	}

	void Simulator2D::Update(GameTime gameTime)
	{
		UpdateShapeList();	
		int count = _shapes.Count();
		BeginTimeStep(gameTime, count);
		FindCollisions(gameTime, count);
		EndTimeStep(gameTime, count);
	}

	void Simulator2D::UpdateShapeList()
	{
		for(int i = 0; i < _shapesRemoveList.Count(); i++) 
			_shapes.RemoveAt(_shapesRemoveList[i] - i);
		_shapesRemoveList.Clear();
	}

	void Simulator2D::BeginTimeStep(GameTime gameTime, int count)
	{ for(int i = 0; i < count; i++) _shapes[i]->BeginTimeStep(gameTime, _gravity); }

	void Simulator2D::FindCollisions(GameTime gameTime, int count)
	{
		static List<CollisionInfo> infos;
		infos.Clear();
		for(int a = 0; a < count; a++)
			for(int b = a + 1; b < count; b++)
					_shapes[a]->FindCollisions(_shapes[b], infos);

		for(int i = 0; i < infos.Count(); i++)
		{
			//Body* A(infos[i].A);
			//Body* B(infos[i].B);
			infos[i].A->Collide(infos[i]);

			//infos[i].Normal = -infos[i].Normal;
			//infos[i].Incidence = -infos[i].Incidence;
			//infos[i].Velocity = -infos[i].Velocity;
			//infos[i].A = B;
			//infos[i].B = A;
			//B->Collide(infos[i]);
		}

				//A and B Have Collided, And We Have Collision Normal and Penetration.
				//Component of velocity in the direction of (distance.X, distance.Y)
				//Vector2 velocity1 = info.A->GetAcceleration() * gameTime.ElaspedTime;
				//Vector2 velocity2 = info.B->GetAcceleration() * gameTime.ElaspedTime;

				//float component1 = velocity1.DotProduct(info.Normal);
				//float component2 = velocity2.DotProduct(info.Normal);

				////Collision Should Have Occurred Now Minus dt
				//float dt = info.Penetration / (component1 - component2);
				//if(dt < 0) dt = -dt;

				////Time Travel Backwards By dt
				////info.A->Translate(-velocity1 * dt);
				////info.B->Translate(-velocity2 * dt);

				////Projection of the velocities in these axes
				//Vector2 projectedVelocity1(component1, velocity1.DotProduct(info.Normal.Perpendicular())); 
				//Vector2 projectedVelocity2(component2, velocity2.DotProduct(info.Normal.Perpendicular()));

				////New velocities in these axes (after collision): e<=1,  for elastic collision e=1
				//float e = (info.A->GetRestitution() + info.B->GetRestitution()) / 2.0f;
				//float newProjectedVelocity1 = 0.0f;
				//float newProjectedVelocity2 = 0.0f;

				//if(!info.A->IsStatic())
				//{
				//	if(info.B->IsStatic()) newProjectedVelocity1 = projectedVelocity1.X + (e + 1) * (projectedVelocity2.X - projectedVelocity1.X);
				//	else newProjectedVelocity1 = projectedVelocity1.X + (e + 1) * (projectedVelocity2.X - projectedVelocity1.X) / (1 + info.A->GetMass() / info.B->GetMass());
				//}
				//if(!info.B->IsStatic())
				//{
				//	if(info.A->IsStatic()) newProjectedVelocity2 = projectedVelocity2.X + (e + 1) * (projectedVelocity1.X - projectedVelocity2.X);
				//	else newProjectedVelocity2 = projectedVelocity2.X + (e + 1) * (projectedVelocity1.X - projectedVelocity2.X) / (1 + info.B->GetMass() / info.A->GetMass());
				//}

				////UnProject Velocitys
				//info.A->SetVelocity(Vector2(newProjectedVelocity1 * info.Normal.X - projectedVelocity1.Y * info.Normal.Y,
				//							newProjectedVelocity1 * info.Normal.Y + projectedVelocity1.Y * info.Normal.X));

				//info.B->SetVelocity(Vector2(newProjectedVelocity2 * info.Normal.X - projectedVelocity2.Y * info.Normal.Y,
				//							newProjectedVelocity2 * info.Normal.Y + projectedVelocity2.Y * info.Normal.X));

				//Back To The Future
				//info.A->Translate(info.A->GetVelocity() * dt);
				//info.B->Translate(info.B->GetVelocity() * dt);
		//}
	}

	void Simulator2D::EndTimeStep(GameTime gameTime, int count)
	{ for(int i = 0; i < count; i++) _shapes[i]->EndTimeStep(gameTime); }
}