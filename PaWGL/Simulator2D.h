#ifndef __Simulator_H__
#define __Simulator_H__

#include "List.h"
#include "UniqueList.h"
#include "Body.h"
#include "Circle.h"
#include "GameTime.h"

namespace Physics
{
	class Simulator2D
	{
	private:
		UniqueList<int> _shapesRemoveList;
		List<Body*> _shapes;
		Vector2 _gravity;
		AABB _worldBounds;

	public:
		Simulator2D(Vector2 gravity = Vector2(0, 9.8f));
		~Simulator2D(void);

		void AddBody(Body* Body);
		void RemoveBody(Body* Body);

		void Update(GameTime gameTime);

	private:
		void UpdateShapeList();
		void BeginTimeStep(GameTime gameTime, int count);
		void FindCollisions(GameTime gameTime, int count);
		void EndTimeStep(GameTime gameTime, int count);
	};
}

#endif