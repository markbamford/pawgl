#include "SoftBody.h"

namespace Physics
{
	SoftBody::SoftBody(const Vector2 position, const float mass, const float restitution, const float friction, const bool isStatic)
		: Body(position, mass, restitution, friction, isStatic), _centerMass(position, false, 0.01f), _rotationalMotionSystem(0.0f)
	{ _isSoftBody = true; }

	SoftBody::~SoftBody(void) { }

	List<PointMass>& SoftBody::GetPointMasses() { return _pointMasses; }
	PointMass& SoftBody::GetCenterPointMass() { return _centerMass; }
	//List<DampenedSpringMassSystem>& SoftBody::GetDampenedSpringMassSystems() { return _dampedSpringMassSystems; }
	List<DampenedVector2SpringMassSystem>& SoftBody::GetDampenedVector2SpringMassSystems() { return _dampedVector2SpringMassSystems; }

	void SoftBody::Rotate(float rotation) { _rotationalMotionSystem.Position += rotation; }

	float& SoftBody::GetRotationalDisplacement() { return _rotationalMotionSystem.Displacement; }

	void SoftBody::Translate(const Vector2 &displacement)
	{ 
		_motionSystem.Position += displacement; 
		for(int i = 0; i < _pointMasses.Count(); i++) _pointMasses[i].Translate(displacement);
		_centerMass.Translate(displacement);
	}
	void SoftBody::ApplyVelocity(Vector2 velocity, GameTime gameTime) { _motionSystem.ApplyVelocity(velocity, gameTime); }

	void SoftBody::SaveShape()
	{
		_shape.Clear();
		for(int i = 0; i < _pointMasses.Count(); i++) _shape.Add(_pointMasses[i].GetPosition());
	}

	void SoftBody::UpdateSides()
	{
		_sides.Clear();
		int Last = _pointMasses.Count() - 1;
		for(int i = 0; i < Last; i++)
			_sides.Add(Line2D(_pointMasses[i].GetPosition(), _pointMasses[i + 1].GetPosition()));
		_sides.Add(Line2D(_pointMasses[Last].GetPosition(), _pointMasses[0].GetPosition()));
	}

	void SoftBody::BeginTimeStep(GameTime gameTime, Vector2 gravity)
	{
		//Get Average Displacement
		Vector2 averageDisplacement;
		float averageRotation = 0.0f;
		int originalSign = 1;
        float originalAngle = 0;
        for (int i = 0; i < _pointMasses.Count(); i++)
		{
			averageDisplacement += _pointMasses[i].GetDisplacement();
			Vector2 relativePosition(_pointMasses[i].GetPosition() - _centerMass.GetPosition());
			float angle = relativePosition.AngleWith(_shape[i]);
			if(relativePosition.IsCounterClockwise(_shape[i])) angle = -angle;
			if (i == 0)
			{
				originalSign = (angle >= 0.0f) ? 1 : -1;
				originalAngle = angle;
			}
			else
			{
				float diff = (angle - originalAngle);
				int thisSign = (angle >= 0.0f) ? 1 : -1;

				if ((diff > PI || diff < -PI) && (thisSign != originalSign)) angle = (thisSign == -1) ? (TWOPI + angle) : (-angle);
			}
			averageRotation += angle;
		}
		averageDisplacement += _centerMass.GetDisplacement();

		averageDisplacement /= (float)(_pointMasses.Count() + 1);
		averageRotation /= (float)_pointMasses.Count();

		_rotationalMotionSystem.TimeStep(gameTime, 0.98f);

		averageRotation += _rotationalMotionSystem.Position;
		_rotationalMotionSystem.Position = 0.0f;

		//if(averageRotation >= PI) 
		//	averageRotation -= PI;
		//if(averageRotation <= -PI) 
		//	averageRotation += PI;
		//Take Average Displacement Away From PointMasses And Rotate Springs
		//Optimised
		float sinAngle = sinf(averageRotation);
		float cosAngle = cosf(averageRotation);
		for (int i = 0; i < _pointMasses.Count(); i++)
		{
			_pointMasses[i].SetDisplacement(_pointMasses[i].GetDisplacement() - averageDisplacement);

			//Rotate Edge Spring About Center
			int pointB = ((i == _pointMasses.Count() - 1) ? 0 : i + 1);
			Vector2 springVector(
				(cosAngle * (_shape[pointB].X - _shape[i].X)) - (sinAngle * (_shape[pointB].Y - _shape[i].Y)),
				(sinAngle * (_shape[pointB].X - _shape[i].X)) + (cosAngle * (_shape[pointB].Y - _shape[i].Y))
				);
			_dampedVector2SpringMassSystems[(i << 1)].Spring.NaturalLength = springVector;

			//Rotate Shape Spring
			Vector2 rotatedVector(
				(cosAngle * _shape[i].X) - (sinAngle * _shape[i].Y),
				(sinAngle * _shape[i].X) + (cosAngle * _shape[i].Y)
				);
			_dampedVector2SpringMassSystems[(i << 1) + 1].Spring.NaturalLength = rotatedVector;
		}
		_centerMass.SetDisplacement(_centerMass.GetDisplacement() - averageDisplacement);

		//Time Step Spring Mass Systems
		//for(int i = 0; i < _dampedSpringMassSystems.Count(); i++) _dampedSpringMassSystems[i].TimeStep(gameTime);
		for(int i = 0; i < _dampedVector2SpringMassSystems.Count(); i++) _dampedVector2SpringMassSystems[i].TimeStep(gameTime);

		//Time Step PointMasses
		for(int i = 0; i < _pointMasses.Count(); i++) _pointMasses[i].TimeStep(gameTime);
		_centerMass.TimeStep(gameTime);

		//Add Average Displacement To Motion
		_motionSystem.Displacement += averageDisplacement;

		//TimeStep Motion
		_motionSystem.TimeStep(gameTime, 0.999f, gravity);
		
		//Translate PointMasses By Overall Displacement and Add Overall Velocity
		for (int i = 0; i < _pointMasses.Count(); i++)
		{
			_pointMasses[i].Translate(_motionSystem.Displacement);
			_pointMasses[i].SetDisplacement(_pointMasses[i].GetDisplacement() + _motionSystem.Displacement);
		}
		_centerMass.Translate(_motionSystem.Displacement);
		_centerMass.SetDisplacement(_centerMass.GetDisplacement() + _motionSystem.Displacement);
		_motionSystem.Displacement = Vector2(0.0f, 0.0f);

		UpdateSides();
		CreateAABB();
	}

	void SoftBody::Collide(CollisionInfo &info)
	{ 
		_pointMasses[info.AVertice].Collide(info);
		if(_collideEvent) _collideEvent->Invoke(info);
	}

	void SoftBody::EndTimeStep(GameTime gameTime)
	{ 
		//Correct Motion System's Position (Not Nessessary For Simulation To Work)
		_motionSystem.Position = _centerMass.GetPosition();
		UpdateSides(); //Could Be Optimised?
	}
}
