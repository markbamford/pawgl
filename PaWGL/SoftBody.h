#ifndef _SoftBody_H_
#define _SoftBody_H_

#include "Body.h"
#include "DampenedVector2SpringMassSystem.h"
#include "DampenedSpringMassSystem.h"
#include "VerletMotionSystem.h"
#include "MathEx.h"

namespace Physics
{
	class SoftBody : public Body
	{
	public:
		List<Vector2> _shape;
		List<PointMass> _pointMasses;
		//List<DampenedSpringMassSystem> _dampedSpringMassSystems;
		List<DampenedVector2SpringMassSystem> _dampedVector2SpringMassSystems;
		PointMass _centerMass;
		VerletMotionSystem _rotationalMotionSystem;

	public:
		List<PointMass>& GetPointMasses();
		PointMass& GetCenterPointMass();
		List<DampenedSpringMassSystem>& GetDampenedSpringMassSystems();
		List<DampenedVector2SpringMassSystem>& GetDampenedVector2SpringMassSystems();

		SoftBody(const Vector2 position, const float mass = 1.0f, const float restitution = 0.9f, const float friction = 0.9f, const bool isStatic = false);
		~SoftBody(void);

		void Translate(const Vector2 &displacement);
		void ApplyVelocity(Vector2 velocity, GameTime gameTime);

		void Rotate(float rotation);
		float& GetRotationalDisplacement();

		void BeginTimeStep(GameTime gameTime, Vector2 gravity);
		void Collide(CollisionInfo &info);
		void EndTimeStep(GameTime gameTime);

		void SaveShape();
		void UpdateSides();
	};
}

#endif