#include "Spring.h"

namespace Physics
{
	Spring::Spring(float springConstant)
		: NaturalLength(0.0f), Elasticity(0.0f), SpringConstant(springConstant)
	{ }

	Spring::Spring(float naturalLength, float elasticity)
		: NaturalLength(naturalLength), Elasticity(elasticity), SpringConstant(elasticity / naturalLength)
	{ 
		if(naturalLength == 0.0f)
		{
			SpringConstant = elasticity; 
			Elasticity = 0.0f;
		}
	}

	float Spring::CalculateEnergyStored(float length)
	{ 
		float lengthExtension = length - NaturalLength;
		return SpringConstant * lengthExtension * lengthExtension * 0.5f;
	}

	float Spring::CalculateTension(float length)
	{ 
		float lengthExtension = length - NaturalLength;
		return -SpringConstant * lengthExtension; 
	}
}