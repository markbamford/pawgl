#ifndef _Spring_H_
#define _Spring_H_

#include "Vector2.h"

using namespace Math;

namespace Physics
{
	struct Spring
	{
public:	
		float SpringConstant;
		float NaturalLength;
		float Elasticity;

		Spring(float springConstant);
		Spring(float naturalLength, float elasticity);

		float CalculateEnergyStored(float length);
		float CalculateTension(float length);
	};
}

#endif