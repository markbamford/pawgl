#include "Sprite.h"

namespace Graphics
{
	Sprite::Sprite()
	{
		_position.X = _position.Y = 0.0f;
		Initialise();
	}

	Sprite::Sprite(float x, float y)
	{
		_position.X = x; _position.Y = y;
		Initialise();
	}

	Sprite::~Sprite()
	{ }

	void Sprite::Initialise(void)
	{
		_depth = 128.0f;
		_width = _height = 32.0f;
		_color.R = _color.G = _color.B = _color.A = 0x80;
		_matrix = Math::Matrix4x4::IDENTITY;
	}

	void Sprite::Render(void)
	{ }

	void Sprite::Translate(const float x, const float y)
	{
		_position.X += x;
		_position.Y += y;
	}

	void Sprite::Translate(const Vector2 v)
	{
		_position.X += v.X;
		_position.Y += v.Y;
	}

	void Sprite::Rotate(const float angle)
	{
		_rotation += angle;
	}

	Vector2 Sprite::GetPosition() { return _position; }
	void Sprite::SetPosition(const Vector2 position) { _position = position; }

	float Sprite::GetRotation() { return _rotation; }
	void Sprite::SetRotation(const float rotation) { _rotation = rotation; }

	float Sprite::GetDepth() { return _depth; }
	void Sprite::SetDepth(const float depth) { _depth = depth; }

	float Sprite::GetWidth() { return _width; }
	void Sprite::SetWidth(const float width) { _width = width; }

	float Sprite::GetHeight() { return _height; }
	void Sprite::SetHeight(const float height) { _height = height; }

	Color4 Sprite::GetColor() { return _color; }
	void Sprite::SetColor(const Color4 color) { _color = color; }
}