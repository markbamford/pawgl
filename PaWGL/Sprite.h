#ifndef _Sprite_H_
#define _Sprite_H_

#include "Rectangle.h"
#include "Vector2.h"
#include "Color4.h"
#include "Matrix4x4.h"
using namespace Math;

namespace Graphics
{
	class Sprite 
	{
	protected:
		Vector2 _position;
		float _rotation;
		float _depth; // z depth (big = near)
		float _width, _height;
		Color4 _color;
		
		Matrix4x4 _matrix;

	public:
		Vector2 GetPosition();
		void SetPosition(const Vector2 position);

		float GetRotation();
		void SetRotation(const float rotation);

		float GetDepth();
		void SetDepth(const float depth);

		float GetWidth();
		void SetWidth(const float width);

		float GetHeight();
		void SetHeight(const float height);

		Color4 GetColor();
		void SetColor(const Color4 color);

	public:

		Sprite::Sprite();
		Sprite::Sprite(const float x, const float y);
		Sprite::~Sprite();
		
		virtual void Render(void);
		
		void Translate(const float x, const float y);
		void Translate(const Vector2 v);

		void Rotate(const float angle);

	private:
		void Initialise(void);

	};

	
}

#endif