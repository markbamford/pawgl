#include "Surface.h"

Surface::Surface(Simulator2D* simulator, int sideCount, const Vector3 &position, const Vector2 &size, const float restitution, const float friction)
	: _simulator(simulator), _body(sideCount, Vector2(position.X, position.Y), size, 1.0f, restitution, friction, true),
	_triangleStrip((sideCount << 1) + 2),
	_transform(Matrix4x4::Translation(position))
{ 
	_simulator->AddBody(&_body);

	Vertice* v = _triangleStrip.GetVertices();
	for(int i = 0; i < sideCount; i++)
	{
		v[i << 1].Position = Vector3(_body.GetSides()[i].Start.X - position.X, _body.GetSides()[i].Start.Y - position.Y, position.Z);
		v[i << 1].TextureCoord = Vector2((_body.GetSides()[i].Start.X - position.X) / size.X, (_body.GetSides()[i].Start.Y - position.Y) / size.Y);
		v[(i << 1) + 1].Position = Vector3(0.0f, 0.0f, position.Z);
		v[(i << 1) + 1].TextureCoord = Vector2(0.0f, 0.0f);
	}
	v[sideCount << 1].Position = Vector3(_body.GetSides()[0].Start.X - position.X, _body.GetSides()[0].Start.Y - position.Y, position.Z);
	v[sideCount << 1].TextureCoord = Vector2((_body.GetSides()[0].Start.X - position.X) / size.X, (_body.GetSides()[0].Start.Y - position.Y) / size.Y);
	v[(sideCount << 1) + 1].Position = Vector3(0.0f, 0.0f, position.Z);
	v[(sideCount << 1) + 1].TextureCoord = Vector2(0.0f, 0.0f);
}

Surface::~Surface(void)
{
}

TriangleStrip* Surface::GetTriangleStrip() { return &_triangleStrip; }

void Surface::Update(GameTime gameTime)
{
}

void Surface::Draw(GraphicsManager* graphicsManager, Matrix4x4 transform)
{ 	
	AABB bounds;
	bounds.Upper = transform * _body.GetBounds().Upper;
	bounds.Lower = transform * _body.GetBounds().Lower;
	AABB screen;
	screen.Upper = Vector2(400.0f, 300.0f);
	screen.Lower = Vector2(-400.0f, -300.0f);

	if(bounds.Intersects(screen)) _triangleStrip.Draw(graphicsManager, _transform * transform); 
}