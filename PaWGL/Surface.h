#ifndef __Surface_H__
#define __Surface_H__

#include "RegularBody.h"
#include "TriangleStrip.h"
#include "List.h"
#include "Simulator2D.h"

using namespace Physics;
using namespace Graphics;

class Surface
{
private:
	Simulator2D* _simulator;
	RegularBody _body;
	TriangleStrip _triangleStrip;
	Matrix4x4 _transform;

public:
	Surface(Simulator2D* simulator, int sideCount, const Vector3 &position, const Vector2 &size, const float restitution = 0.7f, const float friction = 0.9f);
	~Surface(void);

	TriangleStrip* GetTriangleStrip();

	void Update(GameTime gameTime);
	void Draw(GraphicsManager* graphicsManager, Matrix4x4 transform = Matrix4x4::IDENTITY);
};

#endif