#ifndef __TGA_H__
#define __TGA_H__

namespace FileTypes
{
	struct TGAHeader
	{
		char DataOffset;
		char ColorType;
		char ImageType;
		unsigned short ColorPaletteOffset;
		unsigned short ColorPaletteSize;
		char ColorPaletteDepth;
		unsigned short XOffset;
		unsigned short YOffset;
		unsigned short Width;
		unsigned short Height;
		char BitsPerPixel;
		char Flags;
	};
}

#endif