//#include "TestGame.h"
//#include <iostream>
//using namespace std;
//
//TestGame::TestGame(
//#ifdef OPENGL 
//HINSTANCE hInstance)
//	: MultiplatformGame(hInstance, "Test Game")
//#endif
//#ifdef PS2 
//)
//	: MultiplatformGame()
//#endif
//	,_simulator(Vector2(0, 9.8f)),
//	//_aQuadBody(Vector2(0, -64.0f), 32.0f, 1.0f, 1.0f, 1.0f), 
//	_aQuad(32.0f, 32.0f),
//	//_aQuadBody(Vector2(128,128), Vector2(32.0f, 32.0f), 1.0f, 1.0f, 0.0f), 
//
//	_bQuadBody(Vector2(0, 32.0f), Vector2(128.0f, 32.0f), 1.0f, 1.0f, 0.0f, true), 
//	_bQuad(128.0f, 32.0f),
//	//_bQuadBody(Vector2(300,128), 16.0f, 1.0f, 1.0f, 1.0f), 
//	_ajelly(&_simulator, 8),
//	_bjelly(&_simulator, 32),
//	_aStrip(18), _bStrip(18)
//{ 
//	//_simulator.AddBody(_aQuadBody);
//	_simulator.AddBody(&_bQuadBody);
//	cout << "Assigning Textures" << endl;
//	_aQuad.SetTexture(GetGraphicsManager()->TextureFromBMP("Peg 256.bmp"));
//	_aQuad.SetUseAlpha(true);
//	_bQuad.SetTexture(GetGraphicsManager()->TextureFromBMP("Test256.bmp"));
//	cout << "Init Done" << endl << endl;
//
//	_ajelly.GetSoftBody()->Translate(Vector2(-100.0f, -100.0f));
//	_bjelly.GetSoftBody()->Translate(Vector2(100.0f, -100.0f));
//}
//
//TestGame::~TestGame(void)
//{
//}
//float R;
//Vector3 pos(0.0f//2048.0f
//			, 0.0f//2048.0f
//			, 8.0f);
//void TestGame::Update(GameTime gameTime)
//{
//	cout << "Update(" << gameTime.TotalElaspedTime << ")" << endl;
//#ifdef OPENGL
//	KeyboardState k = Input::States::GetKeyboardState();
//	if(k.IsKeyDown(Input::Keys::Right)) _ajelly.GetSoftBody()->ApplyVelocity(Vector2(1, 0), gameTime);
//	if(k.IsKeyDown(Input::Keys::Left)) _ajelly.GetSoftBody()->ApplyVelocity(Vector2(-1, 0), gameTime);
//	if(k.IsKeyDown(Input::Keys::Up)) _ajelly.GetSoftBody()->ApplyVelocity(Vector2(0, -1), gameTime);
//	if(k.IsKeyDown(Input::Keys::Down)) _ajelly.GetSoftBody()->ApplyVelocity(Vector2(0, 1), gameTime);
//
//	if(k.IsKeyDown(Input::Keys::D)) _bjelly.GetSoftBody()->ApplyVelocity(Vector2(1, 0), gameTime);
//	if(k.IsKeyDown(Input::Keys::A)) _bjelly.GetSoftBody()->ApplyVelocity(Vector2(-1, 0), gameTime);
//	if(k.IsKeyDown(Input::Keys::W)) _bjelly.GetSoftBody()->ApplyVelocity(Vector2(0, -1), gameTime);
//	if(k.IsKeyDown(Input::Keys::S)) _bjelly.GetSoftBody()->ApplyVelocity(Vector2(0, 1), gameTime);
//#endif
//
//	
//#ifdef PS2
//	_aQuad.SetLowerUV(_aQuad.GetLowerUV() + PS2Game::Player(0).LeftStick() / 10);
//	_aQuad.SetUpperUV(_aQuad.GetUpperUV() + PS2Game::Player(0).RightStick() / 10);
//#endif
//	_simulator.Update(gameTime);
//
//	//R = gameTime.TotalElaspedTime;
//}
//
//float Light_Ambient[] = {1.0f, 1.0f, 1.0f, 1.0f};
//float Light_Diffuse[] = {1.0f, 1.0f, 1.0f, 1.0f};
//float Light_Position[]= {0.0f, 0.0f, 6.0f, 1.0f};
//
//void TestGame::Draw()
//{
//	cout << "Draw" << endl;
//
//#ifdef OPENGL
//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);// Clear The Screen And The Depth Buffer
//	glLoadIdentity();// load Identity Matrix
//
//	//gluPerspective(45.0f, 4.0f / 3.0f, 1, 500.0f);
//
//	//set camera looking down the -z axis,  6 units away from the center
//	gluLookAt(0, 0, 100,     0, 0, -100,     0, -1, 0); //Where we are, What we look at, and which way is up
//	
//	glLightfv(GL_LIGHT1, GL_AMBIENT,  Light_Ambient);
//	glLightfv(GL_LIGHT1, GL_DIFFUSE,  Light_Diffuse);
//	//glLightfv(GL_LIGHT1, GL_POSITION, Light_Position);
//	glEnable(GL_LIGHT1);
//#endif
//
//#ifdef PS2
//	PS2Game::Console().PrintCenter("OMG WINZORZ", 0, -200);
//#endif
//	
//	//cout << "Sprite A (" << pos.X << ", " << pos.Y << ")" << endl;
//	//_aQuad.Draw(GetGraphicsManager(), Matrix4x4::RotationY(R) * 
//	//	//Matrix4x4::IDENTITY
//	//	Matrix4x4::Translation(
//	//	_aQuadBody.GetPosition())
//	//	);
//	cout << "Sprite B" << endl;
//	_bQuad.Draw(GetGraphicsManager(), //Matrix4x4::RotationX(R) * 
//		Matrix4x4::Translation(_bQuadBody.GetPosition()));
//
//	cout << "Viewport Test" << endl;
//
//	_aQuad.Draw(GetGraphicsManager(), Matrix4x4::Translation(-200, -200, 0));
//	_aQuad.Draw(GetGraphicsManager(), Matrix4x4::Translation(200, 200, 0));
//	_aQuad.Draw(GetGraphicsManager(), Matrix4x4::Translation(200, -200, 0));
//	_aQuad.Draw(GetGraphicsManager(), Matrix4x4::Translation(-200, 200, 0));
//
//	//Vertice* v = _aStrip.GetVertices();
//	//v[0].Position = Vector3(0, -32.0f, 1.0f);
//	//v[1].Position = Vector3(0, 0, 1.0f);
//	//v[2].Position = Vector3(32.0f * 0.75f, -32.0f * 0.75f, 1.0f);
//	//v[3].Position = Vector3(0, 0, 1.0f);
//	//v[4].Position = Vector3(32.0f, 0, 1.0f);
//	//v[5].Position = Vector3(0, 0, 1.0f);
//	//v[6].Position = Vector3(32.0f * 0.75f, 32.0f * 0.75f, 1.0f);
//	//v[7].Position = Vector3(0, 0, 1.0f);
//	//v[8].Position = Vector3(0, 32.0f, 1.0f);
//	//v[9].Position = Vector3(0, 0, 1.0f);
//	//v[10].Position = Vector3(-32.0f * 0.75f, 32.0f * 0.75f, 1.0f);
//	//v[11].Position = Vector3(0, 0, 1.0f);
//	//v[12].Position = Vector3(-32.0f, 0, 1.0f);
//	//v[13].Position = Vector3(0, 0, 1.0f);
//	//v[14].Position = Vector3(-32.0f * 0.75f, -32.0f * 0.75f, 1.0f);
//	//v[15].Position = Vector3(0, 0, 1.0f);
//	//v[16].Position = Vector3(0, -32.0f, 1.0f);
//
//
//	_ajelly.Draw(GetGraphicsManager());
//	_bjelly.Draw(GetGraphicsManager());
//
//	cout << "Next" << endl << endl;
//
//}