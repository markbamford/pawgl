#include "Texture.h"

namespace Graphics
{
	Texture::Texture(GraphicsManager* graphics, int id, int width, int height, short bitsPerPixel)
		: _graphics(graphics), _id(id), _width(width), _height(height), _bitsPerPixel(bitsPerPixel)
	{
	}

	Texture::~Texture(void)
	{
	}

	int Texture::GetWidth() { return _width; }

	int Texture::GetHeight() { return _height; }

	unsigned short Texture::GetBitsPerPixel() { return _bitsPerPixel; }

	int Texture::GetID() { return _id; }

	void Texture::Select()
	{ _graphics->SelectTexture(_id); }
}