#ifndef __Texture_H__
#define __Texture_H__
#include "GraphicsManager.h"

namespace Graphics
{
	class GraphicsManager;
	class Texture
	{
	protected:
		GraphicsManager* _graphics;
		int _id;

		int _width, _height;
		unsigned short _bitsPerPixel;

	public:
		Texture(GraphicsManager* graphics, int id, int width, int height, short bitsPerPixel);
		virtual ~Texture(void);

		int GetWidth();
		int GetHeight();
		unsigned short GetBitsPerPixel();
		int GetID();

		void Select();
	};
}

#endif