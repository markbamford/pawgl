#include "TriangleStrip.h"

namespace Graphics
{
	TriangleStrip::TriangleStrip(int verticeCount)
		: _texture(0), _useAlpha(false), _verticeCount(verticeCount), _vertices(new Vertice[verticeCount])
	{ }

	TriangleStrip::~TriangleStrip(void)
	{ delete _vertices; }

	Texture* TriangleStrip::GetTexture() { return _texture; }
	void TriangleStrip::SetTexture(Texture* texture)	{ _texture = texture; }

	bool TriangleStrip::GetUseAlpha() { return _useAlpha; }
	void TriangleStrip::SetUseAlpha(bool useAlpha) { _useAlpha = useAlpha; }

	Vertice* TriangleStrip::GetVertices() { return _vertices; }
	int TriangleStrip::GetVerticeCount() { return _verticeCount; }

	void TriangleStrip::Draw(GraphicsManager* graphics, Matrix4x4 m)
	{ 
		if(_texture) 
		{
			_texture->Select();
			graphics->EnableTexturing();
		}
		else graphics->DisableTexturing();

		if(_useAlpha) graphics->EnableAlphaBlend();
		else graphics->DisableAlphaBlend();

		graphics->BeginTriangleStrip(_verticeCount);

		for(int i = 0; i < _verticeCount; i++)
		{
			graphics->AddColor4(_vertices[i].Color);
			if(_texture)
			{
#ifdef PS2
				graphics->AddTextureCoord(Vector2(_texture->GetWidth() + _vertices[i].TextureCoord.X * (_texture->GetWidth() - 1), _texture->GetHeight() + _vertices[i].TextureCoord.Y * (_texture->GetHeight() - 1)));
#else
				graphics->AddTextureCoord(Vector2(_vertices[i].TextureCoord.X, _vertices[i].TextureCoord.Y));
#endif
			}
			graphics->AddNormal(_vertices[i].Normal);
			graphics->AddVertex(m * _vertices[i].Position);
		}

		graphics->End();
	}
}