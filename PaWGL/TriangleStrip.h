#ifndef __TriangleStrip_H__
#define __TriangleStrip_H__
#include "Vector3.h"
#include "Matrix4x4.h"
#include "Vertice.h"
#include "GraphicsManager.h"

using namespace Math;

namespace Graphics
{
	class TriangleStrip
	{
	private:
		int _verticeCount;
		Vertice* _vertices;
		Texture* _texture;
		bool _useAlpha;

	public:
		TriangleStrip(int verticeCount);
		~TriangleStrip(void);

		bool GetUseAlpha();
		void SetUseAlpha(bool useAlpha);

		Texture* GetTexture();
		void SetTexture(Texture* texture);

		Vertice* GetVertices();
		int GetVerticeCount();

		void Draw(GraphicsManager* graphics, Matrix4x4 m = Matrix4x4::IDENTITY);
	};
}

#endif