#include "TriggerArea.h"

namespace Physics
{
	TriggerArea::TriggerArea(const Vector2 &position, const Vector2 &size)
		: Body(position, 1.0f, 1.0f, 0.0f, true), _size(size)
	{ 
		CreateAABB(); 
		_isTriggerArea = true;
	}

	TriggerArea::~TriggerArea(void)
	{ }

	void TriggerArea::CreateAABB()
	{
		_bounds.Upper.X = _motionSystem.Position.X + _size.X * 0.5f;
		_bounds.Upper.Y = _motionSystem.Position.Y + _size.Y * 0.5f;
		_bounds.Lower.X = _motionSystem.Position.X - _size.X * 0.5f;
		_bounds.Lower.Y = _motionSystem.Position.Y - _size.Y * 0.5f;
	}

	void TriggerArea::BeginTimeStep(GameTime gameTime, Vector2 gravity)
	{ }
	void TriggerArea::Collide(CollisionInfo &info)
	{ if(_collideEvent) _collideEvent->Invoke(info); }
	void TriggerArea::EndTimeStep(GameTime gameTime)
	{ }

	bool TriggerArea::Intersects(Body* b, List<CollisionInfo> &info)
	{ 
		if(_bounds.Intersects(b->GetBounds())) 
		{
			CollisionInfo cinfo;
			cinfo.A = this;
			cinfo.B = b;
			info.Add(cinfo);
			return true;
		}
		return false;
	}
}