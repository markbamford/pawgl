#ifndef __TriggerArea_H__
#define __TriggerArea_H__

#include "Body.h"

namespace Physics
{
	class TriggerArea : public Body
	{
	private:
		Vector2 _size;

	public:
		TriggerArea(const Vector2 &position, const Vector2 &size);
		~TriggerArea(void);

		void BeginTimeStep(GameTime gameTime, Vector2 gravity);
		void Collide(CollisionInfo &info);
		void EndTimeStep(GameTime gameTime);

		void CreateAABB();
	protected:
		bool Intersects(Body* b, List<CollisionInfo> &info);
	};
}

#endif