#ifndef __UniqueList_H__
#define __UniqueList_H__

#include "IDValuePair.h"

template <class T> class UniqueList
{
private:
	int _count;
	int _capacity;
	IDValuePair<T>** _list;

public:
	UniqueList(void);
	UniqueList(int capacity);
	~UniqueList(void);

	T& operator[] (const int index);

	void Add(T &item, int id);
	void Remove(const int id);
	void RemoveAt(const int index);
	void Clear();
	bool Contains(const int id);
	int IndexOf(const int id);
	int Count();
	int GetCapacity();
	void SetCapacity(int capacity);
	T* GetArray();

private:
	void FixCapacity();
	static const int DEFAULTCAPACITY = 8;
};

template <class T> UniqueList<T>::UniqueList(void)
{
	_count = 0;
	_list = (IDValuePair<T>**)calloc(DEFAULTCAPACITY, sizeof(IDValuePair<T>*));
	if (_list == 0) throw "Unable To Allocate Memory";
	_capacity = DEFAULTCAPACITY;
}

template <class T> UniqueList<T>::UniqueList(int capacity)
{
	_count = 0;
	_list = (IDValuePair<T>**)calloc(capacity, sizeof(IDValuePair<T>*));
	if (_list == 0) throw "Unable To Allocate Memory";
	_capacity = capacity;
}

template <class T> UniqueList<T>::~UniqueList(void)
{ 
	for(int i = 0; i < _capacity; i++)
		if(_list[i]) delete _list[i];
}

template <class T> T& UniqueList<T>::operator[] (const int index)
{
	if(index < 0 || index >= _count) throw "Index Out Of Range!";
	return *(_list[index]->Value);
}

template <class T> void UniqueList<T>::Add(T &item, const int id)
{
	//Perform a Binary Search
	int low = 0;
	int high = _count;
	int mid;
	
	while(low < high)
	{
		mid = low + ((high - low) >> 1);
		if(_list[mid]->ID < id) low = mid + 1;
		else high = mid;
	}
	//If item is found, doesn't need to be added
	if (((low < _count) && (_list[low]->ID == id))) return;
	//Else add in-order of id (current value of low), this allows the use of the binary search
	FixCapacity();
	memmove(&_list[low + 1], &_list[low], (_count - low) * sizeof(IDValuePair<T>*));
	_list[low] = new IDValuePair<T>(item, id);
	_count++;
}

template <class T> void UniqueList<T>::Remove(const int id)
{ RemoveAt(IndexOf(id)); }

template <class T> void UniqueList<T>::RemoveAt(int index)
{
	if(index < 0 || index >= _count) throw "Index Out Of Range!";
	free(_list[index]);
	_list[index] = 0;
	memmove(&_list[index], &_list[index + 1], (_count - (index + 1)) * sizeof(IDValuePair<T>*));
	_count--;
}

template <class T> void UniqueList<T>::Clear()
{	
	if(_count == 0) return;
	for(int i = 0; i < _count; i++)
		delete _list[i];
	memset(_list, 0, _count * sizeof(T*));
	_count = 0;
}

template <class T> bool UniqueList<T>::Contains(const int id)
{ return (IndexOf(id) > -1); }

template <class T> int UniqueList<T>::IndexOf(const int id)
{ 
	int low = 0;
	int high = _count;
	int mid;
	
	while(low < high)
	{
		mid = low + ((high - low) >> 1);
		if(_list[mid]->ID < id) low = mid + 1;
		else high = mid;
	}
	if ((low < _count) && (_list[low]->ID == id)) return low;
	return -1;
}

template <class T> int UniqueList<T>::Count()
{ return _count; }

template <class T> int UniqueList<T>::GetCapacity()
{ return _capacity; }

template <class T> void UniqueList<T>::SetCapacity(int capacity)
{
	if(capacity < 0) throw "Capacity Cannot Be Less Than Zero!";
	IDValuePair<T>** temp = (IDValuePair<T>**)realloc(_list, capacity * sizeof(IDValuePair<T>*));
	if (temp == 0) throw "Unable To Allocate Memory";
	_list = temp;
	if(capacity > _capacity) memset(&_list[_capacity], 0, (capacity - _capacity) * sizeof(IDValuePair<T>*));
	_capacity = capacity;
}

template <class T> void UniqueList<T>::FixCapacity()
{ if(_count == _capacity) SetCapacity(_capacity << 1); }

template <class T> T* UniqueList<T>::GetArray()
{ 
	T* _array = (T*)calloc(_count, sizeof(T));
	for(int i = 0; i < _count; i++) memcpy(&_array[i], _list[i]->Value, sizeof(T));
	return _array;
}

#endif