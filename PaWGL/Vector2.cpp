#include "Vector2.h"

namespace Math
{
	Vector2::Vector2()
		: X(0), Y(0)
	{ }

	Vector2::Vector2(float x, float y)
		: X(x), Y(y)
	{ }
	
	float Vector2::DotProduct(const Vector2& b) const
	{ return X * b.X + Y * b.Y; }

	Vector2 Vector2::Perpendicular() const
	{ return Vector2(-Y, X); }

	float Vector2::GetLength() const
	{ return sqrt((X * X) + (Y * Y)); }

	float Vector2::GetLengthSquared() const
	{ return (X * X) + (Y * Y); }

	Vector2 Vector2::Normalize() const
	{ return Vector2(X, Y) / GetLength(); }

	float Vector2::AngleWith(const Vector2& b) const
	{ 
		float dotABOverlenAlenB = this->DotProduct(b) / (this->GetLength() * b.GetLength());
		if(dotABOverlenAlenB > 1.0f) dotABOverlenAlenB = 1.0f;
		else if(dotABOverlenAlenB < -1.0f) dotABOverlenAlenB = -1.0f;
		return acosf(dotABOverlenAlenB);
	}

	Vector2 Vector2::RotateBy(const float &angle) const
	{ return Vector2((cosf(angle) * this->X) - (sinf(angle) * this->Y), (sinf(angle) * this->X) + (cosf(angle) * this->Y)); }

	Vector2 Vector2::RotateAbout(const Vector2 &b, const float &angle) const
	{ 
		Vector2 translatedVector(*this - b);
		translatedVector = translatedVector.RotateBy(angle);
		return translatedVector + b;
	}

	bool Vector2::IsCounterClockwise(const Vector2 &b) const
	{ return (b.DotProduct(this->Perpendicular()) > 0.0f); }

	Vector2& Vector2::operator+=(const Vector2& b)
	{ 
		X += b.X;
		Y += b.Y;
		return (*this);
	}

	Vector2& Vector2::operator-=(const Vector2& b)
	{ 
		X -= b.X;
		Y -= b.Y;
		return (*this);
	}

	Vector2& Vector2::operator*=(const Vector2& b)
	{ 
		X *= b.X;
		Y *= b.Y;
		return (*this);
	}

	Vector2& Vector2::operator/=(const Vector2& b)
	{ 
		X /= b.X;
		Y /= b.Y;
		return (*this);
	}

	Vector2& Vector2::operator*=(const float& b)
	{ 
		X *= b;
		Y *= b;
		return (*this);
	}

	Vector2& Vector2::operator/=(const float& b)
	{ 
		X /= b;
		Y /= b;
		return (*this);
	}
}