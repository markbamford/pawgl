#ifndef __Vector2_H__
#define __Vector2_H__

#include "MathEx.h"

namespace Math
{
	struct Vector2
	{				
		float X, Y;

		Vector2();
		Vector2(float x, float y);

		float DotProduct(const Vector2 &b) const;
		Vector2 Perpendicular() const;
		
		float GetLength() const;
		float GetLengthSquared() const;
		Vector2 Normalize() const;

		float AngleWith(const Vector2 &b) const;
		Vector2 RotateBy(const float &angle) const;
		Vector2 RotateAbout(const Vector2 &b, const float &angle) const;

		bool IsCounterClockwise(const Vector2 &b) const;

		Vector2& Vector2::operator+=(const Vector2& b);
		Vector2& Vector2::operator-=(const Vector2& b);
		Vector2& Vector2::operator*=(const Vector2& b);
		Vector2& Vector2::operator/=(const Vector2& b);
		Vector2& Vector2::operator*=(const float& b);
		Vector2& Vector2::operator/=(const float& b);
	};

	inline Vector2 operator+(const Vector2& a, const Vector2& b)
	{ return Vector2(a.X + b.X, a.Y + b.Y); }

	inline Vector2 operator-(const Vector2& a, const Vector2& b)
	{ return Vector2(a.X - b.X, a.Y - b.Y); }

	inline Vector2 operator-(const Vector2& a)
	{ return Vector2(-a.X, -a.Y); }

	inline Vector2 operator*(const Vector2& a, const Vector2& b)
	{ return Vector2(a.X * b.X, a.Y * b.Y); }

	inline Vector2 operator/(const Vector2& a, const Vector2& b)
	{ return Vector2(a.X / b.X, a.Y / b.Y); }

	inline Vector2 operator*(const Vector2& a, const float& b)
	{ return Vector2(a.X * b, a.Y * b); }

	inline Vector2 operator/(const Vector2& a, const float& b)
	{ return Vector2(a.X / b, a.Y / b); }
}

#endif