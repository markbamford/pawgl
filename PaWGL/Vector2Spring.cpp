#include "Vector2Spring.h"

namespace Physics
{
	Vector2Spring::Vector2Spring(Vector2 naturalLength, float elasticity)
		: NaturalLength(naturalLength), Elasticity(elasticity), SpringConstant(Elasticity)
	{ 
		float naturalLengthLength = naturalLength.GetLength();
		if(naturalLengthLength != 0.0f) SpringConstant = Elasticity / naturalLengthLength;
		else Elasticity = 0.0f;
	}

	Vector2Spring::Vector2Spring(float springConstant)
		: NaturalLength(0.0f, 0.0f), Elasticity(0.0f), SpringConstant(springConstant)
	{ }

	Vector2 Vector2Spring::CalculateEnergyStored(Vector2 length)
	{ 
		Vector2 lengthExtension(length - NaturalLength);
		return lengthExtension * lengthExtension * 0.5f * SpringConstant; 
	}

	Vector2 Vector2Spring::CalculateTension(Vector2 length)
	{ 
		Vector2 lengthExtension(length - NaturalLength);
		return lengthExtension * -SpringConstant; 
	}
}