#ifndef _Vector2Spring_H_
#define _Vector2Spring_H_

#include "Vector2.h"

using namespace Math;

namespace Physics
{
	struct Vector2Spring
	{
public:	
		float SpringConstant;
		Vector2 NaturalLength;
		float Elasticity;

		Vector2Spring(Vector2 naturalLength, float elasticity);
		Vector2Spring(float springConstant);

		Vector2 CalculateEnergyStored(Vector2 length);
		Vector2 CalculateTension(Vector2 length);
	};
}

#endif