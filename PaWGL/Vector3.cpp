#include "Vector3.h"

namespace Math
{
	Vector3::Vector3()
		: X(0), Y(0), Z(0)
	{ }

	Vector3::Vector3(float x, float y, float z)
		: X(x), Y(y), Z(z)
	{ }

	Vector3::Vector3(Vector2 vector2)
		: X(vector2.X), Y(vector2.Y), Z(0)
	{ }

	float Vector3::DotProduct(const Vector3& b) const
	{ return X * b.X + Y * b.Y + Z * b.Z; }

	Vector3 Vector3::CrossProduct(const Vector3 &b) const
	{ return Vector3(Y * b.Z - Z * b.Y, Z * b.X - X * b.Z, X * b.Y - Y * b.X); }

	float Vector3::GetLength()
	{ return sqrt(X * X + Y * Y + Z * Z); }

	Vector3 Vector3::Normalize()
	{ return Vector3(X, Y, Z) / GetLength(); }

	Vector3& Vector3::operator+=(const Vector3& b)
	{ 
		X += b.X;
		Y += b.Y;
		Z += b.Z;
		return (*this);
	}

	Vector3& Vector3::operator-=(const Vector3& b)
	{ 
		X -= b.X;
		Y -= b.Y;
		Z -= b.Z;
		return (*this);
	}

	Vector3& Vector3::operator*=(const Vector3& b)
	{ 
		X *= b.X;
		Y *= b.Y;
		Z *= b.Z;
		return (*this);
	}

	Vector3& Vector3::operator/=(const Vector3& b)
	{ 
		X /= b.X;
		Y /= b.Y;
		Z /= b.Z;
		return (*this);
	}

	Vector3& Vector3::operator*=(const float& b)
	{ 
		X *= b;
		Y *= b;
		Z *= b;
		return (*this);
	}

	Vector3& Vector3::operator/=(const float& b)
	{ 
		X /= b;
		Y /= b;
		Z /= b;
		return (*this);
	}
}