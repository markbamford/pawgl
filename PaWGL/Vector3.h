#ifndef __Vector3_H__
#define __Vector3_H__

#include "MathEx.h"
#include "Vector2.h"

namespace Math
{
	struct Vector3
	{				
		float X, Y, Z;

		Vector3();
		Vector3(float x, float y, float z);
		Vector3(Vector2 vector2);

		float DotProduct(const Vector3&) const;
		Vector3 CrossProduct(const Vector3&) const;
		float GetLength();
		Vector3 Normalize();

		Vector3& Vector3::operator+=(const Vector3& b);
		Vector3& Vector3::operator-=(const Vector3& b);
		Vector3& Vector3::operator*=(const Vector3& b);
		Vector3& Vector3::operator/=(const Vector3& b);
		Vector3& Vector3::operator*=(const float& b);
		Vector3& Vector3::operator/=(const float& b);
	};

	inline Vector3 operator+(const Vector3& a, const Vector3& b)
	{ return Vector3(a.X + b.X, a.Y + b.Y, a.Z + b.Z); }

	inline Vector3 operator-(const Vector3& a, const Vector3& b)
	{ return Vector3(a.X - b.X, a.Y - b.Y, a.Z - b.Z); }

	inline Vector3 operator-(const Vector3& a)
	{ return Vector3(-a.X, -a.Y, -a.Z); }

	inline Vector3 operator*(const Vector3& a, const Vector3& b)
	{ return Vector3(a.X * b.X, a.Y * b.Y, a.Z * b.Z); }

	inline Vector3 operator/(const Vector3& a, const Vector3& b)
	{ return Vector3(a.X / b.X, a.Y / b.Y, a.Z / b.Z); }

	inline Vector3 operator*(const Vector3& a, const float& b)
	{ return Vector3(a.X * b, a.Y * b, a.Z * b); }

	inline Vector3 operator/(const Vector3& a, const float& b)
	{ return Vector3(a.X / b, a.Y / b, a.Z / b); }
}

#endif