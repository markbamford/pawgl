#include "Vector4.h"

namespace Math
{
	Vector4::Vector4(void)
	{
	}

	Vector4::Vector4(const float _x, const float _y, const float _z, const float _w)
	:X(_x),Y(_y),Z(_z),W(_w)
	{
	}

	Vector4::Vector4(const Vector4 &rhs)
	:X(rhs.X),Y(rhs.Y),Z(rhs.Z),W(rhs.W)
	{
	}

	Vector4::~Vector4(void)
	{
	}

	Vector4 & Vector4::operator +=(const Vector4 &rhs)
	{
		X += rhs.X;
		Y += rhs.Y;
		Z += rhs.Z;
		W = 1.0f;

		return *this;
	}

	Vector4 & Vector4::operator -=(const Vector4 &rhs)
	{
		X -= rhs.X;
		Y -= rhs.Y;
		Z -= rhs.Z;
		W = 1.0f;

		return *this;
	}

	float Vector4::Dot4(const Vector4 &rhs) const
	{
		return (X * rhs.X + Y * rhs.Y + Z * rhs.Z + W * rhs.W);
	}

	float Vector4::Dot3(const Vector4 &rhs) const
	{
		return (X * rhs.X + Y * rhs.Y + Z * rhs.Z);
	}

	Vector4 Vector4::Cross(const Vector4 &rhs) const
	{
		return Vector4(Y * rhs.Z - Z * rhs.Y, Z * rhs.X - X * rhs.Z, X * rhs.Y - Y * rhs.X, 1.0f); 
	}

	Vector4 & Vector4::operator *=(const float s)
	{
		X *= s;
		Y *= s;
		Z *= s;
		W  = 1.0f;

		return *this;
	}

	Vector4 & Vector4::operator /=(const float s)
	{
		X /= s;
		Y /= s;
		Z /= s;
		W = 1.0f;;

		return *this;
	}

	float Vector4::Length() const
	{
		return sqrt(X * X + Y * Y + Z * Z);
	}

	float Vector4::LengthSqr() const
	{
		return (X * X + Y * Y + Z * Z);
	}


	bool Vector4::operator ==(const Vector4 & rhs) const
	{
		return ((X == rhs.X) && (Y == rhs.Y) && (Z == rhs.Z) && (W == rhs.W));
	}

	Vector4 Vector4::Normalise()
	{
		return (*this / this->Length());
	}

	void Vector4::NormaliseSelf()
	{
		*this /= this->Length();
	}

	void Vector4::DumpVector4(char * s)
	{
		if(s != NULL)printf("\n%f %f %f %f %s\n\n", X, Y, Z, W, s);
		else printf("\n%f %f %f %f\n\n", X, Y, Z, W);
	}
}