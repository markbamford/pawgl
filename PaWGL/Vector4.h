#ifndef __VECTOR4_H__
#define __VECTOR4_H__

#include "MathEx.h"
#include <stdio.h>

namespace Math
	{
	class Vector4
	{
	public:
		// Constructors
		Vector4(void);
		Vector4(const float _x, const float _y, const float _z, const float _w);
		Vector4(const Vector4 & rhs);

		~Vector4(void);

		// Operations with other vectors
		Vector4 & operator+=(const Vector4 & rhs);
		Vector4 & operator-=(const Vector4 & rhs);

		// Special arithmetic
		float Dot3(const Vector4 & rhs) const;
		float Dot4(const Vector4 & rhs) const;
		Vector4 Cross(const Vector4 & rhs) const;

		Vector4 & operator*=(const float s);
		Vector4 & operator/=(const float s);

		bool operator==(const Vector4 & rhs) const;

		// Miscellaneous
		float Length() const;
		float LengthSqr() const;
		Vector4 Normalise();
		void NormaliseSelf();
		void DumpVector4(char * s = NULL);
		
		// Member data
		float X,Y,Z,W; 
	};

	inline Vector4 operator + (const Vector4 &v1,
							   const Vector4 &v2)
	{
		return Vector4(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z, 1.0f);
	}

	inline Vector4 operator - (const Vector4 &v1,
							   const Vector4 &v2)
	{
		return Vector4(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z, 1.0f);
	}

	inline Vector4 operator - (const Vector4 &v1)
	{
		return Vector4(-v1.X, -v1.Y, -v1.Z, 1.0f);
	}

	inline Vector4 operator * (const Vector4 &v,
							   const float &s)
	{
		return Vector4(v.X * s, v.Y * s, v.Z * s, 1.0f);
	}

	inline Vector4 operator * (const float & s,
							   const Vector4 &v)
	{
		return Vector4(v.X * s, v.Y * s, v.Z * s, 1.0f);
	}

	inline Vector4 operator / (const Vector4 &v,
							   const float & s)
	{
		return Vector4(v.X / s, v.Y / s, v.Z / s, 1.0f);
	}

	inline Vector4 CrossProduct (const Vector4 &v1,
								 const Vector4 &v2)
	{
		return Vector4(v1.Y * v2.Z - v1.Z * v2.Y,
					   v1.Z * v2.X - v1.X * v2.Z,
					   v1.X * v2.Y - v1.Y * v2.X,
					   1.0f);
	}

	inline float DotProduct4(const Vector4 &v1,
							 const Vector4 &v2)
	{
		return (v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z + v1.W * v2.W);
	}

	inline float DotProduct3(const Vector4 &v1,
							 const Vector4 &v2)
	{
		return (v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z);
	}

	inline Vector4 Normalise (const Vector4 &v)
	{
		return v / v.Length();
	}
}
#endif
