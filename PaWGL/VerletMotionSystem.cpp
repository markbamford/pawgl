#include "VerletMotionSystem.h"

namespace Physics
{
	VerletMotionSystem::VerletMotionSystem(const float position)
		: Position(position), Displacement(0.0f), Acceleration(0.0f)
	{}

	void VerletMotionSystem::ApplyVelocity(const float velocity, GameTime gameTime)
	{ Displacement += velocity * gameTime.ElaspedTime; }

	float VerletMotionSystem::CalculateVelocity(GameTime gameTime)
	{ return Displacement * gameTime.ElaspedTime; }

	void VerletMotionSystem::TimeStep(GameTime gameTime, float dragCoefficient)
	{ 
		float PosCopy(Position);
		Position = Position + (Displacement * dragCoefficient) + Acceleration * gameTime.ElaspedTime * gameTime.ElaspedTime;
		Displacement = Position - PosCopy;
	}
}