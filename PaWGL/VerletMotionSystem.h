#ifndef __VerletMotionSystem_H__
#define __VerletMotionSystem_H__

#include "GameTime.h"

namespace Physics
{
	struct VerletMotionSystem
	{
	public:
		float Acceleration;
		float Displacement;
		float Position;

		VerletMotionSystem(const float position = 0.0f);

		void ApplyVelocity(const float velocity, GameTime gameTime);
		float CalculateVelocity(GameTime gameTime);

		void TimeStep(GameTime gameTime, float dragCoefficient = 1.0f);
	};
}

#endif