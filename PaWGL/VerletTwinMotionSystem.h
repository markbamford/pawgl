#pragma once
#include "GameTime.h"
#include "Vector2.h"

using namespace Math;

struct VerletTwinMotionSystem
{
	public:
		Vector2 Acceleration1;
		Vector2 PreviousPosition1;
		Vector2 Position1;

		Vector2 Acceleration2;
		Vector2 PreviousPosition2;
		Vector2 Position2;

		VerletTwinMotionSystem(const Vector2 position = Vector2(0, 0));

		void Translate(const Vector2 displacement);

		void TimeStep(GameTime gameTime, float dragCoefficient1 = 1.0f, float dragCoefficient2 = 1.0f, const Vector2 gravity = Vector2(0, 0));
};
