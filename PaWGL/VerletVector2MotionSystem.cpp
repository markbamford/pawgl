#include "VerletVector2MotionSystem.h"

namespace Physics
{
	VerletVector2MotionSystem::VerletVector2MotionSystem(const Vector2 position)
		: Position(position), Acceleration(0, 0)
	{}

	void VerletVector2MotionSystem::ApplyVelocity(const Vector2 velocity, GameTime gameTime)
	{ Displacement += velocity * gameTime.ElaspedTime; }

	Vector2 VerletVector2MotionSystem::CalculateVelocity(GameTime gameTime)
	{ return Displacement * gameTime.ElaspedTime; }

	void VerletVector2MotionSystem::TimeStep(GameTime gameTime, float dragCoefficient, const Vector2 gravity)
	{ 
		Vector2 PosCopy(Position);
		Position = Position + (Displacement * dragCoefficient) + (Acceleration + gravity) * gameTime.ElaspedTime * gameTime.ElaspedTime;
		Displacement = Position - PosCopy;
	}
}