#ifndef __VerletVector2MotionSystem_H__
#define __VerletVector2MotionSystem_H__

#include "GameTime.h"
#include "Vector2.h"

using namespace Math;

namespace Physics
{
	struct VerletVector2MotionSystem
	{
	public:
		Vector2 Acceleration;
		Vector2 Displacement;
		Vector2 Position;

		VerletVector2MotionSystem(const Vector2 position = Vector2(0, 0));

		void ApplyVelocity(const Vector2 velocity, GameTime gameTime);
		Vector2 CalculateVelocity(GameTime gameTime);

		void TimeStep(GameTime gameTime, float dragCoefficient = 1.0f, const Vector2 gravity = Vector2(0, 0));
	};
}

#endif