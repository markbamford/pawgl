#include "Vertice.h"

namespace Graphics
{
	Vertice::Vertice(Vector3 position, Vector3 normal, Color4 color, Vector2 textureCoord)
		: Position(position), Normal(normal), Color(color), TextureCoord(textureCoord)
	{ }
}