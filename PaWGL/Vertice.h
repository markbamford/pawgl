#ifndef __Vertice_H__
#define __Vertice_H__

#include "Vector2.h"
#include "Vector3.h"
#include "Color4.h"
#include "Matrix4x4.h"
#include "GraphicsManager.h"

using namespace Math;

namespace Graphics
{
	struct Vertice
	{
		Vector3 Position;
		Vector3 Normal;
		Color4 Color;
		Vector2 TextureCoord;

		Vertice(Vector3 position = Vector3(0.0f, 0.0f, 0.0f), Vector3 normal = Vector3(0.0f, 0.0f, 1.0f), Color4 color = Color4(1.0f, 1.0f, 1.0f, 1.0f), Vector2 textureCoord = Vector2(0.0f, 0.0f));
	};
}

#endif