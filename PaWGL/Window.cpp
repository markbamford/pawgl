#include "Window.h"

namespace Windows
{
	Queue<Window::Message> Window::_messageQueue;
	List<Window*> Window::_windows;
	HINSTANCE Window::_hInstance;

	HWND Window::GetHandle()
	{
		return _hwnd;
	}

	RECT Window::GetBounds()
	{ return _bounds; }

	Window::Window(HINSTANCE hInstance, LPCSTR title, bool showWindow, int width, int height, DWORD dwStyle, DWORD dwExStyle)
	{
		Window::_hInstance = hInstance;

		WNDCLASS wcex;
		memset(&wcex, 0, sizeof(WNDCLASS));
		wcex.style			= CS_HREDRAW | CS_VREDRAW;		
		wcex.lpfnWndProc	= WndProc;
		wcex.hInstance		= hInstance;						
		//wcex.hIcon			= LoadIcon(NULL, IDI_APPLICATION);
		wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);		
		wcex.hbrBackground	= (HBRUSH)GetStockObject(BLACK_BRUSH);//(HBRUSH) (COLOR_WINDOW+1);
		wcex.lpszMenuName	= NULL;	
		wcex.lpszClassName	= "WindowClass";	
		wcex.cbWndExtra     = sizeof(Window*);
		RegisterClass(&wcex);

		_hwnd = CreateWindowEx(dwExStyle,
			"WindowClass",
			title,
			dwStyle,
			CW_USEDEFAULT,
			CW_USEDEFAULT,
			width,
			height,
			NULL,
			NULL,
			hInstance,
			this
			);

		if (!_hwnd) throw "Error Creating Window";

		LONG_PTR p = ::GetWindowLongPtr(_hwnd, 0);
		Window* pThis = (Window*)p;
		if(pThis != this) throw "Error Creating Window";

		//GetClientRect(_hwnd, &_bounds);
		::GetWindowRect(_hwnd, &_bounds);
		::SetWindowLongPtr(_hwnd, 0, (LONG_PTR)pThis);		

		Window::_windows.Add((Windows::Window*)this);

		Show();
		UpdateWindow(_hwnd);
	}

	Window::~Window(void)
	{
		Window::_windows.Remove((Windows::Window*)this);
		::SetWindowLong(_hwnd, 0, 0);
	}

	void Window::OnResize(WORD x, WORD y)
	{ GetWindowRect(_hwnd, &_bounds); }

	void Window::Show()
	{ ShowWindow(_hwnd, 1); }

	void Window::Hide()
	{ ShowWindow(_hwnd, 0); }

	void Window::Minimize()
	{ CloseWindow(_hwnd); }

	void Window::Close()
	{ DestroyWindow(_hwnd); }

	void Window::OnKeyDown(Keys::Enum k) { }
	void Window::OnKeyUp(Keys::Enum k) { }
	void Window::OnMouseMove(WORD x, WORD y) { }
	void Window::OnLeftMouseDown() { }
	void Window::OnRightMouseDown() { }
	void Window::OnLeftMouseUp() { }
	void Window::OnRightMouseUp() { }
	void Window::OnFrameUpdate() { }
	void Window::OnFrameDraw() { }

	LRESULT CALLBACK Window::WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
	{		
		switch(message)											
		{	
			case WM_DESTROY:	
				UnregisterClass("WindowClass", _hInstance);// Free the window class
				PostQuitMessage(0);	
				break;
			case WM_CREATE:
			{
				CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
				::SetWindowLongPtr(hwnd, 0, (LONG_PTR)(cs->lpCreateParams));
				break;
			}
			case WM_MOVING:
				::GetWindowRect(hwnd, &((Window*)GetWindowLongPtr(hwnd, 0))->_bounds);
			default:
				_messageQueue.Enqueue(Window::Message(hwnd, message, wParam, lParam));
				break;
		}

		return DefWindowProc(hwnd, message, wParam, lParam);
	}

	WPARAM Window::MessageLoop()
	{
		MSG msg;
		
		while (TRUE)					
		{							
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				if (msg.message == WM_QUIT) break;

				TranslateMessage(&msg);							
				DispatchMessage(&msg);
			}
			else
			{
				while(Window::_messageQueue.Count() > 0)
				{
					Window::Message m = Window::_messageQueue.Dequeue();

					LONG_PTR p = ::GetWindowLongPtr(m.hwnd, 0);
						
					if(p)
					{
						switch(m.message)											
						{
							case WM_SIZE:
								((Window*)p)->OnResize(LOWORD(m.lParam), HIWORD(m.lParam));
								break;	
							case WM_KEYDOWN:
								((Window*)p)->OnKeyDown((Keys::Enum)m.wParam);
								break;
							case WM_KEYUP:
								((Window*)p)->OnKeyUp((Keys::Enum)m.wParam);
								break;
							case WM_MOUSEMOVE:
								((Window*)p)->OnMouseMove(LOWORD(m.lParam), HIWORD(m.lParam));
								break;
							case WM_LBUTTONDOWN:
								((Window*)p)->OnLeftMouseDown();
								break;	
							case WM_RBUTTONDOWN:
								((Window*)p)->OnRightMouseDown();
								break;	
							case WM_LBUTTONUP:
								((Window*)p)->OnLeftMouseUp();
								break;	
							case WM_RBUTTONUP:
								((Window*)p)->OnRightMouseUp();
								break;	
						}
					}
				}

				for(int i = 0; i < Window::_windows.Count(); i++)
				{
					_windows[i]->OnFrameUpdate();
					_windows[i]->OnFrameDraw();
				}
			}
		}
		return msg.wParam;
	}
}