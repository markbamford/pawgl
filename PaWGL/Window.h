#pragma once
#include "Queue.h"
#include "List.h"
#include "Keys.h"
#include <windows.h>
#include <stdio.h>
using namespace Input;

namespace Windows
{
	class Window
	{
	protected:
		RECT _bounds;

	private:
		HWND _hwnd;
		static HINSTANCE _hInstance;

	public:
		HWND GetHandle();
		RECT GetBounds();

	public:
		Window(HINSTANCE hInstance, LPCSTR title = "Window Title", bool showWindow = true, int width = CW_USEDEFAULT, int height = CW_USEDEFAULT, DWORD dwStyle = WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, DWORD dwExStyle = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE);
		virtual ~Window(void);

		void Show();
		void Hide();
		void Minimize();
		void Close();

	protected:
		virtual void OnResize(WORD x, WORD y);
		virtual void OnKeyDown(Keys::Enum k);
		virtual void OnKeyUp(Keys::Enum k);
		virtual void OnMouseMove(WORD x, WORD y);
		virtual void OnLeftMouseDown();
		virtual void OnRightMouseDown();
		virtual void OnLeftMouseUp();
		virtual void OnRightMouseUp();
		virtual void OnFrameUpdate();
		virtual void OnFrameDraw();

	private:
		static LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam);
		
		struct Message
		{
			HWND hwnd;
			UINT message;
			WPARAM wParam;
			LPARAM lParam;

			Message()
			{ }

			Message(HWND hWnd, UINT msg, WPARAM WParam, LPARAM LParam)
			{
				hwnd = hWnd;
				message = msg;
				wParam = WParam;
				lParam = LParam;
			}
		};
		static Queue<Window::Message> _messageQueue;
		static List<Window*> _windows;

	public:
		static WPARAM MessageLoop();

	};
}