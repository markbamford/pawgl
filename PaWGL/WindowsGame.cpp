#include "WindowsGame.h"

WindowsGame::WindowsGame(HINSTANCE hInstance, LPCSTR title)
	: _window(new Windows::GameWindow(this, title, hInstance, true))
{ Initialise(new WindowsGraphicsManager(_window->GetHandle())); }

WindowsGame::~WindowsGame()
{ }

int WindowsGame::Run()
{ return Windows::Window::MessageLoop(); }

void WindowsGame::Exit()
{ _window->Close(); }

void WindowsGame::Update(GameTime gameTime)
{ }

void WindowsGame::Draw()
{ }