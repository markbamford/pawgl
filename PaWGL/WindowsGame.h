#pragma once
#include "GameWindow.h"
#include "GameTime.h"
#include "WindowsGraphicsManager.h"
#include <windows.h>

namespace Windows { class GameWindow; };

class WindowsGame : public Game
{
private:
	Windows::GameWindow* _window;

public:
	WindowsGame(HINSTANCE hInstance, LPCSTR title);
	virtual ~WindowsGame();

	int Run();
	void Exit();
	virtual void Update(GameTime gameTime);
	virtual void Draw();
};