#include "WindowsGraphicsManager.h"

namespace Graphics
{
	WindowsGraphicsManager::WindowsGraphicsManager(HWND hwnd)
	{
		_hwnd = hwnd;
		_front = GetDC(_hwnd); // Initialises front buffer device context (window)
		RECT rect;
		GetClientRect(_hwnd, &rect);
		_bounds = Bounds(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
		_back = CreateCompatibleDC(_front); // sets up Back DC to be compatible with the front
		_frontBitmap = CreateCompatibleBitmap(_front, _bounds.Right, _bounds.Bottom); //creates bitmap compatible with the front buffer
		_backBitmap = (HBITMAP)SelectObject(_back, _frontBitmap); //creates bitmap compatible with the back buffer
		FillRect(_back, &rect, (HBRUSH)GetStockObject(0));	
	}

	WindowsGraphicsManager::~WindowsGraphicsManager(void)
	{
		DeleteObject(_frontBitmap);
		DeleteObject(_backBitmap);
		DeleteDC(_back);
		if(_front) ReleaseDC(_hwnd, _front);
	}

	HDC WindowsGraphicsManager::GetHDC()
	{ return _back; }

	HDC WindowsGraphicsManager::CreateCompatibleHDC()
	{ return CreateCompatibleDC(_front); }

	Texture* WindowsGraphicsManager::TextureFromBMP(const char* filename)
	{
		HBITMAP hBitmap = (HBITMAP)LoadImage(NULL, filename, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
		BITMAP b;
		GetObject(hBitmap, sizeof(BITMAP), (LPVOID)&b);
		int width = (int)b.bmWidth;
		int height = (int)b.bmHeight;

		if(b.bmBitsPixel == 32)
		{
			int size = b.bmHeight * b.bmWidth * b.bmBitsPixel / 8;
			BYTE* lpBits = new BYTE[size];
			GetBitmapBits(hBitmap, size, lpBits);

			// Alpha Channel
			BYTE* pPixel = lpBits;
			// pre-multiply rgb channels with alpha channel
			for (int y = 0; y < height; y++)
			{				
				for (int x = 0; x < width ; x++)
				{
					pPixel[0]= (BYTE)((float)pPixel[0] * (float)pPixel[3] / 255.0f);
					pPixel[1]= (BYTE)((float)pPixel[1] * (float)pPixel[3] / 255.0f);
					pPixel[2]= (BYTE)((float)pPixel[2] * (float)pPixel[3] / 255.0f);
					pPixel += 4;
				}
			}

			SetBitmapBits(hBitmap, size, lpBits);
			free(lpBits);
		}

		_textures.Add(new WindowsTexture(this, _textures.Count(), width, height, b.bmBitsPixel, hBitmap));
		return _textures[_textures.Count() - 1];
	}

	bool WindowsGraphicsManager::SelectTexture(int id)
	{
		if(!GraphicsManager::SelectTexture(id)) return false;
		WindowsTexture* tex((WindowsTexture*)_textures[id]);
		SelectObject(tex->GetHDC(), tex->GetHBitmap());//Need Revising mb?
		return true;
	}

	void WindowsGraphicsManager::SetBounds(Bounds bounds)
	{
		_bounds.Right = bounds.Right - bounds.Left; 
		_bounds.Bottom = bounds.Bottom - bounds.Top; 
	}

	void WindowsGraphicsManager::Present()
	{
		BitBlt(_front, _bounds.Left, _bounds.Top, _bounds.Right, _bounds.Bottom, _back, 0, 0, SRCCOPY);
		RECT rect;
		rect.left = _bounds.Left;
		rect.top = _bounds.Top;
		rect.right = _bounds.Right;
		rect.bottom = _bounds.Bottom;
		FillRect(_back, &rect, (HBRUSH)GetStockObject(BLACK_BRUSH));//CreateSolidBrush(RGB(0,0,255)));//
	}
}