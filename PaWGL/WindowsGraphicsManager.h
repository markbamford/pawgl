#ifndef __WindowsGraphicsManager_H__
#define __WindowsGraphicsManager_H__
#include <windows.h>
#include "GraphicsManager.h"
#include "WindowsTexture.h"

namespace Graphics
{
	class WindowsGraphicsManager : public GraphicsManager
	{
	private:
		HWND _hwnd;
		HDC _front;
		HDC _back;
		HBITMAP	_backBitmap;
		HBITMAP	_frontBitmap;

	public:
		WindowsGraphicsManager(HWND hwnd);
		~WindowsGraphicsManager(void);
			
		HDC GetHDC();
		HDC CreateCompatibleHDC();

		bool SelectTexture(int id);
		void SetBounds(Bounds bounds);
		void Present();

	protected:
		Texture* TextureFromBMP(const char* filename);
	};
}
#endif