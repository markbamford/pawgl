#include <windows.h>
#include <stdio.h>
#include "GameWindow.h"
#include "TestGame.h"
#include "JellyDemo.h"
#include "Keys.h"
#include "Input.h"
#include "WindowsSprite.h"
using namespace Windows;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR szCmdLine, int nCmdShow)			
{					
	JellyDemo g(hInstance);
	return g.Run();
	//TestGame g(hInstance);
	//return g.Run();
}