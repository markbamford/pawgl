#include "WindowsTexture.h"

namespace Graphics
{
	WindowsTexture::WindowsTexture(WindowsGraphicsManager* graphics, int id, int width, int height, short bitsPerPixel, HBITMAP hBitmap)
		 : Texture(graphics, id, width, height, bitsPerPixel), _hBitmap(hBitmap), _hdc(graphics->CreateCompatibleHDC())
	{ }

	WindowsTexture::~WindowsTexture(void)
	{
		DeleteObject(_hBitmap);
		DeleteDC(_hdc);
	}

	HDC WindowsTexture::GetHDC() { return _hdc; }

	HBITMAP WindowsTexture::GetHBitmap() { return _hBitmap; }

	void WindowsTexture::Render(int x, int y)
	{
		HBITMAP originalBitMap;
		originalBitMap = (HBITMAP)SelectObject(_hdc, _hBitmap);
		AlphaBlend(((WindowsGraphicsManager*)_graphics)->GetHDC(), x - (_width >> 1), y - (_height >> 1), _width, _height, _hdc, 0, 0, _width, _height, Blend);
		SelectObject(_hdc, originalBitMap); 
	}

	BLENDFUNCTION WindowsTexture::Blend = {AC_SRC_OVER, 0, 255, AC_SRC_ALPHA};
}