#pragma once
#include <windows.h>
#include "WindowsGraphicsManager.h"
#include "Texture.h"

namespace Graphics
{
	class WindowsGraphicsManager;
	class WindowsTexture : public Texture
	{
	private:
		HDC _hdc;
		HBITMAP _hBitmap;

	public:
		WindowsTexture(WindowsGraphicsManager* graphics, int id, int width, int height, short bitsPerPixel, HBITMAP hBitmap);
		~WindowsTexture(void);

		HDC GetHDC();
		HBITMAP GetHBitmap();
		void Render(int x, int y);

		static BLENDFUNCTION Blend;
	};
}